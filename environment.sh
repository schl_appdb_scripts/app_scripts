#!/bin/bash
##############
# Author.....: Renford McDonald
# Created....: 2014-06-19
# Updated....: 2014-07-03 by Renford McDonald
# Updated....: ####-##-## by Firstname Lastname
# Name.......: <scriptName>.sh -d <database>
# Description: Setup application runtime defaults for scripts etc
##############
# Assume all scripts are in the same directory.
OSTYPE=$( uname | tr a-z A-Z )
EXE_DIR=$(dirname $0) && [[ ${EXE_DIR} == '..' ]] && EXE_DIR='.'
SCRIPT_BASE_NAME=$(basename $0 .sh)
WORK_DIR='/backups'         # set the default work directory
USER=`whoami`

#if [[ ! -d ${WORK_DIR} ]]; then
#   WORK_DIR=${HOME}
#fi
# set some OS defaults (Linux vs. AIX)
if [[ ${OSTYPE} == 'LINUX' ]]; then
   rm_option=' -v '
   mv_option=' -v '
else
   rm_option=' -e '
   mv_option=' -e '
   # WORK_DIR='/work/database'
fi

help_usage() {
   echo "$1"
   #echo "Usage: $0 -h | -i <Instance> -d <Database>"
   echo "Usage: $0 -h |  -d <databaseName>"
}

# START HEREq
if [[ -z ${DB2INSTANCE} ]]; then
  help_usage "$( date '+%Y-%m-%d-%H.%M.%S' ): A DB2 instance environment has not been loaded!"
  exit 1
fi

OPTIND=1
while getopts "d:h" option
do
   case $option in
      h) help_usage $0
         exit 1 ;;
      d) database=${OPTARG}
         [[ -z ${database} ]] && help_usage && exit 1;;
      *) help_usage
         exit 1;;
   esac
   # echo "Option ${option}: ${OPTARG}"
done
if [[ -z ${database} ]]; then
   help_usage
   exit 1
fi
database=$( echo ${database} | tr A-Z a-z )
DATABASE=$( echo ${database} | tr a-z A-Z )

app_env=${database:$((${#database}-1)):1}   # The last character of the database name; environment
case ${app_env} in
    'd') app_env='dev';;
    'q') app_env='test';;
    'p') app_env='perf';;
    'u') app_env='uat';;
    'p') app_env='prod';;
     * ) app_env='dev';;
esac

app_name=${database:0:$((${#database}-3))}
case ${app_name} in
    '' );;
esac

data_dir="${WORK_DIR}/data/${USER}/${database}" # /${app_name}"
logs_dir="${WORK_DIR}/logs/${USER}/${database}" # /${app_name}"



if [[ ! -d ${data_dir} ]]; then
#   data_dir="${HOME}/data" #/${app_name}"
   mkdir -p ${data_dir}
fi

if [[ ! -d ${logs_dir} ]]; then
#   data_dir="${HOME}/data" #/${app_name}"
   mkdir -p ${logs_dir}
fi

echo "HOME: $HOME"
if [[ ! -d ${data_dir} ]] ||  [[ ! -d ${logs_dir} ]]; then
   help_usage "Unable to create/setup work directories!"
###   exit 1
fi

export DATABASE database WORK_DIR data_dir logs_dir app_name app_env OSTYPE
export DB_INSTANCE_HOME="${WORK_DIR}/${app_name}"
export APP_HOME="${WORK_DIR}/${app_name}"

# OPTIND=1
echo "PWD: $PWD"
echo "EXE_DIR: ${EXE_DIR}"
echo "Application: ${app_name}"
echo "APP_HOME: ${APP_HOME}"
echo "DATABASE: ${DATABASE}"
echo "database: ${database}"
echo "WORK_DIR: ${WORK_DIR}"
echo "DATA_DIR: ${data_dir}"
echo "LOGS_DIR: ${logs_dir}"
echo "SCRIPT_BASE_NAME: ${SCRIPT_BASE_NAME}"


