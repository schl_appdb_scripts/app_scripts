#!/bin/bash
# File Name     : bkfair_data_maintenance.ksh 
# Version       : V-03
#                 
# Remark        : 1- The Warning_file is being appended each time a 
#                    warning is issued.
#                 2- the file variable "dup_url_email_body" will generate 
#                    an email body only when duplicated URLs will be found
#                    and are kept for history purpose per Business request.
#                 
# Purpose       : Maintenance script setup towards cleaning data
#                 regularly and allowing task expansion for future 
#                 work request needing to be automated.
#                 Check the History section to refer to the script expansion.
#                 
# Parameters    : A1 : -e + <global environment name> -- Mandatory
#                           e.g. dev,qa,prod
#                 A2 : -d + <database_name>           -- Mandatory
#                 A3 : -s + <schema_name>             -- Mandatory
#                           note: in uppercase
#                 A4 : -a + <script_action>           -- Mandatory
#                      note: what the script will essentially do.
#                 A5 : -o + <script_option>           -- Optional
#                      Note: E.g. represents the column name in the case of
#                            running the special_program data update.
# Create Date   : 05/20/2011
# Created By    : Franck Cabaret (FC)
#
# set -n        # Uncomment to check the script syntax without any execution
# set -x        # Uncomment to debug this shell script
#
# History       : mm/dd/yyyy (DBA_initials)
#                 05/20/2011 (FC)
#                 Part of Bookfair Release 18
#                 Setup of Section # 1 which deletes records from table
#                 MY_FAIR_SELECTION for data that is older than 2 years.
#
#                 02/29/2012 (FC)
#                 release 19.5
#                 Add Section # 2
#                 which allows the update of both tables 
#                 ONLINE_HOMEPAGE on a yearly basis via a call to a new stored 
#                 procedure SP_RESET_ONLINE_HOMEPAGE_SPECIAL_PROGRAMS.
#                 
#                 12/10/2013 (FC) V-02
#                    1- Integration of multiple environments
#                    2- Add Check for duplicated URLs per Business Request.
#                 
#                 07/11/2014 (FC) V-03
#                    1- AWS Migration version
#                 
###############################################################################
#                             VARIABLES                                       #
###############################################################################
create_date=$(date +"%m%d%Y")
create_time=$(date +"%H_%M_%S")
exec_timestamp="${create_date}_${create_time}"
default_string="not_set"
host_name=$(hostname -s)
user_name=${LOGNAME}
exec_date=$(date +%Y%m%d)
exec_time=$(date +%H%M%S)
current_hour=$(date +%H)
script_name=$(basename $0 .ksh)
param_list=$@

   # Parameter Initialization...
hostname_env=${default_string}
db_name=${default_string}
schema_name=${default_string}
script_action=${default_string}
script_option=${default_string}

   # "app_env" tracks the different swimlanes:
   #    [ dev, dev02, qa, qa02, uat01, prod ]
app_env=""

     # for Section # 1
num_rec_delete_ct=""

     # for Section # 2
to_be_updated_count=0
bkfair01_update_count=0
unpublished_update_count=0

     # for Section # 3
duplicate_url_count=0

################################################################################
#                             MAIL RELATED                                     #
################################################################################
global_subject_line=""
subject_line="Bookfair Data Maintenance"
mailout_list="fcabaret@scholastic.com,MNaresh@scholastic.com,RAvala@scholastic.com,BTracey@scholastic.com"
mailout_list_error="fcabaret@scholastic.com,MNaresh@scholastic.com,RAvala@scholastic.com"
prod_mailout_list="fcabaret@scholastic.com,MNaresh@scholastic.com,RAvala@scholastic.com,BTracey@scholastic.com,LBrown@ScholasticBookfairs.com,MPCallahan@scholasticbookfairs.com"

################################################################################
#                             FILES                                            #
################################################################################
   # Set the Base Work Directory (for all environments)
export WORK_DIR='/backups'

     # Default "mail_body" location in case an error is encountered
     #   and to insure an email will be fired correctly...
     
mail_body=/tmp/${user_name}_${app_env}_bkfair_maintenance_body.txt

   # Check that the db2 instance has been loaded on that machine...
   # The profile is loaded directly when the User logs in (Chef).
   # For that matter, any user must have his/her own profile upgraded.
   
if [[ -z ${DB2INSTANCE} ]]; then
   printf "\n\t All,"                                                >  ${mail_body}
   printf "\n\n\t The script -${script_name}- failed as"             >> ${mail_body}
   printf "\n\t the DB2 instance environment had not been loaded!."  >> ${mail_body}
   printf "\n\t Here follows your command line:"                     >> ${mail_body}
   printf "\n\t Script Name : $script_name"                          >> ${mail_body}
   printf "\n\t Command Line: $param_list"                           >> ${mail_body}
   printf "\n\t Please, contact the DB Admin group to install "      >> ${mail_body}
   printf "\n\t the DB2 instance;  Then, re-run the script if the ." >> ${mail_body}
   printf "\n\t time permits it."                                    >> ${mail_body}
   printf "\n\t This script exited gracefully."                      >> ${mail_body}
   printf "\n\n\t Thank you"                                         >> ${mail_body}
   printf "\n\t The Data Team\n"                                     >> ${mail_body}
   
   global_subject_line="db2_task_failure -${host_name}-${app_env} - ${subject_line} failed to run...instance not setup"
   mail -s "${global_subject_line}"  "${mailout_list}" < ${mail_body}

   exit 1
fi

     #-----------------------------------------------------------
     # First call to the "getops" loop to JUST set up the
     #    "Environment" default values when appropriate.
     # All other parameters will be filtered during the
     #    second call.
     #
     # Note-1: The -e parameter value needs to be replaced 
     #         by the value setup into Chef when moving to AWS.
     # Note-2: This call is a "light" version of the 
     #         functionalities encountered in the second call
     #         to "getops"; this light version is dynamically
     #         filtering the current environment [dev,qa,prod]
     #         using the database name pattern.
     #         The variable "app_env" will be finalized in the 
     #         second call.
     # Note-3: There is no function call from this section.
     #-----------------------------------------------------------
     
while getopts :e:d:s:a:o:t: param_item
do
     case $param_item in
          e)      # Environment setup...
                  #----------------------------------------------
                  # 1- DEV environemnt variables values setup
                  #----------------------------------------------
          
               if [[ "$OPTARG" == "dev" ]]; then
                    app_env="dev"
                    DATA_FILE_PREFIX="DEV"
               
                  #----------------------------------------------
                  # 2- QA environemnt variables values setup
                  #----------------------------------------------
               
               elif [[ "$OPTARG" == "qa" ]]; then
                    app_env="qa"
                    DATA_FILE_PREFIX="QA"
               
                  #----------------------------------------------
                  # 3- PROD environemnt variables values setup
                  #----------------------------------------------
               
               elif [[ "$OPTARG" == "prod" ]]; then
                    app_env="prod"
                    DATA_FILE_PREFIX="PROD"
               
               else
                    printf "\n\t All,"                                          >  ${mail_body}
                    printf "\n\n\t The script -${script_name}- failed to run"   >> ${mail_body}
                    printf "\n\t due to its Environment parameter "             >> ${mail_body}
                    printf "\n\t value -$OPTARG- which was not valid."          >> ${mail_body}
                    printf "\n\t Here follows your command line:"               >> ${mail_body}
                    printf "\n\t Script Name : $script_name"                    >> ${mail_body}
                    printf "\n\t Command Line: ${param_list}"                   >> ${mail_body}
                    printf "\n\t Please, resubmit your command with the"        >> ${mail_body}
                    printf "\n\t proper parameters."                            >> ${mail_body}
                    printf "\n\t This script exited gracefully."                >> ${mail_body}
                    printf "\n\n\t Thank you"                                   >> ${mail_body}
                    printf "\n\t The Data Team\n"                               >> ${mail_body}
                    
                    global_subject_line="db2_task_failure -${host_name}-${app_env}-${subject_line} failed to run...wrong environment"
                    mail -s "${global_subject_line}"  "${mailout_list}" < ${mail_body}

                    exit 2
               fi
             ;;
          
          d)   if [[ "$OPTARG" == "bkf01d" ||
                     "$OPTARG" == "bkf02d" ||
                     "$OPTARG" == "bkf01q" ||
                     "$OPTARG" == "bkf02q" ||
                     "$OPTARG" == "bkf01u" ||
                     "$OPTARG" == "bkf01p"   ]]; then
                     
                    db_name=$OPTARG
                    LOG_HOME="${WORK_DIR}/logs/${LOGNAME}/${db_name}"
                    DB_HOME="${WORK_DIR}/data/${LOGNAME}/${db_name}"
                       # We capture the last character of the database name [d,q,u,p]
                    app_env=${db_name:$((${#db_name}-1)):1}
                    case ${app_env} in
                        'd') app_env='dev'
                             ;;
                        'q') app_env='qa'
                             ;;
                        'u') app_env='uat01'
                             ;;
                        'p') app_env='prod'
                             ;;
                    esac
                  
               else
                    printf "\n\t All,"                                          >  ${mail_body}
                    printf "\n\n\t The script -${script_name}- failed to run"   >> ${mail_body}
                    printf "\n\t due to its Database parameter "                >> ${mail_body}
                    printf "\n\t value -$OPTARG- which was not valid."          >> ${mail_body}
                    printf "\n\t Here follows your command line:"               >> ${mail_body}
                    printf "\n\t Script Name : $script_name"                    >> ${mail_body}
                    printf "\n\t Command Line: ${param_list}"                   >> ${mail_body}
                    printf "\n\t Please, resubmit your command with the"        >> ${mail_body}
                    printf "\n\t proper parameters."                            >> ${mail_body}
                    printf "\n\t This script exited gracefully."                >> ${mail_body}
                    printf "\n\n\t Thank you"                                   >> ${mail_body}
                    printf "\n\t The Data Team\n"                               >> ${mail_body}
                    
                    global_subject_line="db2_task_failure -${host_name}-${app_env}-${subject_line} failed to run...wrong database name"
                    mail -s "${global_subject_line}"  "${mailout_list}" < ${mail_body}

                    exit 2
               fi
               
                  # Check if the Log and File directories are available...
               if [[ ! -w ${DB_HOME} ]] && 
                  [[ ! -w ${LOG_HOME} ]]; then
                    echo 'The output directories are not writable or does not exists!'
                    printf "\n\t All,"                                          >  ${mail_body}
                    printf "\n\n\t The script -${script_name}- failed to run"   >> ${mail_body}
                    printf "\n\t as either the LOG directory or:and the file"   >> ${mail_body}
                    printf "\n\t directory was:were not available."             >> ${mail_body}
                    printf "\n\n\t Please verify the environment structure and" >> ${mail_body}
                    printf "\n\t resubmit your script if time permits it."      >> ${mail_body}
                    printf "\n\n\t This script exited gracefully."              >> ${mail_body}
                    printf "\n\n\t Thank you"                                   >> ${mail_body}
                    printf "\n\t The Data Team\n"                               >> ${mail_body}
                    
                    global_subject_line="db2_task_failure -${host_name}-${app_env}-${subject_line} failed to run...directory not available"
                    mail -s "${global_subject_line}"  "${mailout_list}" < ${mail_body}
                    exit 2
               fi
             ;;
          
          s) ;;
          
          a) ;;
              
          o) ;;
          
          t) ;;
          
          \?) printf "\n\t All,"                                                >  ${mail_body}
              printf "\n\n\t The script -${script_name}- failed to run due"     >> ${mail_body}
              printf "\n\t to one of its parameters which was not valid."       >> ${mail_body}
              printf "\n\t Here follows your command line:"                     >> ${mail_body}
              printf "\n\t Script Name : $script_name"                          >> ${mail_body}
              printf "\n\t Command Line: $param_list"                           >> ${mail_body}
              printf "\n\t Please, resubmit your command with the"              >> ${mail_body}
              printf "\n\t proper parameters."                                  >> ${mail_body}
              printf "\n\t This script exited gracefully."                      >> ${mail_body}
              printf "\n\n\t Thank you"                                         >> ${mail_body}
              printf "\n\t The Data Team\n"                                     >> ${mail_body}
                    
              global_subject_line="db2_task_failure -${host_name}-${app_env} - ${subject_line} failed to run...wrong parameter"
              mail -s "${global_subject_line}"  "${mailout_list}" < ${mail_body}
              exit 2
     esac
done

     # Reset the getops index...
OPTIND=1

exported_data_dir=${DB_HOME}/

#########################################################################
#                       FUNCTIONS                                       #
#########################################################################
#********************* fn_db2Check *************************************
# Purpose      : This function checks the proper output of a db2 command.
#                Note: Based on the release to be executed, the "exit 2"
#                      command from within the function "fn_db2_check" can
#                      be uncommented for the desired result.
#                
# Parameters   : p1  : <exit_code>     mandatory
#                p2  : <object_type>   optional
#                p3  : <object_name>   optional
#
# Remarks      : This function, as it is, will not make the script exit.
#                If you want it to, you need to code additionally for it
#                to take the proper ensuing action.
#        
function fn_db2Check {
     exit_code=$1
     fn_object_name=$2
     fn_object_action=$3
     fn_object_option=$4

     if (( $exit_code == 0 )); then
               # everything ran fine... and we don't do anything.
          :
     elif (( $exit_code == 1 )); then
               # A "update" or "delete" returned no rows...
          printf " \n\t--------------------------------------------------"    >> $warning_file
          printf " \n\t Execution of Script Name: ${script_name} on $(date):" >> $warning_file
          printf " \n\t With action code of     : ${script_action}"           >> $warning_file
          printf " \n\t Error code --$exit_code-- was issued"                 >> $warning_file
          printf " \n\t while processing object name: --${fn_object_name}--"  >> $warning_file
          printf " \n\t and actionning a: --${fn_object_action}--"            >> $warning_file
          printf " \n\t More likely, either an UPDATE or DELETE ended up"     >> $warning_file
          printf " \n\t targeting zero data record."                          >> $warning_file
          printf " \n\t Please review the error log and log for more info."   >> $warning_file
          printf " \n\t action if necessary.\n"                               >> $warning_file

     elif (( $exit_code == 2 )); then
               # A db2 command or sql statement warning occured...
          printf " \n\t A WARNING --$exit_code-- occured"                     | tee -a $error_file
          printf " \n\t while processing object name: --${fn_object_name}--"  | tee -a $error_file
          printf " \n\t and actionning a: --${fn_object_action}--"            | tee -a $error_file
          printf " \n\t e.g. that object may have not existed "               | tee -a $error_file
          printf " \n\t or"                                                   | tee -a $error_file
          printf " \n\t a PK constraint has been named after"                 | tee -a $error_file
          printf " \n\t      an existing unique index."                       | tee -a $error_file
          printf " \n\t Please review the error log and take appropriate"     | tee -a $error_file
          printf " \n\t action if necessary.\n"                               | tee -a $error_file

     elif (( $exit_code == 4 )); then
               # A db2 or SQL statement error occured...
          printf " \n\t An ERROR --$exit_code-- occured while processing"     | tee -a $error_file
          printf " \n\t object name: --${fn_object_name}--"                   | tee -a $error_file
          printf " \n\t and actionning a: --${fn_object_action}--"            | tee -a $error_file
          printf " \n\t Please review the error log and take appropriate"     | tee -a $error_file
          printf " \n\t action if necessary.\n"                               | tee -a $error_file
          printf " \n\t As the script exited with an error, it will be "      | tee -a $error_file
          printf " \n\t necessary to run the left over SQL manually from"     | tee -a $error_file
          printf " \n\t where the script failed."                             | tee -a $error_file
          
          if [[ "$fn_object_action" == "DB2_CONNECT_CMD" ]]; then
               printf "\n\n\t The source of the problem was an impossibility" | tee -a $error_file
               printf "\n\t to connect to database ${db_name} to export data."| tee -a $error_file
               printf "\n\t Therefore the script exited without"              | tee -a $error_file
               printf "\n\t processing any data.\n\n"                         | tee -a $error_file
          
          elif [[ "$fn_object_action" == "EXPORT_CMD" ]]; then
               printf "\n\n\t There was a problem exporting data from "       | tee -a $error_file
               printf "\n\t table ${fn_object_name}."                         | tee -a $error_file
               # There is no need to terminate to see outcome of all export commands.
          fi
          
          printf " \n\n\t Thank you"                                          | tee -a $error_file
          printf " \n\t The Data Team.\n\n"                                   | tee -a $error_file
          db2 terminate >> $error_file
          
          cat $log      >> $error_file
          mail -s "db2_task_failure-${host_name}-${subject_line} encountered problems..." "$mailout_list"  <  $error_file
          exit 4

     elif (( $exit_code == 8 )); then
          fn_set_task_end_timestamp  Internal_OS_Error
               # A command line processor system error occured...
          printf " \n\t A SYSTEM ERROR --$exit_code-- occured \n"             | tee -a $error_file
          printf " \n\t while processing object name: --${fn_object_name}--"  | tee -a $error_file
          printf " \n\t and actionning a: --${fn_object_action}--"            | tee -a $error_file
          printf " \n\t Please review the error log and take appropriate"     | tee -a $error_file
          printf " \n\t action if necessary.\n"                               | tee -a $error_file
          printf " \n\t As the script exits with an error, it will be "       | tee -a $error_file
          printf " \n\t necessary to run the left over SQL manually from"     | tee -a $error_file
          printf " \n\t where the script failed."                             | tee -a $error_file
          printf " \n\t The Data Team.\n\n"                                   | tee -a $error_file
          db2 terminate >> $error_file
          
          cat $log      >> $error_file
          mail -s "db2_task_failure-${host_name}-${subject_line} encountered problems..." "$mailout_list"  <  $error_file
          exit 8
     fi
}     ### fn_db2Check


#**************************** fn_mail_test_setup *******************************
#      Purpose      : This function sets all email destination to the username
#                     of the User running this script when passing the option 
#                     parameter "test_dev_run"
#      Parameters   : none
#      Remark       : This function is called from the "-t" script parameter
#                     section.  The DBA running that option will have created
#                     proper directories to mimick the Dev/QA/Prod ones
function fn_mail_test_setup {

   mailout_list="${user_name}@scholastic.com"
   mailout_list_error="${user_name}@scholastic.com"
   prod_mailout_list="${user_name}@scholastic.com"
 
}     ### end --> fn_mail_test_setup


#########################################################################
#                                   MAIN                                #
#########################################################################
#----------------------------------
#          Parameter Filtering
#----------------------------------
while getopts :e:d:s:a:o:t: param_item
do
     case $param_item in
          e)        # Environment setup (hostname) and remote...
               if [[ "$OPTARG" == "dev" ]]; then
                    hostname_env=$OPTARG
               
               elif [[ "$OPTARG" == "qa" ]]; then
                    hostname_env=$OPTARG
               
               elif [[ "$OPTARG" == "prod" ]]; then
                    hostname_env=$OPTARG
               
               else
                    printf "\n\t This environment -$OPTARG- and "               >  ${mail_body}
                    printf "\n\t -${host_name}- is not available."              >> ${mail_body}
                    printf "\n\t Your script and command line were:"            >> ${mail_body}
                    printf "\n\t Script Name : $script_name"                    >> ${mail_body}
                    printf "\n\t Command Line: ${param_list}"                   >> ${mail_body}
                    printf "\n\t Please, resubmit your command with the"        >> ${mail_body}
                    printf "\n\t proper parameters."                            >> ${mail_body}
                    printf "\n\t This script exited gracefully."                >> ${mail_body}
                    printf "\n\n\t Thank you"                                   >> ${mail_body}
                    printf "\n\t The Data Team\n"                               >> ${mail_body}
                    
                    mail -s "db2_task_failure -${host_name}-${app_env}-${subject_line} failed to run...wrong environment" \
                            "$mailout_list"  <  ${mail_body}
                    exit 2
               fi
               ;;

               # if-01
               # Filters all possible databases; when more are added,
               # include the new one down here.
               
          d)   if [[ "$OPTARG" == "bkf01d" ||
                     "$OPTARG" == "bkf02d" ||
                     "$OPTARG" == "bkf01q" ||
                     "$OPTARG" == "bkf02q" ||
                     "$OPTARG" == "bkf01u" ||
                     "$OPTARG" == "bkf01p"   ]]; then
                    db_name=$OPTARG
                    
                         # Set a different work path based on the database
                         #    names and LOGNAME.
                         # Reminder_1: bkf02x exists in both DEV and QA
                         # Reminder_2: bkf01u exists only in QA
                         
                       # log files directory
                    export LOG_HOME="${WORK_DIR}/logs/${LOGNAME}/${db_name}"
                       # data files directory
                    export DB_HOME="${WORK_DIR}/data/${LOGNAME}/${db_name}"
                    
                       # if-02
                       # Set up the remote-FTP directory and the app_env 
                       #  according to the database being used.
                       # Remember that the default app_env were setup in the 
                       #  first call to getops.
                       
                    if [[ "$db_name" == "bkf01d" ]] || 
                       [[ "$db_name" == "bkf01q" ]] || 
                       [[ "$db_name" == "bkf01p" ]];   then
                       FTP_HOME=SIG_BookFairs
                       
                    elif [[ "$db_name" == "bkf02d" ]] || 
                         [[ "$db_name" == "bkf02q" ]]; then
                       FTP_HOME=SIG_BookFairs/SWIMLANE_2/
                         
                       # if-03
                       if [[ "$db_name" == "bkf02d" ]]; then
                          app_env="dev02"
                          DATA_FILE_PREFIX="DEV02"
                          
                            # OR, Address bkfair02 db in QA...
                              
                       elif [[ "$db_name" == "bkf02q" ]]; then
                              app_env="qa02"
                              DATA_FILE_PREFIX="QA02"
                              
                       fi   ### if-03-end
                         
                         # Address UAT db only in QA...
                    
                    elif [[ "$db_name" == "bkf01u" ]]; then
                         FTP_HOME=SIG_BookFairs/UAT_PERF/
                         
                            # Address UAT db only in QA checking once more the env...
                              
                         app_env="uat"
                         DATA_FILE_PREFIX="UAT"
                         
                         # This "else" is to prevent to run this script
                         #  in Prod against the wrong database...
                    else
                         printf "\n\t The combination database ${db_name}"      >  ${mail_body}
                         printf "\n\t and environment ${host_name}"             >> ${mail_body}
                         printf "\n\t is not available."                        >> ${mail_body}
                         printf "\n\t Your script and command line were:"       >> ${mail_body}
                         printf "\n\t Script Name : $script_name"               >> ${mail_body}
                         printf "\n\t Command Line: $param_list"                >> ${mail_body}
                         printf "\n\t Please, resubmit your command with the"   >> ${mail_body}
                         printf "\n\t proper parameters."                       >> ${mail_body}
                         printf "\n\t This script exited gracefully."           >> ${mail_body}
                         printf "\n\n\t Thank you"                              >> ${mail_body}
                         printf "\n\t The Data Team\n"                          >> ${mail_body}
                         
                         mail -s "db2_task_failure -${host_name}-${app_env}-${subject_line} failed to run...wrong database" \
                            "$mailout_list"  <  ${mail_body}
                         exit 2
                    fi   ### if-02-end
               else
                    printf "\n\t This database --$OPTARG--is not available."    >  ${mail_body}
                    printf "\n\t Your script and command line were:"            >> ${mail_body}
                    printf "\n\t Script Name : $script_name"                    >> ${mail_body}
                    printf "\n\t Command Line: $param_list"                     >> ${mail_body}
                    printf "\n\t Please, resubmit your command with the"        >> ${mail_body}
                    printf "\n\t proper parameters."                            >> ${mail_body}
                    printf "\n\t This script exited gracefully."                >> ${mail_body}
                    printf "\n\n\t Thank you"                                   >> ${mail_body}
                    printf "\n\t The Data Team\n"                               >> ${mail_body}
                    
                    mail -s "db2_task_failure -${host_name}-${app_env}-${subject_line} failed to run... wrong database." \
                            "$mailout_list"  <  ${mail_body}
                    exit 2
               fi   ### if-01-end
               ;;

          s)   # if-04 
               if [[ "$OPTARG" == "BKFAIR01" ]]; then
                    schema_name=$OPTARG
               else
                    printf "\n\t This schema --$OPTARG--is not available."      >  ${mail_body}
                    printf "\n\t Your script and command line were:"            >> ${mail_body}
                    printf "\n\t Script Name : $script_name"                    >> ${mail_body}
                    printf "\n\t Command Line: $param_list"                     >> ${mail_body}
                    printf "\n\t Please, resubmit your command with the"        >> ${mail_body}
                    printf "\n\t proper parameters."                            >> ${mail_body}
                    printf "\n\t This script exited gracefully."                >> ${mail_body}
                    printf "\n\n\t Thank you"                                   >> ${mail_body}
                    printf "\n\t The Data Team\n"                               >> ${mail_body}
                    
                    mail -s "db2_task_failure -${host_name}-${app_env}-${subject_line} failed to run... wrong schema." \
                            "$mailout_list"  <  ${mail_body}
                    exit 2
               fi   # if-04
               ;;

          a)   # if-05
               if [[ "$OPTARG" == "fair_selection_cleanup"  || 
                     "$OPTARG" == "online_homepage_yearly_update" || 
                     "$OPTARG" == "duplicated_url_check" ]]; then
                    script_action=$OPTARG
                    
                         # This sub-section sets the log names according 
                         #   to the script name and script action.
                  
                  # if-06
                  if [[ "${script_action}" == "fair_selection_cleanup" ]]; then
                     log=${LOG_HOME}/${app_env}_${script_name}__${script_action}_${exec_date}_${exec_time}.log
                     error_file=${LOG_HOME}/${app_env}_${script_name}__${script_action}__error_${exec_date}_${exec_time}.log
                  
                  elif [[ "${script_action}" == "online_homepage_yearly_update" ]]; then
                     log=${LOG_HOME}/${app_env}_${script_name}__${script_action}_${exec_date}_${exec_time}.log
                     error_file=${LOG_HOME}/${app_env}_${script_name}__${script_action}__error_${exec_date}_${exec_time}.log
                  
                  elif [[ "${script_action}" == "duplicated_url_check" ]]; then
                     log=${LOG_HOME}/${app_env}_${script_name}__${script_action}_${exec_date}_${exec_time}.log
                     error_file=${LOG_HOME}/${app_env}_${script_name}__${script_action}__error_${exec_date}_${exec_time}.log
                  fi   # if-06
                  
                  email_feedback_body=${LOG_HOME}/${app_env}_mail_body_bkfair_data_maintenance_${exec_date}_${exec_time}.txt
                  spec_prog_email_body=${LOG_HOME}/${app_env}_special_prgms_updt_bkfair_email_feedback_${exec_date}_${exec_time}.txt
                  warning_file=${LOG_HOME}/${app_env}_warning_bkfair_data_maintenance_${exec_date}_${exec_time}.txt
                  dup_url_email_body=${LOG_HOME}/${app_env}_dup_url_bkfair_email_feedback_${exec_date}_${exec_time}.txt
               
               else
                    printf "\n\t This schema --$OPTARG--is not available."      >  ${mail_body}
                    printf "\n\t Your script and command line were:"            >> ${mail_body}
                    printf "\n\t Script Name : $script_name"                    >> ${mail_body}
                    printf "\n\t Command Line: $param_list"                     >> ${mail_body}
                    printf "\n\t Please, resubmit your command with the"        >> ${mail_body}
                    printf "\n\t proper parameters."                            >> ${mail_body}
                    printf "\n\t This script exited gracefully."                >> ${mail_body}
                    printf "\n\n\t Thank you"                                   >> ${mail_body}
                    printf "\n\t The Data Team\n"                               >> ${mail_body}
                    
                    mail -s "db2_task_failure -${host_name}-${app_env}-${subject_line} failed to run... wrong action" \
                            "$mailout_list"  <  ${mail_body}
                    exit 2
              fi   # if-05
              ;;
              
          o)   # if-07
               if [[ "$OPTARG" == "special_program_include_description"   || 
                     "$OPTARG" == "special_program_include_2_description" || 
                     "$OPTARG" == "special_program_include_3_description" || 
                     "$OPTARG" == "special_program_include_4_description"    ]]; then
                    script_option=$OPTARG
               else
                    printf "\n\t This Option --$OPTARG--is not available."      >  ${mail_body}
                    printf "\n\t Your script and command line were:"            >> ${mail_body}
                    printf "\n\t Script Name : $script_name"                    >> ${mail_body}
                    printf "\n\t Command Line: $param_list"                     >> ${mail_body}
                    printf "\n\t Please, resubmit your command with the"        >> ${mail_body}
                    printf "\n\t proper parameters."                            >> ${mail_body}
                    printf "\n\t This script exited gracefully."                >> ${mail_body}
                    printf "\n\n\t Thank you"                                   >> ${mail_body}
                    printf "\n\t The Data Team\n"                               >> ${mail_body}
                    
                    mail -s "db2_task_failure -${host_name}-${app_env}-${subject_line} failed to run... wrong option" \
                            "$mailout_list"  <  ${mail_body}
                    exit 2
              fi   # if-07
              ;;

          t)   if [[ "$OPTARG" == "dev_test_run" ]]; then
                    script_option=$OPTARG
                    fn_mail_test_setup
                    
               else
                    printf "\n\t This option --$OPTARG--is not allowed."        >  ${mail_body}
                    printf "\n\t Your script and command line were:"            >> ${mail_body}
                    printf "\n\t Script Name : $script_name"                    >> ${mail_body}
                    printf "\n\t Command Line: $param_list"                     >> ${mail_body}
                    printf "\n\t Please, resubmit your command with the"        >> ${mail_body}
                    printf "\n\t proper parameters."                            >> ${mail_body}
                    printf "\n\t This script exited gracefully."                >> ${mail_body}
                    printf "\n\n\t Thank you"                                   >> ${mail_body}
                    printf "\n\t The Data Team\n"                               >> ${mail_body}
                    
                    mail -s "db2_task_failure -${host_name}-${app_env}-${subject_line} failed to run... wrong test-string" \
                            "$mailout_list"  <  ${mail_body}
                    exit 2
               fi
               ;;
               
          \?) printf "\n\t You provided an option that was not available. "     >  ${mail_body}
              printf "\n\t Your script and command line were:"                  >> ${mail_body}
              printf "\n\t Script Name : $script_name"                          >> ${mail_body}
              printf "\n\t Command Line: $param_list"                           >> ${mail_body}
              printf "\n\t Please, resubmit your command with the"              >> ${mail_body}
              printf "\n\t proper parameters."                                  >> ${mail_body}
              printf "\n\t This script exited gracefully."                      >> ${mail_body}
              printf "\n\n\t Thank you"                                         >> ${mail_body}
              printf "\n\t The Data Team\n"                                     >> ${mail_body}
                    
              mail -s "db2_task_failure -${host_name}-${app_env}-${subject_line} failed to run... wrong parameter" \
                            "$mailout_list"  <  ${mail_body}
              exit 2
     esac
done


     #################################################################
     #################################################################
     #     Section # 1:  DELETION OF MY_FAIR_SELECTION DATA
     #                   OLDER THAN 2 YEARS...
     #################################################################
     #################################################################

if [[ "${script_action}" == "fair_selection_cleanup" ]]; then
     printf "\n All,"                                                    >> $log
     printf "\n\n The following output represents the feedback"          >> $log
     printf "\n from BookFair data maintenance regarding data deletion " >> $log
     printf "\n from the table MY_FAIR_SELECTION.\n\n"                   >> $log
     
     db2 -v "connect to ${db_name}"  >>  $log
     fn_db2Check $? ${db_name}_CONNECT  DB2_CONNECT_CMD
     db2 -v "SET SCHEMA ${schema_name}"  >> $log
     fn_db2Check $? ${schema_name}  SET_SCHEMA_CMD

     printf "\n\n Data Deletion for MY_FAIR_SELECTION started on $(date)...\n\n" >> $log

     exec_timestamp=$(db2 -x \
          "SELECT CURRENT TIMESTAMP FROM sysibm.sysdummy1" )

     db2 -av "DELETE FROM ${schema_name}.my_fair_selection \
                 WHERE creation_date < CURRENT TIMESTAMP - 2 YEARS "  | sed 's/  */ /g' >> $log          
     fn_db2Check $? MY_FAIR_SELECTION  DELETION_STMT
     
          # Capture the number of deleted records...
     num_rec_delete_ct=$(tail -n 8 $log | awk '$1~/sqlerrd/{print $8}')
     
     printf "\n All,"                                                      > $email_feedback_body
     printf "\n\n Data maintenance deleted ${num_rec_delete_ct} records "  >> $email_feedback_body
     printf "from the table"                                               >> $email_feedback_body
     printf "\n MY_FAIR_SELECTION which were more than "                   >> $email_feedback_body
     printf "\n 2 years old."                                              >> $email_feedback_body
     printf "\n\n Regards,"                                                >> $email_feedback_body
     printf "\n The Data Team\n\n"                                         >> $email_feedback_body

     # Register this new task action in maintenance table...
     db2 -v "INSERT INTO ${schema_name}.maint_bkfair_tasks \
             (task_id, create_timestamp, task_name, script_name) \
             VALUES ( (SELECT MAX(task_id) + 1 FROM ${schema_name}.maint_bkfair_tasks), \
                      '${exec_timestamp}', \
                      'MY_FAIR_SELECTION data cleanup', \
                      '${script_name}' ) " >> $log

     printf "\n\n Data Deletion for MY_FAIR_SELECTION ended on $(date)\n" >> $log

     db2 -v "terminate"  >> $log
     printf "\n\nThe Data Team.\n"  >> $log

          #-----------------------------
          #  Send Feedback email...
          #-----------------------------

     if [[ -e $error_file ]]; then
          cat $log >> $error_file
          mail -s "db2_task_failure -${host_name}-${app_env}-${subject_line} encountered problems - ${script_action}..." \
               "$mailout_list"  <  $error_file
     else
          mail -s "db2_task_success -${host_name}-${app_env}-${subject_line} Successful - ${script_action}" \
               "$mailout_list"  <  $log
          mail -s "db2_task_success -${host_name}-${app_env}-${subject_line} Successful - ${script_action}" \
               "$mailout_list"  <  $email_feedback_body
     fi

     # the following is towards the next cmd extension for future maintenance request...


     #################################################################
     #################################################################
     #   Section # 2:  YEARLY UPDATE OF ONLINE_HOMEPAGE
     #                 FOR SPECIAL PROGRAMS...
     #################################################################
     #################################################################
     
elif [[ "${script_action}" == "online_homepage_yearly_update" ]]  && 
     [[ "${script_option}" == "special_program_include_description"    || 
        "${script_option}" == "special_program_include_2_description"  || 
        "${script_option}" == "special_program_include_3_description"  || 
        "${script_option}" == "special_program_include_4_description"  ]]; then
     printf "\n All,"                                                                    >> $log
     printf "\n\n The following output represents the feedback"                          >> $log
     printf "\n from BookFair data maintenance regarding updating "                      >> $log
     printf "\n both ONLINE_HOMEPAGE tables for the column: ${script_option}.\n\n"       >> $log
     
     db2 -v "connect to ${db_name}"  >>  $log
     fn_db2Check $? ${db_name}_CONNECT  DB2_CONNECT_CMD
     db2 -v "SET SCHEMA ${schema_name}"  >> $log
     fn_db2Check $? ${schema_name}  SET_SCHEMA_CMD

     printf "\n\n Data update for ONLINE_HOMEPAGE x2 started on $(date)...\n\n" >> $log

     exec_timestamp=$(db2 -x "SELECT CURRENT TIMESTAMP FROM sysibm.sysdummy1" )
     
        # Get the number of fairs that will need to be updated...
     to_be_updated_count=$(db2 -x "SELECT COUNT(oh.fair_id) \
                                     FROM unpublished.online_homepage oh, \
                                          bkfair01.fair f \
                                     WHERE oh.${script_option} IS NOT NULL \
                                        AND oh.${script_option} != '' \
                                        AND oh.fair_id = f.id \
                                     FOR READ ONLY WITH UR " )
        
        # Update the ONLINE_HOMEPAGE column submitted as a parameter to this script...
     db2 -a -v "CALL bkfair01.sp_reset_online_homepage_special_programs('${script_option}', ?, ?)" >> $log 
     
        # Collect the stored procedure result set in an array...
     set -A sp_result_set_array $(tail -n 20 ${log} | awk '$1~/Parameter/{print $4}' | sed 's/ //g')
     
     loop_index=0
     
        # Process the stored procedure result set...
     while (( $loop_index <= (${#sp_result_set_array[*]}-1) ))
     do
        if [[ "${sp_result_set_array[$loop_index]}" == "PI_BKFAIR01_UPDATE_COUNT_OUT" ]]; then
           (( loop_index=loop_index + 1 ))
           bkfair01_update_count=${sp_result_set_array[$loop_index]}
        elif [[ "${sp_result_set_array[$loop_index]}" == "PI_UNPUBLISHED_UPDATE_COUNT_OUT" ]]; then
           (( loop_index=loop_index + 1 ))
           unpublished_update_count=${sp_result_set_array[$loop_index]}
        fi
        
        (( loop_index=loop_index + 1 ))
     done
     
     printf "\n All,"                                                      >  $spec_prog_email_body
     printf "\n\n Data maintenance updated the column ${script_option} "   >> $spec_prog_email_body
     printf "\n in table BKFAIR01.ONLINE_HOMEPAGE: "                       >> $spec_prog_email_body
     printf "\n      for    : ${bkfair01_update_count} records "           >> $spec_prog_email_body
     printf "\n      out of : ${to_be_updated_count} total records"        >> $spec_prog_email_body
     printf "\n in table UNPUBLISHED.ONLINE_HOMEPAGE:  "                   >> $spec_prog_email_body
     printf "\n      for    : ${unpublished_update_count} records "        >> $spec_prog_email_body
     printf "\n      out of : ${to_be_updated_count} total records"        >> $spec_prog_email_body
     printf "\n for all fairs having valid data for that column. "         >> $spec_prog_email_body
     printf "\n\n Regards,"                                                >> $spec_prog_email_body
     printf "\n The Data Team\n\n"                                         >> $spec_prog_email_body

     # Register this new task action in maintenance table...
     db2 -v "INSERT INTO ${schema_name}.maint_bkfair_tasks \
             (task_id, create_timestamp, task_name, script_name) \
             VALUES ( (SELECT MAX(task_id) + 1 FROM ${schema_name}.maint_bkfair_tasks), \
                      '${exec_timestamp}', \
                      '${script_option} update in OH', \
                      '${script_name}' ) " | sed 's/  */ /g' >> $log

     printf "\n\n Data update for ONLINE_HOMEPAGE x2 ended on $(date)\n" >> $log

     db2 -v "terminate"  >> $log
     printf "\n\nThe Data Team.\n"  >> $log

          #--------------------------------------------------------
          #  Send Feedback email...
          #--------------------------------------------------------

     if [[ -e $error_file ]]; then
          mail -s "db2_task_failure -${host_name}-${app_env}-${subject_line} encountered problems - ${script_action}..." \
               "$mailout_list_error"  <  $error_file
          mail -s "db2_task_failure -${host_name}-${app_env}-${subject_line} - ${script_action}" \
               "$mailout_list"  <  $log
     else
          mail -s "db2_task_success -${host_name}-${app_env}-${subject_line} Successful - ${script_action}" \
               "$mailout_list"  <  $log
          mail -s "db2_task_success -${host_name}-${app_env}-${subject_line} Successful - ${script_action}" \
               "$mailout_list"  <  $spec_prog_email_body
     fi


     #################################################################
     #################################################################
     #     SECTION # 3:  CHECK FOR DUPLICATED URLs...
     #
     #        Note: Business wanted a permanent check for 
     #              potential creation of duplicated URLs 
     #              (or via sql, update a status backward)
     #              The only possibility for that 
     #              eventuality is for someone to create it
     #              manually via SQL as the duplication is
     #              not possible thanks to the trigger code.
     #################################################################
     #################################################################
     
elif [[ "${script_action}" == "duplicated_url_check" ]]; then

     subject_line="Bookfair Data Maintenance-${script_action}"

          #-----------------------------------------------------------
          # Sub-section which sets email subject lines and recipient
          # to Test-recipients when the option of "dev_test_run" has
          # been set as an  option parameter via -t.
          #-----------------------------------------------------------

     if [[ "${script_option}" == "dev_test_run" ]]; then
          fn_mail_test_setup
          subject_line="Test-${app_env}-Bookfair Data Maintenance-${script_action}"
     fi
     
     printf "\n All,"  >> $log
     printf "\n\n Per Business Request, here is the feedback from Bkfair"  >> $log
     printf "\n Toolkit letting you know about duplicated URLs status.\n"  >> $log

     db2 -v "connect to ${db_name}"  >>  $log
     fn_db2Check $? ${db_name}_CONNECT  DB2_CONNECT_CMD
     db2 -v "SET SCHEMA ${schema_name}"  >> $log
     fn_db2Check $? ${schema_name}  SET_SCHEMA_CMD
     exec_timestamp=$(db2 -x "SELECT CURRENT TIMESTAMP FROM sysibm.sysdummy1" )
     
          # Get the count of number of potential duplicated URLs...

     duplicate_url_count=$(db2 -x "SELECT TRIM(CHAR( COUNT(*) )) FROM ( \
                                      SELECT web_url, COUNT(*) Count \
                                         FROM bkfair01.homepage_url \
                                         WHERE url_status = 'RESERVED' \
                                         GROUP BY web_url HAVING COUNT(*) > 1 ) \
                                         FOR READ ONLY WITH UR ")

     if (( ${duplicate_url_count} > 0 )); then

          printf "\n All,"                                                       > $dup_url_email_body
          printf "\n\n Per Business Request, here is the feedback"              >> $dup_url_email_body
          printf "\n from the Bkfair Toolkit database letting you know that"    >> $dup_url_email_body
          printf "\n there were ${duplicate_url_count} duplicated URLs found"   >> $dup_url_email_body
          printf "\n in the database.\n\n"                                      >> $dup_url_email_body
               # Get more URL information for Business...
          db2 "SELECT web_url, COUNT(*) Count \
                  FROM bkfair01.homepage_url \
                  WHERE url_status = 'RESERVED' \
                  GROUP BY web_url HAVING COUNT(*) > 1  \
                  FOR READ ONLY WITH UR "                                       >> $dup_url_email_body
          printf "\n\n Regards,"                                                >> $dup_url_email_body
          printf "\n The Data Team\n\n"                                         >> $dup_url_email_body

               #----------------------------------
               #  Send Feedback email...
               #     Note: for Prod, add 2 names
               #----------------------------------

          if [[ "$db_name" == "bkf01p" ]]; then
               mail -s "db2_task_warning -${host_name}-${app_env}-${subject_line}-${script_action}" \
                       "$prod_mailout_list"  <  $dup_url_email_body
          else
               mail -s "db2_task_warning -${host_name}-${app_env}-${subject_line}-${script_action}" \
                       "$mailout_list"  <  $dup_url_email_body
          fi
     fi   ### (( ${duplicate_url_count} > 0 ))...

          # Register this new task action in maintenance table...
     db2 -v "INSERT INTO ${schema_name}.maint_bkfair_tasks \
                (task_id, create_timestamp, task_name, script_name) \
                VALUES ( (SELECT MAX(task_id) + 1 FROM ${schema_name}.maint_bkfair_tasks), \
                         '${exec_timestamp}', \
                         'Check for Duplicated URLs: ${duplicate_url_count}', \
                         '${script_name}.${script_action}' ) " | sed 's/  */ /g' >> $log

     printf "\n\n Duplicate URLs Check ended on $(date)\n" >> $log
     db2 -v "terminate"  >> $log

     if [[ -e $error_file ]]; then
          cat $log >> $error_file
          mail -s "db2_task_failure -${host_name}-${app_env}-${subject_line}-${script_action} encountered problems ..." \
               "$mailout_list_error"  <  $error_file
     fi


     #################################################################
     #################################################################
     #     SECTION # 4:  Future request
     #                   Note: this secion is towards the next cmd 
     #                         extension for future maintenance 
     #                         request...
     #################################################################
     #################################################################

#elif [[ "${script_action}" == "<new_script_action>" ]]; then
     
else
     printf "\n All,"  > $error_file
     printf "\n\t This script action is not available."      >> $error_file
     printf "\n\t Your script and command line were:"        >> $error_file
     printf "\n\t Script Name : $script_name"                >> $error_file
     printf "\n\t Command Line: $param_list_array"           >> $error_file
     printf "\n\t This script exited gracefully.\n"          >> $error_file
     printf "\n\n Regards,"                                  >> $error_file
     printf "\n The Data Team\n\n"                           >> $error_file
     mail -s "db2_task_failure -${host_name}-${app_env}-${subject_line} CMD was not correct" \
             "$mailout_list"  <  $error_file
     exit 2
fi  ##### if [[ "$script_action" == "..." ]]; then...

     # Clean up input data files older than 3 months...
find ${LOG_HOME} -mtime +90 -name "${app_env}_${script_name}__${script_action}*" -exec rm -f {} \;

exit 0
