#!/bin/bash
# File Name     : bkfair_cis_load.sh   
# Version       : V-36
# Remark        : n/a
#                 
# Notes         : 1- In the main script body, a variable name starting with "fn"
#                    indicates a call to a function.
#                 2- When that function-call is followed by other labels, these
#                    are parameters to the function.
#                 
# Purpose       : This script imports and exports files to be loaded into the 
#                 BookFair database very early on a daily basis.
#                 These files get compressed the following day.
#                 
# set -n        # Uncomment to check the script syntax without any execution
# set -x        # Uncomment to debug this shell script
# History       : mm/dd/yyy (DBA initials)
#                 10/23/2013 release 24 version V-18.1
#                    1- Inclusion of 2 new environments/databases for this script
#                         to be able to run: [ Bkfair02, UAT ].
#                    2- Add the function fn_mail to be able to prevent
#                       repetitive coding based on the environment we are in.
#                 
#                   11/01/2013 release 24 version V-19
#                      1- Add Test Product ID filter to export data concerning
#                         the file:
#                            - PROD_WORKSHOP_REWARDS_REPUBLISHED_HOMEPAGES_EXPORT
#                         ME, MM, MB
#                      2- Add Test Product ID filter to export data concerning
#                         the file:
#                            - PROD_VOLUNTEER_OPTIN
#                         ME, MM, MB
#                   
#                   11/12/2013 release 24 version V-20
#                      1- Consolidation and streamlining in bringing several
#                         scriptsinto this main single one using the parameter
#                         feature of this script to run them individually.
#                      2- Added a "-o" parameter for "option" which feature 
#                         allows a dba to run this script his/her own 
#                         environment once proper directories have been created.
#                   
#                   11/25/2013 release 24 version V-21
#                      1- Integration of the Bkfair Tax script
#                   
#                   12/09/2013 Release 24 version V-22
#                      1- Integration of the Bkfair Zipcode script
#                   
#                   02/19/2014 QC # 1886 before Release 24.5 V-23
#                      1- Modify the import of file PROD_TRANSACTION_CPW
#                      2- Modify the import of file PROD_TRANSACTION_OB_CPW
#                      This modification is to handle negative values from CRM
#                         provided between a pair of double_quotes.
#                   
#                   01/07/2014 Release 24.5 version V-25
#                      1- Additional File Import for PROD_PLANNER_WORKSHOPS.CPW
#                      2- Added 2 reorg and runstats to be run on a daily basis
#                           as a performance tuning task and in response to the 
#                           last performance testing.
#                      3- Replace the call to the parameter-less procedure 
#                         "s_previous_fairs" wih a call to parametized upgraded
#                         procedure "sp_previous_fairs" to reduce processing time
#                         by 95%.
#                      4- Correct syntax error
#                      5- Remove the command emptying the table "previous_fairs"
#                      
#                   05/22/2014 Release 25 (BTS 2014-2015) version V-27
#                      1- From within "Fourth Section" dubbed BKFAIR EMAIL_FORMS
#                         Per Beth T.(BA), remove data export PROD_COA_EXPORT
#                            using view V_EXPORT_COA_RPT as its data is being
#                            consumed directly via the Messaging implementation.
#                      2- Remove BA Elise from email list.
#                   
#                   07/07/2014 AWS Migration version V-28
#                      1- Change directory structure now set on Linux
#                      2- Replace all extensions of Message files from ".msg" to ".txt"
#                           because of their mishandlings in Outlook emails.
#                      
#                   09/08/2014 Release 25.3 BTS # 02 V-29-30
#                      1- Modify the email addresses to reflect the upgraded groups
#                      2- Create 2 new data imports
#                         2-1 For PROD_CALENDAR_TYPE_CPW
#                         2-2 For PROD_CALENDAR_CPW
#                      3- Add Checking for Calendar data import errors
#                         and sending data V-30
#                   
#                   10/02/2014 Release 25.3 BTS #2 V-31
#                      1- Force Calendar email data to lower case on both sides
#                         of the join to prevent data misses.
#                   
#                   10/14/2014 Release 25.5 Financials V-32 
#                      1- That release was cancelled.
#                   
#                   04/20/2015 Release 26.5 RSVP V-33
#                      1- Remove all code related to data load aiming at 
#                         populating LU_HOMEPAGE_EVENT table as that CIS import
#                         (PROD_EVENT_CPW)is being replaced with a new Administration section to
#                         support Events.
#                      2- Correct the prod email problem for OFE export in adding 
#                         the forgotten character "$" to "{log}"
#                      3- Remove every aspect of code dealing with Special Programs
#                   
#                   05/19/2015 Release BTS 2015-2016 V-34
#                      1- Add 3 columns to the import command 
#                         into the table BOOK_FAIR_DATA_LOAD:
#                              - prior_sales_amount
#                              - fair_number
#                              - ofe_sales_flag
#                   05/20/2015 Release BTS 2015-2016 V-35
#                      1- Added modification to the import command using
#                         the file QA_SCHL_CIS_CPW in putting the 3 new colums.
#                   
#                   07/13/2015 Prod-Support V-36
#                      1- Add Commands to get the ..._SCHL_files for taxes data
#                   
################################################################################
#                             VARIABLES SETUP
################################################################################
default_string="not_set"
host_name=$(hostname -s)
user_name=${LOGNAME}

exec_date=$(date +%Y%m%d)
exec_time=$(date +%H%M%S)
current_hour=$(date +%H)
   #Get the name of the currently running script and remove the ".sh" extension
script_name=$(basename $0 .sh)
param_list=$@
node_ip="10.12.0.53"
   # "app_env" tracks the different swimlanes:
   #    [ dev, dev02, qa, qa02, uat01, prod ]
app_env=""

   # Parameter Initialization...
hostname_env=${default_string}
db_name=${default_string}
schema_name=${default_string}
script_action=${default_string}
script_option=${default_string}

     #-- initialize import failure switch
     #-- edited by flim to capture malformed import columns
IMP_FAIL=0
CPW_FAILED_FILES=""

     # Other data initialization
new_export_exec_timestamp=""

     # Release 19.5
file_ofe_cancel_ct=0  # tracks the number of ofe fairs to be cancelled from file.
ofe_cancel_loop_ct=0  # tracks the number of ofe fairs actually updated.
cmd_success_ct=0
last_ofe_export_date=""

   # ftp zipcode related variables...
   # Note: Due to multiple swimlanes which are now available,
   #       the 2 file names below will always be prepended 
   #       with ${app_env} for differentiation purpose.
   
ftp_zipcode_ip="scewrissp01.scholastic.net"
ftp_zipcode_file="bkfair_ziplist5.txt"
application_zipcodes="formatted_lu_zipcodes.ixf"


################################################################################
#                             MAIL RELATED                                     #
################################################################################
     # Note: "temp_subject_line" is a global variable and always setup
     #       and represent a concatenation of objects for specific
     #       mail subject lines.
global_subject_line=""
subject_line="Bkfair Main Load Process "
subject_line_2="Dev Malformed Import CIS CPW files"
subject_line_3="QA Malformed Import CIS CPW files"
subject_line_4=" 1 or more CIS procedures encountered errors"
subject_line_success="Bkfair CIS load process completed"
prod_subject_line_failure="Prod Bkfair CIS Load Process Failed: 1 or more procedures encountered errors"
email_dev="mnaresh@scholastic.com,fcabaret@scholastic.com,ravala@scholastic.com,JDuvvuri-consultant@scholastic.com"
email_qa="ravala@Scholastic.com,fcabaret@scholastic.com,mnaresh@scholastic.com,psubramaniam-consultant@scholastic.com"
email_dev_run="mnaresh@scholastic.com,fcabaret@scholastic.com,ravala@scholastic.com,JDuvvuri-consultant@scholastic.com"
email_qa_run="eCommerceDatabaseAdministrators@scholastic.com,mnaresh@scholastic.com,fcabaret@scholastic.com,ravala@scholastic.com,psubramaniam-consultant@scholastic.com"
email_prod_support="eCommerceOperations@Scholastic.com,eCommerceDatabaseAdministrators@scholastic.com,MNaresh@scholastic.com,FCabaret@scholastic.com,BTracey@scholastic.com,LBrown@ScholasticBookfairs.com,MPCallahan@scholasticbookfairs.com"
email_prod="bthorgersen@scholastic.com,fcabaret@scholastic.com"
email_ofe="btracey@scholastic.com,dstrange"
email_cancel_ofes="btracey@scholastic.com,dstrange@scholasticbookfairs.com,acowan@scholasticbookfairs.com,fcabaret@scholastic.com"
     # The following email setup is towards running the script using
     #   the parameter option -o- set to "dev_test_run"
email_logname_test_run="fcabaret@scholastic.com"

################################################################################
#                             FILES                                            #
################################################################################

   # Set the Base Work Directory (for all environments)
export WORK_DIR='/backups'

     # Default "mail_body" location in case an error is encountered
     #   and to insure an email will be fired correctly...
     
mail_body=/tmp/${user_name}_${app_env}_bkfair_cis_mail_body.txt

   # Check that the db2 instance has been loaded on that machine...
   # The profile is loaded directly when the User logs in (Chef).
   # For that matter, any user must have his/her own profile upgraded.
   
if [[ -z ${DB2INSTANCE} ]]; then
   printf "\n\t All,"                                                >  ${mail_body}
   printf "\n\n\t The script -${script_name}- failed as"             >> ${mail_body}
   printf "\n\t the DB2 instance environment had not been loaded!."  >> ${mail_body}
   printf "\n\t Here follows your command line:"                     >> ${mail_body}
   printf "\n\t Script Name : $script_name"                          >> ${mail_body}
   printf "\n\t Command Line: $param_list"                           >> ${mail_body}
   printf "\n\t Please, contact the DB Admin group to install "      >> ${mail_body}
   printf "\n\t the DB2 instance;  Then, re-run the script if the ." >> ${mail_body}
   printf "\n\t time permits it."                                    >> ${mail_body}
   printf "\n\t This script exited gracefully."                      >> ${mail_body}
   printf "\n\n\t Thank you"                                         >> ${mail_body}
   printf "\n\t The Data Team\n"                                     >> ${mail_body}
   
   global_subject_line="db2_task_failure -${host_name}-${app_env} - ${subject_line} failed to run...instance not setup"
   mail -s "${global_subject_line}"  "${email_prod_support}" < ${mail_body}

   exit 1
fi


     #-----------------------------------------------------------
     # First call to the "getops" loop to JUST set up the
     #    "Environment" default values when appropriate.
     # All other parameters will be filtered during the
     #    second call.
     #
     # Note-1: The -e parameter value needs to be replaced 
     #         by the value setup into Chef when moving to AWS.
     # Note-2: This second call is a "light" version of the 
     #         functionalities encountered in the second call
     #         to "getops"; this light version is dynamically
     #         filtering the current environment [dev,qa,prod]
     #         using the database name pattern.
     #         The variable "app_env" will be finalized in the 
     #         second call.
     # Note-3: There is no function call from this section.
     #-----------------------------------------------------------
     
while getopts :e:d:s:a:o: param_item
do
     case $param_item in
          e)      # Environment setup...
                  #----------------------------------------------
                  # 1- DEV environemnt variables values setup
                  #----------------------------------------------
          
               if [[ "$OPTARG" == "dev" ]]; then
                    app_env="dev"
                    DATA_FILE_PREFIX="DEV"
               
                  #----------------------------------------------
                  # 2- QA environemnt variables values setup
                  #----------------------------------------------
               
               elif [[ "$OPTARG" == "qa" ]]; then
                    app_env="qa"
                    DATA_FILE_PREFIX="QA"
               
                  #----------------------------------------------
                  # 3- PROD environemnt variables values setup
                  #----------------------------------------------
               
               elif [[ "$OPTARG" == "prod" ]]; then
                    app_env="prod"
                    DATA_FILE_PREFIX="PROD"
               
               else
                    printf "\n\t All,"                                          >  ${mail_body}
                    printf "\n\n\t The script -${script_name}- failed to run"   >> ${mail_body}
                    printf "\n\t due to its Environment parameter "             >> ${mail_body}
                    printf "\n\t value -$OPTARG- which was not valid."          >> ${mail_body}
                    printf "\n\t Here follows your command line:"               >> ${mail_body}
                    printf "\n\t Script Name : $script_name"                    >> ${mail_body}
                    printf "\n\t Command Line: ${param_list}"                   >> ${mail_body}
                    printf "\n\t Please, resubmit your command with the"        >> ${mail_body}
                    printf "\n\t proper parameters."                            >> ${mail_body}
                    printf "\n\t This script exited gracefully."                >> ${mail_body}
                    printf "\n\n\t Thank you"                                   >> ${mail_body}
                    printf "\n\t The Data Team\n"                               >> ${mail_body}
                    
                    global_subject_line="db2_task_failure -${host_name}-${app_env}-${subject_line} failed to run...wrong environment"
                    mail -s "${global_subject_line}"  "${email_prod_support}" < ${mail_body}

                    exit 2
               fi
             ;;
          
          d)   if [[ "$OPTARG" == "bkf01d" ||
                     "$OPTARG" == "bkf02d" ||
                     "$OPTARG" == "bkf01q" ||
                     "$OPTARG" == "bkf02q" ||
                     "$OPTARG" == "bkf01u" ||
                     "$OPTARG" == "bkf01p"   ]]; then
                     
                    db_name=$OPTARG
                    LOG_HOME="${WORK_DIR}/logs/${LOGNAME}/${db_name}"
                    DB_HOME="${WORK_DIR}/data/${LOGNAME}/${db_name}"
                       # We capture the last character of the database name [d,q,u,p]
                    app_env=${db_name:$((${#db_name}-1)):1}
                    case ${app_env} in
                        'd') app_env='dev'
                             ;;
                        'q') app_env='qa'
                             ;;
                        'u') app_env='uat01'
                             ;;
                        'p') app_env='prod'
                             ;;
                    esac
                  
               else
                    printf "\n\t All,"                                          >  ${mail_body}
                    printf "\n\n\t The script -${script_name}- failed to run"   >> ${mail_body}
                    printf "\n\t due to its Database parameter "                >> ${mail_body}
                    printf "\n\t value -$OPTARG- which was not valid."          >> ${mail_body}
                    printf "\n\t Here follows your command line:"               >> ${mail_body}
                    printf "\n\t Script Name : $script_name"                    >> ${mail_body}
                    printf "\n\t Command Line: ${param_list}"                   >> ${mail_body}
                    printf "\n\t Please, resubmit your command with the"        >> ${mail_body}
                    printf "\n\t proper parameters."                            >> ${mail_body}
                    printf "\n\t This script exited gracefully."                >> ${mail_body}
                    printf "\n\n\t Thank you"                                   >> ${mail_body}
                    printf "\n\t The Data Team\n"                               >> ${mail_body}
                    
                    global_subject_line="db2_task_failure -${host_name}-${app_env}-${subject_line} failed to run...wrong database name"
                    mail -s "${global_subject_line}"  "${email_prod_support}" < ${mail_body}

                    exit 2
               fi
               
                  # Check if the Log and File directories are available...
               if [[ ! -w ${DB_HOME} ]] && 
                  [[ ! -w ${LOG_HOME} ]]; then
                    echo 'The output directories are not writable or does not exists!'
                    printf "\n\t All,"                                          >  ${mail_body}
                    printf "\n\n\t The script -${script_name}- failed to run"   >> ${mail_body}
                    printf "\n\t as either the LOG directory or:and the file"   >> ${mail_body}
                    printf "\n\t directory was:were not available."             >> ${mail_body}
                    printf "\n\n\t Please verify the environment structure and" >> ${mail_body}
                    printf "\n\t resubmit your script if time permits it."      >> ${mail_body}
                    printf "\n\n\t This script exited gracefully."              >> ${mail_body}
                    printf "\n\n\t Thank you"                                   >> ${mail_body}
                    printf "\n\t The Data Team\n"                               >> ${mail_body}
                    
                    global_subject_line="db2_task_failure -${host_name}-${app_env}-${subject_line} failed to run...directory not available"
                    mail -s "${global_subject_line}"  "${email_prod_support}" < ${mail_body}
                    exit 2
               fi
             ;;
          
          s) ;;
          
          a) ;;
              
          o) ;;
          
          \?) printf "\n\t All,"                                                >  ${mail_body}
              printf "\n\n\t The script -${script_name}- failed to run due"     >> ${mail_body}
              printf "\n\t to one of its parameters which was not valid."       >> ${mail_body}
              printf "\n\t Here follows your command line:"                     >> ${mail_body}
              printf "\n\t Script Name : $script_name"                          >> ${mail_body}
              printf "\n\t Command Line: $param_list"                           >> ${mail_body}
              printf "\n\t Please, resubmit your command with the"              >> ${mail_body}
              printf "\n\t proper parameters."                                  >> ${mail_body}
              printf "\n\t This script exited gracefully."                      >> ${mail_body}
              printf "\n\n\t Thank you"                                         >> ${mail_body}
              printf "\n\t The Data Team\n"                                     >> ${mail_body}
                    
              global_subject_line="db2_task_failure -${host_name}-${app_env} - ${subject_line} failed to run...wrong parameter"
              mail -s "${global_subject_line}"  "${email_prod_support}" < ${mail_body}

              exit 2
     esac
done

     # Reset the getops index...
OPTIND=1


################################################################################
#                             FUNCTIONS                                        #
################################################################################

#**************************** fn_mail ******************************************
# Purpose      : In case of system error, this function sends the email to a 
#                group which group depends on the environment we are running
#                this script in.
# Parameters   : p1  : <file_name_to_be_sent>  mandatory
#                      Note: used to filter on which file to grasp
#                            [ log, error_log, mail_body_in_tmp_directory ].
# Remark       : This function uses the global variable "global_subject_line"
#                which represents the subject line to be sent and which has been
#                assembled specifically for different events.
function fn_mail {
     fn1_file_name=$1

     if [[ "${app_env}" == "dev"  ||
           "${app_env}" == "dev02"  ]]; then

          if [[ "${fn1_file_name}" == "MAIL_BODY" ]]; then
               mail -s "${global_subject_line}"  "${email_dev_run}" < ${mail_body}
          
          elif [[ "${fn1_file_name}" == "ERROR_LOG" ]]; then
               mail -s "${global_subject_line}"  "${email_dev_run}" < ${error_file}
          
          elif [[ "${fn1_file_name}" == "LOG_FILE" ]]; then
               mail -s "${global_subject_line}"  "${email_dev_run}" < ${log}
          fi
          
     elif [[ "${app_env}" == "qa"   ||
             "${app_env}" == "qa02" ||
             "${app_env}" == "uat01"  ]];  then

          if [[ "${fn1_file_name}" == "MAIL_BODY" ]]; then
               mail -s "${global_subject_line}"  "${email_qa_run}" < ${mail_body}
          
          elif [[ "${fn1_file_name}" == "ERROR_LOG" ]]; then
               mail -s "${global_subject_line}"  "${email_qa_run}" < ${error_file}
          
          elif [[ "${fn1_file_name}" == "LOG_FILE" ]]; then
               mail -s "${global_subject_line}"  "${email_qa_run}" < ${log}
          fi
          
     elif [[ "${app_env}" == "prod" ]]; then

          if [[ "${fn1_file_name}" == "MAIL_BODY" ]]; then
               mail -s "${global_subject_line}"  "${email_prod_support}" < ${mail_body}
          
          elif [[ "${fn1_file_name}" == "ERROR_LOG" ]]; then
               mail -s "${global_subject_line}"  "${email_prod_support}" < ${error_file}
          
          elif [[ "${fn1_file_name}" == "LOG_FILE" ]]; then
               mail -s "${global_subject_line}"  "${email_prod_support}" < ${log}
          fi
     fi
}


#**************************** fn_db2Check **************************************
# Purpose      : This function checks the proper output of a db2 command.
#                Note: Based on the release to be executed, the "exit 2"
#                      command from within the function "fn_db2_check" can
#                      be uncommented for the desired result.
#                
# Parameters   : p1  : <exit_code>     mandatory
#                p2  : <object_type>   optional
#                p3  : <object_name>   optional
#
# Remarks      : This function, as it is, will not make the script exit.
#                If you want it to, you need to code additionally for it
#                to take the proper ensuing action.
#        
function fn_db2Check {
     exit_code=$1
     fn_object_name=$2
     fn_object_action=$3
     fn_object_option=$4

     if (( $exit_code == 0 )); then
               # everything ran fine... and we don't do anything.
          :
     elif (( $exit_code == 1 )); then
               # A "select" or "fetch" returned no rows...
          printf " \n\t Error code --$exit_code-- was issued"                 | tee -a $error_file
          printf " \n\t while processing object name: --${fn_object_name}--"  | tee -a $error_file
          printf " \n\t and actionning a: --${fn_object_action}--"            | tee -a $error_file
          printf " \n\t Please review the error log and take appropriate"     | tee -a $error_file
          printf " \n\t action if necessary.\n"                               | tee -a $error_file

     elif (( $exit_code == 2 )); then
               # A db2 command or sql statement warning occured...
          printf " \n\t A WARNING --$exit_code-- occured"                     | tee -a $error_file
          printf " \n\t while processing object name: --${fn_object_name}--"  | tee -a $error_file
          printf " \n\t and actionning a: --${fn_object_action}--"            | tee -a $error_file
          printf " \n\t e.g. that object may have not existed "               | tee -a $error_file
          printf " \n\t or"                                                   | tee -a $error_file
          printf " \n\t a PK constraint has been named after"                 | tee -a $error_file
          printf " \n\t      an existing unique index."                       | tee -a $error_file
          printf " \n\t Please review the error log and take appropriate"     | tee -a $error_file
          printf " \n\t action if necessary.\n"                               | tee -a $error_file

     elif (( $exit_code == 4 )); then
               # A db2 or SQL statement error occured...
          printf " \n\t An ERROR --$exit_code-- occured while processing"     | tee -a $error_file
          printf " \n\t object name: --${fn_object_name}--"                   | tee -a $error_file
          printf " \n\t and actionning a: --${fn_object_action}--"            | tee -a $error_file
          printf " \n\t Please review the error log and take appropriate"     | tee -a $error_file
          printf " \n\t action if necessary.\n"                               | tee -a $error_file
          printf " \n\t As the script exited with an error, it will be "      | tee -a $error_file
          printf " \n\t necessary to run the left over SQL manually from"     | tee -a $error_file
          printf " \n\t where the script failed."                             | tee -a $error_file
          
          if [[ "$fn_object_action" == "DB2_CONNECT_CMD" ]]; then
               printf "\n\n\t The source of the problem was an impossibility" | tee -a $error_file
               printf "\n\t to connect to database ${db_name} to export data."| tee -a $error_file
               printf "\n\t Therefore the script exited without"              | tee -a $error_file
               printf "\n\t processing any data.\n\n"                         | tee -a $error_file
          
          elif [[ "$fn_object_action" == "EXPORT_CMD" ]]; then
               printf "\n\n\t There was a problem exporting data from "       | tee -a $error_file
               printf "\n\t table ${fn_object_name}."                         | tee -a $error_file
               # There is no need to terminate to see outcome of all export commands.
          fi
          
          printf " \n\n\t Thank you"                                          | tee -a $error_file
          printf " \n\t The Data Team.\n\n"                                   | tee -a $error_file
          db2 terminate >> $error_file
          
               # Concatenate the log file to the error file which error file will be sent.
               # The error will come as the header and the Reader will have to go to the 
               # bottom of the email to see where the script ended.
               
          cat $log      >> $error_file
          global_subject_line="db2_task_failure-${host_name}-${app_env}-${subject_line}-${script_action} encountered problems...exit 4"
          fn_mail "ERROR_LOG"
          exit 4

     elif (( $exit_code == 8 )); then
          fn_set_task_end_timestamp  Internal_OS_Error
               # A command line processor system error occured...
          printf " \n\t A SYSTEM ERROR --$exit_code-- occured \n"             | tee -a $error_file
          printf " \n\t while processing object name: --${fn_object_name}--"  | tee -a $error_file
          printf " \n\t and actionning a: --${fn_object_action}--"            | tee -a $error_file
          printf " \n\t Please review the error log and take appropriate"     | tee -a $error_file
          printf " \n\t action if necessary.\n"                               | tee -a $error_file
          printf " \n\t As the script exits with an error, it will be "       | tee -a $error_file
          printf " \n\t necessary to run the left over SQL manually from"     | tee -a $error_file
          printf " \n\t where the script failed."                             | tee -a $error_file
          printf " \n\n\t Thank you"                                          | tee -a $error_file
          printf " \n\t The Data Team.\n\n"                                   | tee -a $error_file
          db2 terminate >> $error_file
          
          global_subject_line="db2_task_failure-${host_name}-${app_env}-${subject_line}-${script_action} encountered problems...exit 8"
          fn_mail "ERROR_LOG"
          global_subject_line="db2_task_failure-${host_name}-${app_env}-${subject_line}-${script_action}...exit 8"
          fn_mail "LOG_FILE"
          exit 8
     fi
}     ### fn_db2Check


#**************************** fn_import_process_log ****************************
# Purpose : We build the "-a" mail command section (attachment section)
#              which will gather all possible failed import files.
#           The resulted variable will then be passed as is to the
#              mail command to send the message files through.
function fn_import_process_log {
     if [ $1 -ne 0 ]; then
          CPW_FAILED_FILES="$CPW_FAILED_FILES -a $2"
          IMP_FAIL=1
     fi
}     ### fn_import_process_log


#**************************** fn_process_message_files *************************
# Purpose : This function sets the encoded filename used to capture the 
#           import files sent with emails showing potential errors.
#           When additional swimlanes are used the variable ${app_env} allows
#           for their differentiation when creating the message file.
#           The variable ${LOGNAME} allows to run the script in a user
#           respective environment.
function fn_process_message_files {

     if [[ "$1" == "cis_load" ]]; then
          BKF_ENCODE=/tmp/${user_name}_${app_env}_bkfair_cis.tmp.encode
     
     elif [[ "$1" == "rewards_load" ]]; then
          BKF_ENCODE=/tmp/${user_name}_${app_env}_bkfair_rewards_load.tmp.encode
     
     elif [[ "$1" == "tax_load" ]]; then
          BKF_ENCODE=/tmp/${user_name}_${app_env}_bkfair_tax_load.tmp.encode
     fi
     
     touch $BKF_ENCODE
     #rm $BKF_ENCODE
     #for attch in $CPW_FAILED_FILES
     #do
     #     uuencode $attch $attch >> $BKF_ENCODE
     #done
}     ### fn_process_message_files


#**************************** fn_ftp_connection_check **************************
# Purpose : This function checks if we have lost any ftp connection once we
#           got or put the files.
#           Based on the action, we either stop the script and report via email 
#           or we send an email.
function fn_ftp_connection_check {
   
   fn_ret_code=$1
   fn_object_name=$2
   fn_object_action=$3
   fn_ftp_connect=0
   fn_ftp_loss=0
   
      #-----------------------------------------------------
      # This section addresses credentials entered 
      # in netrc file towards establishing a connection...
      #-----------------------------------------------------

   fn_ftp_connect=$(grep '530 User cannot log in'  ${log} | wc -l | sed 's/  *//')
   
   if (( ${fn_ftp_connect} > 0 )) ; then
      printf "\n\n\t There was a failure connecting"        | tee -a ${error_file}
      printf "\n\t to the ftp site ${node_ip}."             | tee -a ${error_file}
      printf "\n\t None of the files was "                  | tee -a ${error_file}
      printf "\n\t processed and this script exited "       | tee -a ${error_file}
      printf "\n\t without any data changes."               | tee -a ${error_file}
      printf "\n\t Please review username and password."    | tee -a ${error_file}
      printf "\n\t The script name is   : ${script_name}"   | tee -a ${error_file}
      printf "\n\t The host name is     : ${host_name}\n"   | tee -a ${error_file}
      pringf "\n\t The script action is : ${script_action}" | tee -a ${error_file}
      printf "\n\t Thank you."                              | tee -a ${error_file}
      printf "\n\t The Data Team.\n\n"                      | tee -a ${error_file}
      
      cat ${log} >> ${error_file}
      
      if [[ "${app_env}" == "dev"  ||
            "${app_env}" == "dev02"  ]]; then
         mail -s "db2_task_failure-${host_name}-${app_env}-${subject_line}-${script_action} FTP connect failed" \
                 "$email_dev"  <  ${error_file}
      
      elif [[ "${app_env}" == "qa"   ||
              "${app_env}" == "qa02" ||
              "${app_env}" == "uat01"  ]]; then
         mail -s "db2_task_failure-${host_name}-${app_env}-${subject_line}-${script_action} FTP connect failed" \
                 "$email_qa_run"  <  ${error_file}
      
      elif [[ "${app_env}" == "prod" ]]; then
         mail -s "db2_task_failure-${host_name}-${app_env}-${subject_line}-${script_action} FTP connect failed" \
                 "${email_prod_support}"  <  ${error_file}
      fi   ### if [[ ${app_env} == "dev" ]]...
      exit 2
   fi   ### if (( ${fn_ftp_connect} > 0 ))...
   
      #----------------------------------------
      # This section addresses potential 
      # connection loss once connected...
      #----------------------------------------
   
   ftp_connect_check=$(grep '421 Service not available'  ${log} | wc -l | sed 's/  *//')

      #----------------------------------------
      # First, sub-section checking connection 
      # loss while GETTING the files...
      #----------------------------------------

   if (( ${ftp_connect_check} == 1 )); then
      if [[ ${fn_object_name} == "cis_ftp" ]] &&
         [[ ${fn_object_action} == "GET_FILES" ]]; then
         printf " \n\t An FTP connection loss occured while"                 | tee -a $error_file
         printf " \n\t getting files FROM --${fn_object_name}--"             | tee -a $error_file
         printf " \n\t and actionning a: --${fn_object_action}--;"           | tee -a $error_file
         printf " \n\t Review the error log and take appropriate"            | tee -a $error_file
         printf " \n\t action as we did not have access to all the files.\n" | tee -a $error_file
         printf " \n\t As a precaution, the script --${script_name}--"       | tee -a $error_file
         printf " \n\t using action parameter --${script_action}--"          | tee -a $error_file
         printf " \n\t was stopped without any further processing."          | tee -a $error_file
         printf " \n\t None of the Bookfair data has been loaded."           | tee -a $error_file
         printf " \n\t The script name is     : ${script_name}"              | tee -a $error_file
         printf " \n\t The host name is       : ${host_name}\n"              | tee -a $error_file
         printf " \n\t The script action is   : ${script_action}"            | tee -a $error_file
         printf " \n\t The FTP action is      : ${fn_object_action}"         | tee -a $error_file
         printf " \n\n\t Thank you."                                         | tee -a $error_file
         printf " \n\t The Data Team.\n\n"                                   | tee -a $error_file
         
         cat ${log} >> ${error_file}
         
         if [[ "${app_env}" == "dev"  ||
               "${app_env}" == "dev02"  ]]; then
            mail -s "db2_task_failure-${host_name}-${app_env}-${subject_line}-${script_action} FTP Connection Loss getting files" \
                    "$email_dev"  <  ${error_file}
         
         elif [[ "${app_env}" == "qa"   ||
                 "${app_env}" == "qa02" ||
                 "${app_env}" == "uat01"  ]]; then
            mail -s "db2_task_failure-${host_name}-${app_env}-${subject_line}-${script_action} FTP Connection Loss getting files" \
                    "$email_qa_run"  <  ${error_file}
         
         elif [[ "${app_env}" == "prod" ]]; then
            mail -s "db2_task_failure-${host_name}-${app_env}-${subject_line}-${script_action} FTP Connection Loss getting files" \
                    "${email_prod_support}"  <  ${error_file}
         fi
         exit 4
         
         #----------------------------------------
         # Second, sub-section checking connection 
         # loss while UPLOADING the files...
         #----------------------------------------
         
      elif [[ ${fn_object_name} == "cis_ftp" ]] &&
           [[ ${fn_object_action} == "PUT_FILES" ]]; then
         printf " \n\t An FTP connection loss occured while"                 | tee -a $error_file
         printf " \n\t putting files TO --${fn_object_name}--"               | tee -a $error_file
         printf " \n\t and actionning a: --${fn_object_action}--;\n"         | tee -a $error_file
         printf " \n\t While the script loaded all the files, the loss of"   | tee -a $error_file
         printf " \n\t connection prevented the script from uploaded "       | tee -a $error_file
         printf " \n\t the resulting files to -${FTP_HOME}- directory.\n"    | tee -a $error_file
         printf " \n\t Review the log files for further problems and take "  | tee -a $error_file
         printf " \n\t corrective action such as uploading"                  | tee -a $error_file
         printf " \n\t the files manually."                                  | tee -a $error_file
         printf " \n\t As a precaution, the script --${script_name}--"       | tee -a $error_file
         printf " \n\t was stopped without any further processing."          | tee -a $error_file
         printf " \n\t The script name is     : ${script_name}"              | tee -a $error_file
         printf " \n\t The host name is       : ${host_name}"                | tee -a $error_file
         printf " \n\t The script action is   : ${script_action}"            | tee -a $error_file
         printf " \n\t The FTP action is      : ${fn_object_action}"         | tee -a $error_file
         printf " \n\n\t Thank you."                                         | tee -a $error_file
         printf " \n\t The Data Team.\n\n"                                   | tee -a $error_file
         
         cat ${log} >> ${error_file}
         
         if [[ "${app_env}" == "dev"  ||
               "${app_env}" == "dev02"  ]]; then
            mail -s "db2_task_failure-${host_name}-${app_env}-${subject_line}-${script_action} FTP Connection Loss uploading files" \
                    "$email_dev"  <  ${error_file}
         
         elif [[ "${app_env}" == "qa"   ||
                 "${app_env}" == "qa02" ||
                 "${app_env}" == "uat01"  ]]; then
            mail -s "db2_task_failure-${host_name}-${app_env}-${subject_line}-${script_action} FTP Connection Loss uploading files" \
                    "$email_qa_run"  <  ${error_file}
         
         elif [[ "${app_env}" == "prod" ]]; then
            mail -s "db2_task_failure-${host_name}-${app_env}-${subject_line}-${script_action} FTP Connection Loss uploading files" \
                    "${email_prod_support}"  <  ${error_file}
         fi
         exit 4
      fi
   fi
}     ### fn_ftp_connection_check


#**************************** fn_mail_test_setup *******************************
# Purpose      : This function sets all email destination to the username
#                of the User running this script when passing the option 
#                parameter "test_dev_run"
# Parameters   : none
# Remark       : This function is called from the "-o" script parameter
#                section.  The DBA running that option will have created
#                proper directories to mimick the Dev/QA/Prod ones
function fn_mail_test_setup {
      # Regarding cis_main_load emails setup...
   email_dev="${email_logname_test_run}"
   email_qa="${email_logname_test_run}"
   email_dev_run="${email_logname_test_run}"
   email_qa_run="${email_logname_test_run}"
   email_prod_support="${email_logname_test_run}"
   email_prod="${email_logname_test_run}"
   email_ofe="${email_logname_test_run}"
   email_cancel_ofes="${email_logname_test_run}"

      # Regarding Hourly_ofe emails setup...
   mailout_list="${email_logname_test_run}"
   mailout_list_error="${email_logname_test_run}"
   
      # Regarding Email-Forms emails setup...
   dev_mailout_list="${email_logname_test_run}"
   qa_mailout_list="${email_logname_test_run}"
   prod_mailout_list="${email_logname_test_run}"
   
      # Regarding Zipcode emails setup...
   mail_report_list="${email_logname_test_run}"
}     ### end --> fn_mail_test_setup


#**************************** fn_directory_cleanup *****************************
# Purpose      : This function cleans up old files using the set of respective 
#                directories created for each each environment based on 
#                the database name.
#                The deletion makes use of global variables used
#                in patterned-file-names (standardized).
# Parameters   : None

function fn_directory_cleanup {
     find ${LOG_HOME} -mtime +33 -name "${app_env}_${script_name}__${script_action}*" -exec rm -f {} \;
     find ${DB_HOME} -mtime +33 -name "${DATA_FILE_PREFIX}*" -exec rm -f {} \;
}     ### fn_directory_cleanup


################################################################################
#                             MAIN                                             #
################################################################################

     # Remark: At this point, either the log or the error file
     #         have not been used yet; so, it is ok to relocate
     #         them down below if we are working in bkfair02 or UAT.
     
while getopts :e:d:s:a:o: param_item
do
     case $param_item in
          e)        # Environment setup (hostname) and remote...
               if [[ "$OPTARG" == "dev" ]]; then
                    hostname_env=$OPTARG
               
               elif [[ "$OPTARG" == "qa" ]]; then
                    hostname_env=$OPTARG
               
               elif [[ "$OPTARG" == "prod" ]]; then
                    hostname_env=$OPTARG
               
               else
                    printf "\n\t This environment -$OPTARG- and "               >  ${mail_body}
                    printf "\n\t -${host_name}- is not available."              >> ${mail_body}
                    printf "\n\t Your script and command line were:"            >> ${mail_body}
                    printf "\n\t Script Name : $script_name"                    >> ${mail_body}
                    printf "\n\t Command Line: ${param_list}"                   >> ${mail_body}
                    printf "\n\t Please, resubmit your command with the"        >> ${mail_body}
                    printf "\n\t proper parameters."                            >> ${mail_body}
                    printf "\n\t This script exited gracefully."                >> ${mail_body}
                    printf "\n\n\t Thank you"                                   >> ${mail_body}
                    printf "\n\t The Data Team\n"                               >> ${mail_body}
                    
                    global_subject_line="db2_task_failure -${host_name}-${app_env}-${subject_line} failed to run...wrong environment"
                    fn_mail "MAIL_BODY"
                    exit 2
               fi
               ;;

               # if-01
          d)   if [[ "$OPTARG" == "bkf01d" ||
                     "$OPTARG" == "bkf02d" ||
                     "$OPTARG" == "bkf01q" ||
                     "$OPTARG" == "bkf02q" ||
                     "$OPTARG" == "bkf01u" ||
                     "$OPTARG" == "bkf01p"   ]]; then
                    db_name=$OPTARG
                    FTP_ZIPCODE_HOME=/build/Sps_zipcodes
                    
                         # Set a different work path when dev/qa databases
                         #   bkfair01/bkfruat are being used...
                         # Reminder_1: bkf02x exists in both DEV and QA
                         # Reminder_2: bkf01u exists only in QA
                    
                    export LOG_HOME="${WORK_DIR}/logs/${LOGNAME}/${db_name}"          # log files directory
                    export DB_HOME="${WORK_DIR}/data/${LOGNAME}/${db_name}"           # data files directory
                    
                    # if-02
                    if [[ "$db_name" == "bkf01d" ]] || 
                       [[ "$db_name" == "bkf01q" ]] || 
                       [[ "$db_name" == "bkf01p" ]];   then
                         FTP_HOME=SIG_BookFairs
                    
                    elif [[ "$db_name" == "bkf02d" ]] || 
                         [[ "$db_name" == "bkf02q" ]];   then
                         FTP_HOME=SIG_BookFairs/SWIMLANE_2/
                         
                              # Set the app_env and prefix variable anew
                              #   based on whether we are in DEV or QA
                              #   and relocate the log files accordingly...
                              #
                              # Either, Address bkf02x db in DEV...
                         
                         # if-03
                         if [[ "$db_name" == "bkf02d" ]]; then
                              app_env="dev02"
                              DATA_FILE_PREFIX="DEV02"
                         
                              # OR, Address bkf02x db in QA...
                         
                         elif [[ "$db_name" == "bkf02q" ]]; then
                              app_env="qa02"
                              DATA_FILE_PREFIX="QA02"
                         fi   ### if-03-end
                         
                         # Address bkf01u db only in QA...
                    
                    elif [[ "$db_name" == "bkf01u" ]]; then
                         FTP_HOME=SIG_BookFairs/UAT_PERF/
                         
                              # Address bkfruat db only in QA checking once more the app_env...
                         
                         app_env="uat01"
                         DATA_FILE_PREFIX="UAT"
                         
                         # This "else" is to prevent to run this script
                         #  in Prod against the wrong database...
                    else
                         printf "\n\t The combination database ${db_name}"      >  ${mail_body}
                         printf "\n\t and environment ${host_name}"             >> ${mail_body}
                         printf "\n\t is not available."                        >> ${mail_body}
                         printf "\n\t Your script and command line were:"       >> ${mail_body}
                         printf "\n\t Script Name : $script_name"               >> ${mail_body}
                         printf "\n\t Command Line: $param_list"                >> ${mail_body}
                         printf "\n\t Please, resubmit your command with the"   >> ${mail_body}
                         printf "\n\t proper parameters."                       >> ${mail_body}
                         printf "\n\t This script exited gracefully."           >> ${mail_body}
                         printf "\n\n\t Thank you"                              >> ${mail_body}
                         printf "\n\t The Data Team\n"                          >> ${mail_body}
                         
                         global_subject_line="db2_task_failure -${host_name} - ${subject_line} failed to run... wrong db."
                         fn_mail  "MAIL_BODY"
                         exit 2
                    fi   ### if-02-end
               else
                    printf "\n\t This database --$OPTARG--is not available."    >  ${mail_body}
                    printf "\n\t Your script and command line were:"            >> ${mail_body}
                    printf "\n\t Script Name : $script_name"                    >> ${mail_body}
                    printf "\n\t Command Line: $param_list"                     >> ${mail_body}
                    printf "\n\t Please, resubmit your command with the"        >> ${mail_body}
                    printf "\n\t proper parameters."                            >> ${mail_body}
                    printf "\n\t This script exited gracefully."                >> ${mail_body}
                    printf "\n\n\t Thank you"                                   >> ${mail_body}
                    printf "\n\t The Data Team\n"                               >> ${mail_body}
                    
                    global_subject_line="db2_task_failure -${host_name}-${app_env} - ${subject_line} failed to run... wrong db."
                    fn_mail "MAIL_BODY"
                    exit 2
               fi   ### if-01-end
               ;;

          s)        # 
               if [[ "$OPTARG" == "BKFAIR01" ]]; then
                    schema_name=$OPTARG
               else
                    printf "\n\t This schema --$OPTARG--is not available."      >  ${mail_body}
                    printf "\n\t Your script and command line were:"            >> ${mail_body}
                    printf "\n\t Script Name : $script_name"                    >> ${mail_body}
                    printf "\n\t Command Line: $param_list"                     >> ${mail_body}
                    printf "\n\t Please, resubmit your command with the"        >> ${mail_body}
                    printf "\n\t proper parameters."                            >> ${mail_body}
                    printf "\n\t This script exited gracefully."                >> ${mail_body}
                    printf "\n\n\t Thank you"                                   >> ${mail_body}
                    printf "\n\t The Data Team\n"                               >> ${mail_body}
                    
                    global_subject_line="db2_task_failure -${host_name}-${app_env}-${subject_line} failed to run... wrong schema"
                    fn_mail "MAIL_BODY"
                    exit 2
               fi
               ;;

          a)   if [[ "$OPTARG" == "data_load"      ||
                     "$OPTARG" == "secondary_load" ||
                     "$OPTARG" == "hourly_ofe"     ||
                     "$OPTARG" == "email_forms"    ||
                     "$OPTARG" == "rewards_load"   ||
                     "$OPTARG" == "tax_load"       ||
                     "$OPTARG" == "zipcode_load"     ]]; then
                    script_action=$OPTARG
                    
                         # This sub-section filters the different parameter 
                         #   action values which allows for running the 
                         #   proper script section.
                         #   All Action parameter values represents the number
                         #   of all scripts that have been regrouped under this 
                         #   one as of Winter 2013.
                         
                    if [[ "${script_action}" == "data_load" ]]; then
                         log=${LOG_HOME}/${app_env}_${script_name}__${script_action}_${exec_date}_${exec_time}.log
                         error_file=${LOG_HOME}/${app_env}_${script_name}__${script_action}__error_${exec_date}_${exec_time}.log
                         email_feedback_body=${LOG_HOME}/data_load_email_feedback_body.txt
                         calendar_error_email=${LOG_HOME}/calendar_error_email_body.txt
                    
                    elif [[ "${script_action}" == "secondary_load" ]]; then
                         log=${LOG_HOME}/${app_env}_${script_name}__${script_action}_${exec_date}_${exec_time}.log
                         error_file=${LOG_HOME}/${app_env}_${script_name}__${script_action}__error_${exec_date}_${exec_time}.log
                         email_feedback_body=${LOG_HOME}/secondary_load_email_feedback_body.txt
                    
                    elif [[ "${script_action}" == "hourly_ofe" ]]; then
                         log=${LOG_HOME}/${app_env}_${script_name}__${script_action}_${exec_date}_${exec_time}.log
                         error_file=${LOG_HOME}/${app_env}_${script_name}__${script_action}__error_${exec_date}_${exec_time}.log
                         email_feedback_body=${LOG_HOME}/hourly_ofe_email_feedback_body.txt
                         
                    elif [[ "${script_action}" == "email_forms" ]]; then
                         log=${LOG_HOME}/${app_env}_${script_name}__${script_action}_${exec_date}_${exec_time}.log
                         error_file=${LOG_HOME}/${app_env}_${script_name}__${script_action}__error_${exec_date}_${exec_time}.log
                         email_feedback_body=${LOG_HOME}/email_forms_email_feedback_body.txt
                         
                    elif [[ "${script_action}" == "rewards_load" ]]; then
                         log=${LOG_HOME}/${app_env}_${script_name}__${script_action}_${exec_date}_${exec_time}.log
                         error_file=${LOG_HOME}/${app_env}_${script_name}__${script_action}__error_${exec_date}_${exec_time}.log
                         email_feedback_body=${LOG_HOME}/rewards_load_email_feedback_body.txt
                    
                    elif [[ "${script_action}" == "tax_load" ]]; then
                         log=${LOG_HOME}/${app_env}_${script_name}__${script_action}_${exec_date}_${exec_time}.log
                         error_file=${LOG_HOME}/${app_env}_${script_name}__${script_action}__error_${exec_date}_${exec_time}.log
                         email_feedback_body=${LOG_HOME}/tax_load_email_feedback_body.txt
                    
                    elif [[ "${script_action}" == "zipcode_load" ]]; then
                         log=${LOG_HOME}/${app_env}_${script_name}__${script_action}_${exec_date}_${exec_time}.log
                         error_file=${LOG_HOME}/${app_env}_${script_name}__${script_action}__error_${exec_date}_${exec_time}.log
                         email_feedback_body=${LOG_HOME}/zipcode_load_email_feedback_body.txt
                    fi
               
               else
                    printf "\n\t This action --$OPTARG--is not allowed."        >  ${mail_body}
                    printf "\n\t Your script and command line were:"            >> ${mail_body}
                    printf "\n\t Script Name : $script_name"                    >> ${mail_body}
                    printf "\n\t Command Line: $param_list"                     >> ${mail_body}
                    printf "\n\t Please, resubmit your command with the"        >> ${mail_body}
                    printf "\n\t proper parameters."                            >> ${mail_body}
                    printf "\n\t This script exited gracefully."                >> ${mail_body}
                    printf "\n\n\t Thank you"                                   >> ${mail_body}
                    printf "\n\t The Data Team\n"                               >> ${mail_body}
                    
                    global_subject_line="db2_task_failure -${host_name}-${app_env} - ${subject_line} failed to run... wrong action"
                    fn_mail "MAIL_BODY"
                    exit 2
              fi
              ;;
              
          o)   if [[ "$OPTARG" == "dev_test_run" ]]; then
                    script_option=$OPTARG
                    fn_mail_test_setup
                    
               else
                    printf "\n\t This option --$OPTARG--is not allowed."        >  ${mail_body}
                    printf "\n\t Your script and command line were:"            >> ${mail_body}
                    printf "\n\t Script Name : $script_name"                    >> ${mail_body}
                    printf "\n\t Command Line: $param_list"                     >> ${mail_body}
                    printf "\n\t Please, resubmit your command with the"        >> ${mail_body}
                    printf "\n\t proper parameters."                            >> ${mail_body}
                    printf "\n\t This script exited gracefully."                >> ${mail_body}
                    printf "\n\n\t Thank you"                                   >> ${mail_body}
                    printf "\n\t The Data Team\n"                               >> ${mail_body}
                    
                    global_subject_line="db2_task_failure -${host_name}-${app_env} - ${subject_line}-${script_action} failed to run... wrong option"
                    fn_mail "MAIL_BODY"
                    exit 2
               fi
               ;;
          
          \?) printf "\n\t You provided an option that was not available. "     >  ${mail_body}
              printf "\n\t Your script and command line were:"                  >> ${mail_body}
              printf "\n\t Script Name : $script_name"                          >> ${mail_body}
              printf "\n\t Command Line: $param_list"                           >> ${mail_body}
              printf "\n\t Please, resubmit your command with the"              >> ${mail_body}
              printf "\n\t proper parameters."                                  >> ${mail_body}
              printf "\n\t This script exited gracefully."                      >> ${mail_body}
              printf "\n\n\t Thank you"                                         >> ${mail_body}
              printf "\n\t The Data Team\n"                                     >> ${mail_body}
                    
              global_subject_line="db2_task_failure -${host_name}-${app_env} - ${subject_line} failed to run...wrong parameter"
              fn_mail "MAIL_BODY"
              exit 2
     esac
done

cd ${DB_HOME} # Set the Current Directory towards File being FTPed...

     #################################################################
     #################################################################
     #    FIRST SECTION: MAIN BKFAIR DATA LOAD...
     #################################################################
     #################################################################
     
     
if [[ "${script_action}" == "data_load" ]]; then
     
          #-----------------------------------------------------------
          # Subsection which sets email subject lines and recipient to
          # Test-recipients when the option of "dev_test_run" has been
          # set as an  option parameter.
          #-----------------------------------------------------------

     subject_line="${app_env}-Bkfair Main Load Process "
     subject_line_2="${app_env}-Malformed Import CIS CPW files"
     subject_line_3="${app_env}-Malformed Import CIS CPW files"
     subject_line_4="${app_env}-1 or more CIS procedures encountered errors"
     subject_line_success="${app_env}-Bkfair CIS load process completed"
     prod_subject_line_failure="${app_env}-Bkfair CIS Load Process Failed: 1 or more procedures encountered errors"

     if [[ "${script_option}" == "dev_test_run" ]]; then
          fn_mail_test_setup
          global_subject_line=""
          subject_line="Test-${app_env}-Bkfair Main Load Process "
          subject_line_2="Test-${app_env}-Malformed Import CIS CPW files"
          subject_line_3="Test-${app_env}-Malformed Import CIS CPW files"
          subject_line_4="Test-${app_env}-1 or more CIS procedures encountered errors"
          subject_line_success="Test-${app_env}-Bkfair CIS load process completed"
          prod_subject_line_failure="Test-${app_env}-Bkfair CIS Load Process Failed: 1 or more procedures encountered errors"
     fi
     
     echo "----------------------------------------------------------"  >> $log
     echo "  Starting Bkfair Main Data Load..."                         >> $log
     echo "  ...Archiving of yesterday CPW files at $(date) ..."        >> $log
     echo "----------------------------------------------------------"  >> $log
     
     mv ${DATA_FILE_PREFIX}_CIS_CPW \
        ${DATA_FILE_PREFIX}_CIS_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_CIS_CPW_prev.${exec_date}_${exec_time}
     
     mv ${DATA_FILE_PREFIX}_SCHL_CIS_CPW \
        ${DATA_FILE_PREFIX}_SCHL_CIS_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_SCHL_CIS_CPW_prev.${exec_date}_${exec_time}
     
     mv ${DATA_FILE_PREFIX}_PROFIT_BALANCE_CPW \
        ${DATA_FILE_PREFIX}_PROFIT_BALANCE_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_PROFIT_BALANCE_CPW_prev.${exec_date}_${exec_time}
     
     mv ${DATA_FILE_PREFIX}_SCHL_PROFIT_BALANCE_CPW \
        ${DATA_FILE_PREFIX}_SCHL_PROFIT_BALANCE_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_SCHL_PROFIT_BALANCE_CPW_prev.${exec_date}_${exec_time}
     
          #-----------------------------------
          #       Bookfair Release 8
          #-----------------------------------
     mv ${DATA_FILE_PREFIX}_HP_MESSAGE_CPW \
        ${DATA_FILE_PREFIX}_HP_MESSAGE_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_HP_MESSAGE_CPW_prev.${exec_date}_${exec_time}
     
          #-----------------------------------
          #       Bookfair Release 8.5
          #-----------------------------------
     
     mv ${DATA_FILE_PREFIX}_PERCENTAGE_CPW \
        ${DATA_FILE_PREFIX}_PERCENTAGE_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_PERCENTAGE_CPW_prev.${exec_date}_${exec_time}
     
     mv ${DATA_FILE_PREFIX}_THRESHOLD_CPW \
        ${DATA_FILE_PREFIX}_THRESHOLD_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_THRESHOLD_CPW_prev.${exec_date}_${exec_time}
     
#          !!! TO BE REMOVED POST RELEASE 26.5 ONLY (PART OF ROLLBACK PLAN)
#          #-----------------------------------
#          #       Bookfair Release 10.5
#          #-----------------------------------
#     
#     mv ${DATA_FILE_PREFIX}_EVENT_CPW \
#        ${DATA_FILE_PREFIX}_EVENT_CPW_prev.${exec_date}_${exec_time}
#     gzip ${DATA_FILE_PREFIX}_EVENT_CPW_prev.${exec_date}_${exec_time}
     
          #-----------------------------------
          #       Bookfair Release 11
          #-----------------------------------
     
     mv ${DATA_FILE_PREFIX}_IDEA_CPW \
        ${DATA_FILE_PREFIX}_IDEA_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_IDEA_CPW_prev.${exec_date}_${exec_time}
     
          #-----------------------------------
          #       Bookfair Release 12.5
          #-----------------------------------
     
     mv ${DATA_FILE_PREFIX}_PROFIT_RULES_CPW \
        ${DATA_FILE_PREFIX}_PROFIT_RULES_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_PROFIT_RULES_CPW_prev.${exec_date}_${exec_time}
     
     mv ${DATA_FILE_PREFIX}_FUEL_SURCHARGE_CPW \
        ${DATA_FILE_PREFIX}_FUEL_SURCHARGE_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_FUEL_SURCHARGE_CPW_prev.${exec_date}_${exec_time} 
     
     mv ${DATA_FILE_PREFIX}_FAIR_HISTORY_PRT_CPW \
        ${DATA_FILE_PREFIX}_FAIR_HISTORY_PRT_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_FAIR_HISTORY_PRT_CPW_prev.${exec_date}_${exec_time}
     
     mv ${DATA_FILE_PREFIX}_SCHL_HISTORY_PRT_CPW \
        ${DATA_FILE_PREFIX}_SCHL_HISTORY_PRT_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_SCHL_HISTORY_PRT_CPW_prev.${exec_date}_${exec_time}
     
     mv ${DATA_FILE_PREFIX}_FAIR_HISTORY_CHLD_CPW \
        ${DATA_FILE_PREFIX}_FAIR_HISTORY_CHLD_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_FAIR_HISTORY_CHLD_CPW_prev.${exec_date}_${exec_time}
     
     mv ${DATA_FILE_PREFIX}_SCHL_HISTORY_CHLD_CPW \
        ${DATA_FILE_PREFIX}_SCHL_HISTORY_CHLD_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_SCHL_HISTORY_CHLD_CPW_prev.${exec_date}_${exec_time}
     
          #-----------------------------------
          #       Bookfair Release 14.5
          #-----------------------------------
     
     mv ${DATA_FILE_PREFIX}_COA_CPW \
        ${DATA_FILE_PREFIX}_COA_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_COA_CPW_prev.${exec_date}_${exec_time}
     
          #-----------------------------------
          #       Book fair Release 18
          #-----------------------------------
     
     mv ${DATA_FILE_PREFIX}_TRANSACTION_CPW \
        ${DATA_FILE_PREFIX}_TRANSACTION_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_TRANSACTION_CPW_prev.${exec_date}_${exec_time}
     
     mv ${DATA_FILE_PREFIX}_TRANSACTION_OB_CPW \
        ${DATA_FILE_PREFIX}_TRANSACTION_OB_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_TRANSACTION_OB_CPW_prev.${exec_date}_${exec_time}
     
          #-----------------------------------
          #       Book fair Release 19
          #-----------------------------------
     
     mv ${DATA_FILE_PREFIX}_WORKSHOP_CPW \
        ${DATA_FILE_PREFIX}_WORKSHOP_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_WORKSHOP_CPW_prev.${exec_date}_${exec_time}
     
          #-----------------------------------
          #       Book fair Release 19.5
          #-----------------------------------
     
     mv ${DATA_FILE_PREFIX}_OFE_CANCEL_CPW \
        ${DATA_FILE_PREFIX}_OFE_CANCEL_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_OFE_CANCEL_CPW_prev.${exec_date}_${exec_time}
     
          #-----------------------------------
          #       Book fair Release 19.8
          #-----------------------------------
     
     mv ${DATA_FILE_PREFIX}_BFC_EVENTS_CPW \
        ${DATA_FILE_PREFIX}_BFC_EVENTS_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_BFC_EVENTS_CPW_prev.${exec_date}_${exec_time}
     
          #-----------------------------------
          #       Book fair Release 21
          #-----------------------------------
     
     mv ${DATA_FILE_PREFIX}_HP_PRINCIPAL_CPW \
        ${DATA_FILE_PREFIX}_HP_PRINCIPAL_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_HP_PRINCIPAL_CPW_prev.${exec_date}_${exec_time}
     
          #-----------------------------------
          #       Book fair Release 22
          #-----------------------------------
     
     mv ${DATA_FILE_PREFIX}_COA_TAX_CPW \
        ${DATA_FILE_PREFIX}_COA_TAX_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_COA_TAX_CPW_prev.${exec_date}_${exec_time}
     
          #------------------------------------------
          #   Book fair Release 24.5 Planner Calendar
          #------------------------------------------
     
     mv ${DATA_FILE_PREFIX}_PLANNER_WORKSHOPS_CPW \
        ${DATA_FILE_PREFIX}_PLANNER_WORKSHOPS_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_PLANNER_WORKSHOPS_CPW_prev.${exec_date}_${exec_time}
     
          #------------------------------------------
          #   Book fair Release 25.3 Calendar Trade
          #   As per the Business Rule, the files will
          #    be posted only when Business needs to 
          #    refresh the data and the files will be 
          #    removed the following day.
          #------------------------------------------
     
     if [[ -e ${DATA_FILE_PREFIX}_CALENDAR_TYPE_CPW ]]; then
        mv ${DATA_FILE_PREFIX}_CALENDAR_TYPE_CPW \
           ${DATA_FILE_PREFIX}_CALENDAR_TYPE_CPW_prev.${exec_date}_${exec_time}
        gzip ${DATA_FILE_PREFIX}_CALENDAR_TYPE_CPW_prev.${exec_date}_${exec_time}
     fi
     
     if [[ -e ${DATA_FILE_PREFIX}_CALENDAR_CPW ]]; then
        mv ${DATA_FILE_PREFIX}_CALENDAR_CPW \
           ${DATA_FILE_PREFIX}_CALENDAR_CPW_prev.${exec_date}_${exec_time}
        gzip ${DATA_FILE_PREFIX}_CALENDAR_CPW_prev.${exec_date}_${exec_time}
     fi
     
     echo "----------------------------------------------------------" >> $log
     echo "              Starting FTP Download at $(date) ..."         >> $log
     echo "----------------------------------------------------------" >> $log

     # Note: ftp commands must be flushed to the left margin in any script.
     #       ftp CMD specific to script_action = data_load

ftp -iv ${node_ip} << EOF >> $log 
binary
lcd ${DB_HOME}
cd ${FTP_HOME}
get ${DATA_FILE_PREFIX}_CIS_CPW
get ${DATA_FILE_PREFIX}_PROFIT_BALANCE_CPW
# Book fair Release8 enchancement
get ${DATA_FILE_PREFIX}_HP_MESSAGE_CPW
# Book fair Release8.5 enchancement
get ${DATA_FILE_PREFIX}_PERCENTAGE_CPW
get ${DATA_FILE_PREFIX}_THRESHOLD_CPW
## Book fair Release10.5 enchancement
#get ${DATA_FILE_PREFIX}_EVENT_CPW
# Book fair Release11 enchancement
get ${DATA_FILE_PREFIX}_IDEA_CPW
#-- To import  e-scholastic Test data...
get ${DATA_FILE_PREFIX}_SCHL_PROFIT_BALANCE_CPW
# Book fair Release 12.5 enchancements
get ${DATA_FILE_PREFIX}_PROFIT_RULES_CPW
get ${DATA_FILE_PREFIX}_FUEL_SURCHARGE_CPW
get ${DATA_FILE_PREFIX}_SCHL_CIS_CPW
# Book Fair release 14.1
get ${DATA_FILE_PREFIX}_FAIR_HISTORY_PRT_CPW 
get ${DATA_FILE_PREFIX}_FAIR_HISTORY_CHLD_CPW
get ${DATA_FILE_PREFIX}_SCHL_HISTORY_PRT_CPW
get ${DATA_FILE_PREFIX}_SCHL_HISTORY_CHLD_CPW
# Book Fair release 14.5
get ${DATA_FILE_PREFIX}_COA_CPW
# Bookfair release 18
get ${DATA_FILE_PREFIX}_TRANSACTION_CPW
get ${DATA_FILE_PREFIX}_TRANSACTION_OB_CPW
# Bookfair release 19
get ${DATA_FILE_PREFIX}_WORKSHOP_CPW
# Bookfair release 19.5
get ${DATA_FILE_PREFIX}_OFE_CANCEL_CPW
# Bookfair release 19.8
get ${DATA_FILE_PREFIX}_BFC_EVENTS_CPW
# Bookfair release 21
get ${DATA_FILE_PREFIX}_HP_PRINCIPAL_CPW
# Bookfair release 22
get ${DATA_FILE_PREFIX}_COA_TAX_CPW
# Bookfair release 24.5
get ${DATA_FILE_PREFIX}_PLANNER_WORKSHOPS_CPW
# Bookfair release 25.3 (files posted only occasionally)
get ${DATA_FILE_PREFIX}_CALENDAR_TYPE_CPW
get ${DATA_FILE_PREFIX}_CALENDAR_CPW
bye
EOF
     # End of FTPing.
     
         # Check if we have not lost a connection to the fpt server
     fn_ftp_connection_check  $?  cis_ftp  GET_FILES
     
     if [ -f ${DB_HOME}/${DATA_FILE_PREFIX}_CIS_CPW ]; then
          echo "----------------------------------------------------------" >> $log
          echo "        Starting Loading CIS_CPW File at $(date)... "       >> $log
          echo "----------------------------------------------------------" >> $log
     
     else
          printf "\n==========================================="   >> $log
          printf "\n Error: CIS CPW file not found on FTP site "   >> $log 
          printf "\n File not loaded !!!"                          >> $log
          printf "\n===========================================\n" >> $log
     
          if [[ "${app_env}" == "dev"  ||
                "${app_env}" == "dev02"  ]]; then
               mail -s "db2_task_failure-${host_name}-${app_env}-${subject_line}" \
                       "${email_dev}" < $log 
     
          elif [[ "${app_env}" == "qa"   ||
                  "${app_env}" == "qa02" ||
                  "${app_env}" == "uat01"  ]]; then
               mail -s "db2_task_failure-${host_name}-${app_env}-${subject_line}" \
                       "${email_dev}"  < $log 
     
          elif [[ "${app_env}" == "prod" ]]; then
               mail -s "db2_task_failure-${host_name}-${app_env}-${subject_line}" \
                       "${email_prod}" < $log 
          fi
     
          exit 1
     fi   ### if [ -f ${DB_HOME...
     
     echo "====================  Connecting to DB ${db_name}... " >> $log
     
     db2 -v connect to ${db_name} >> $log
     fn_db2Check $? ${db_name}_CONNECT  DB2_CONNECT_CMD
     db2 -v SET SCHEMA ${schema_name} >> $log
     
     echo "====================  Start to connect to load into DB ${db_name}... " >> $log
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_CIS_CPW.txt
     
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_CIS_CPW' OF DEL \
          MODIFIED BY COMPOUND=5 DATEFORMAT=\"yyyymmdd\" \
          COMMITCOUNT 5000 \
          MESSAGES '$MSGFILE' \
          REPLACE INTO bkfair01.book_fair_data_load ( \
             fair_id, tax_detail_tax_rate, \
             tax_detail_exempt_salesallowed, tax_detail_customer_may_paytax, \
             fair_financial_info_scdbooking, fair_financial_info_incr_reven, \
             fair_gray_state, payment_rcvd, \
             original_trk_delivery_date, original_trk_pickup_date, \
             bogo_bonus, prefair_description, \
             prefair_value, attended_workshop_description, \
             attended_workshop_value, second_bogo_booking, \
             prior_sales_amount, fair_number, ofe_sales_flag )" >> $log
     fn_import_process_log $? $MSGFILE
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_SCHL_CIS_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_SCHL_CIS_CPW' OF DEL \
          MODIFIED BY COMPOUND=5 DATEFORMAT=\"yyyymmdd\" \
          COMMITCOUNT 5000 \
          MESSAGES '$MSGFILE' \
          INSERT INTO bkfair01.book_fair_data_load ( \
             fair_id, tax_detail_tax_rate, \
             tax_detail_exempt_salesallowed, tax_detail_customer_may_paytax, \
             fair_financial_info_scdbooking, fair_financial_info_incr_reven, \
             fair_gray_state, payment_rcvd, \
             original_trk_delivery_date, original_trk_pickup_date, \
             bogo_bonus, prefair_description, \
             prefair_value, attended_workshop_description, \
             attended_workshop_value, second_bogo_booking, \
             prior_sales_amount, fair_number, ofe_sales_flag )" >> $log
     fn_import_process_log $? $MSGFILE
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_PROFIT_BALANCE_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_PROFIT_BALANCE_CPW' OF DEL \
          MODIFIED BY COMPOUND=5 \
          COMMITCOUNT 5000 \
          MESSAGES '$MSGFILE' \
          REPLACE INTO bkfair01.profit_balance_data_load " >> $log
     fn_import_process_log $? $MSGFILE
     
          #-----------------------------------
          #-- import test data for e-Scholastic.
          #-----------------------------------
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_SCHL_PROFIT_BALANCE_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_SCHL_PROFIT_BALANCE_CPW'  OF DEL \
          MESSAGES '$MSGFILE' \
          INSERT INTO bkfair01.profit_balance_data_load " >> $log
     fn_import_process_log $? $MSGFILE
     
          #--------------------------------------------------------
          #          Bookfair Release 8
          #          Release-19 use new table LU_HOMEPAGE_MESSAGES
          #            which replaces LU_HOMEPAGE_MESSAGE.
          #--------------------------------------------------------
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_HP_MESSAGE_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_HP_MESSAGE_CPW' OF DEL \
          MODIFIED BY COMPOUND=5 \
          COMMITCOUNT 5000 \
          MESSAGES '$MSGFILE' \
          REPLACE INTO bkfair01.lu_homepage_messages " >> $log
     fn_import_process_log $? $MSGFILE
     
          #-----------------------------------
          #          Bookfair Release 8.5
          #-----------------------------------
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_PERCENTAGE_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_PERCENTAGE_CPW' OF DEL \
          MODIFIED BY COMPOUND=5 \
          COMMITCOUNT 5000 \
          MESSAGES '$MSGFILE' \
          REPLACE INTO bkfair01.irc_percentage_increase_rule_data_load " >> $log
     fn_import_process_log $? $MSGFILE
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_THRESHOLD_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_THRESHOLD_CPW' OF DEL \
          MODIFIED BY COMPOUND=5 \
          COMMITCOUNT 5000 \
          MESSAGES '$MSGFILE' \
          REPLACE INTO bkfair01.irc_threshold_bonus_rules_data_load " >> $log
     fn_import_process_log $? $MSGFILE
     
#          !!! TO BE REMOVED POST RELEASE 26.5 ONLY (PART OF ROLLBACK PLAN)
#          #-----------------------------------
#          #          Bookfair Release 10.5
#          #-----------------------------------
#     
#     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_EVENT_CPW.txt
#     echo "" > $MSGFILE
#     echo "Import message file: $MSGFILE" >> $log
#     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_EVENT_CPW' OF DEL \
#          MODIFIED BY COMPOUND=5 \
#          COMMITCOUNT 5000 \
#          MESSAGES '$MSGFILE' \
#          REPLACE INTO bkfair01.lu_homepage_event_load " >> $log
#     fn_import_process_log $? $MSGFILE
     
          #-----------------------------------
          #          Bookfair Release 11
          #-----------------------------------
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_IDEA_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_IDEA_CPW' OF DEL \
          MODIFIED BY COMPOUND=5 \
          COMMITCOUNT 5000 \
          MESSAGES '$MSGFILE' \
          REPLACE INTO bkfair01.lu_share_idea_data_load " >> $log
     fn_import_process_log $? $MSGFILE
     
          #-----------------------------------
          #          Bookfair Release 12.5
          #-----------------------------------
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_PROFIT_RULES_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_PROFIT_RULES_CPW' OF DEL \
          MODIFIED BY COMPOUND=5 \
          COMMITCOUNT 5000 \
          MESSAGES '$MSGFILE' \
          REPLACE INTO bkfair01.profit_rules_data_load " >> $log
     fn_import_process_log $? $MSGFILE
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_FUEL_SURCHARGE_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_FUEL_SURCHARGE_CPW' OF DEL \
          MODIFIED BY COMPOUND=5 \
          COMMITCOUNT 5000 \
          MESSAGES '$MSGFILE' \
          REPLACE INTO bkfair01.fuel_surcharge_data_load "  >> $log 
     fn_import_process_log $? $MSGFILE
     
          #-----------------------------------
          #          Bookfair Release 14.1 
          #-----------------------------------
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_FAIR_HISTORY_CHLD_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_FAIR_HISTORY_CHLD_CPW' OF DEL \
          MODIFIED BY COMPOUND=5 \
          COMMITCOUNT 5000 \
          MESSAGES '$MSGFILE' \
          REPLACE INTO bkfair01.MAX_FAIR_HISTORY_CHLD_LOAD " >> $log
     fn_import_process_log $? $MSGFILE
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_SCHL_HISTORY_CHLD_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_SCHL_HISTORY_CHLD_CPW' OF DEL \
          MODIFIED BY COMPOUND=5 \
          COMMITCOUNT 5000 \
          MESSAGES '$MSGFILE' \
          INSERT INTO bkfair01.MAX_FAIR_HISTORY_CHLD_LOAD " >> $log
     fn_import_process_log $? $MSGFILE
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_FAIR_HISTORY_PRT_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_FAIR_HISTORY_PRT_CPW' OF DEL \
          MODIFIED BY COMPOUND=5 \
          COMMITCOUNT 5000 \
          MESSAGES '$MSGFILE' \
          REPLACE INTO bkfair01.MAX_FAIR_HISTORY_PRT_LOAD " >> $log
     fn_import_process_log $? $MSGFILE
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_SCHL_HISTORY_PRT_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_SCHL_HISTORY_PRT_CPW' OF DEL \
          MODIFIED BY COMPOUND=5 \
          COMMITCOUNT 5000 \
          MESSAGES '$MSGFILE' \
          INSERT INTO bkfair01.MAX_FAIR_HISTORY_PRT_LOAD " >> $log
     fn_import_process_log $? $MSGFILE
     
          #-----------------------------------
          #          Bookfair Release 14.5
          #-----------------------------------
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_COA_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM  '${DB_HOME}/${DATA_FILE_PREFIX}_COA_CPW' OF DEL \
          COMMITCOUNT 5000 \
          MESSAGES '$MSGFILE' \
          REPLACE INTO bkfair01.LU_COA_LOAD " >> $log
     fn_import_process_log $? $MSGFILE
     
          #---------------------------------------------------------
          #          Bookfair Release 18
          # Note: The difference with previous releases
          #       resides in the fact that the data is imported
          #       from files using the import syntax that adapts
          #       file data to table data type.
          #---------------------------------------------------------
          # Check if the file exists first before processing it...
          
     if [ -e ${DB_HOME}/${DATA_FILE_PREFIX}_TRANSACTION_CPW ]; then
               # As part of QC # 1886, massage the file at the 6th column since
               #   that data can have negative DECIMAL set in double quotes and
               #   db2 does not recognize that format.
               #   As a result, the file massaging removes these double_quotes
               #   from the 6th column corresponding to DAILY_BALANCED_AMOUNT
               #   and copy that data into a "temp_" file.
               
          awk -F"," '{gsub(/"/, "", $6)} \
                     {print $1 "," $2 "," $3 "," $4 "," $5 "," $6}' \
                     ${DB_HOME}/${DATA_FILE_PREFIX}_TRANSACTION_CPW > ${DB_HOME}/temp_${DATA_FILE_PREFIX}_TRANSACTION_CPW
          
          MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_TRANSACTION_CPW.txt
          echo "" > $MSGFILE
          echo "Import message file: $MSGFILE" >> $log
          db2 "IMPORT FROM  '${DB_HOME}/temp_${DATA_FILE_PREFIX}_TRANSACTION_CPW' OF DEL \
               MODIFIED BY implieddecimal dateformat =\"YYYYMMDD\" \
               METHOD P(1,2,3,4,5,6) \
               COMMITCOUNT 5000 \
               MESSAGES '$MSGFILE' \
               REPLACE INTO bkfair01.transaction_details " >> $log
          fn_import_process_log $? $MSGFILE
          
          db2 -v "REORG TABLE bkfair01.transaction_details \
                  INDEX bkfair01.x_transacdetails" | sed 's/  */ /g' >> $log
          
          db2 -v "RUNSTATS ON TABLE bkfair01.transaction_details \
                     WITH DISTRIBUTION AND INDEXES ALL" | sed 's/  */ /g' >> $log
     
     else
          printf "\n\n The Input File:\n" >> $log
          printf "\t${DB_HOME}/${DATA_FILE_PREFIX}_TRANSACTION_CPW " >> $log
          printf "\nwas not found.\n" $log
     fi
     
          # Check if the file exists first before processing it...
          
     if [ -e ${DB_HOME}/${DATA_FILE_PREFIX}_TRANSACTION_OB_CPW ]; then
               # As part of QC # 1886, massage the file at the 6th column since
               #   that data can have negative DECIMAL set in double quotes and
               #   db2 does not recognize that format.
               #   As a result, the file massaging removes these double_quotes
               #   from the 3rd column corresponding to OPENING_BALANCE_AMOUNT
               #   and copy that data into a "temp_" file.
               
          awk -F"," '{gsub(/"/, "", $3)} \
                     {print $1 "," $2 "," $3}' \
                     ${DATA_FILE_PREFIX}_TRANSACTION_OB_CPW > ${DB_HOME}/temp_${DATA_FILE_PREFIX}_TRANSACTION_OB_CPW
          
          MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_TRANSACTION_OB_CPW.txt
          echo "" > $MSGFILE
          echo "Import message file: $MSGFILE" >> $log
          db2 "IMPORT FROM  '${DB_HOME}/temp_${DATA_FILE_PREFIX}_TRANSACTION_OB_CPW' OF DEL \
               MODIFIED BY implieddecimal dateformat =\"YYYYMMDD\" \
               METHOD P(1,2,3) \
               COMMITCOUNT 5000 \
               MESSAGES '$MSGFILE' \
               REPLACE INTO bkfair01.transaction_open_balance " >>$log
          fn_import_process_log $? $MSGFILE
          
          db2 -v "REORG TABLE bkfair01.transaction_open_balance \
                  INDEX bkfair01.xpk_transacopenbal_school_id" | sed 's/  */ /g' >> $log
          
          db2 -v "RUNSTATS ON TABLE bkfair01.transaction_open_balance \
                     WITH DISTRIBUTION AND INDEXES ALL" | sed 's/  */ /g' >> $log
     
     else
          printf "\n\n The Input File:\n" >> $log
          printf "\t${DB_HOME}/${DATA_FILE_PREFIX}_TRANSACTION_OB_CPW " >> $log
          printf "\nwas not found.\n" $log
     fi
     
          #---------------------------------------------------------
          #          Bookfair Release 19
          #---------------------------------------------------------
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_WORKSHOP_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_WORKSHOP_CPW' OF DEL \
          METHOD P(1,2,3) \
          COMMITCOUNT 1000 \
          MESSAGES '$MSGFILE' \
          INSERT_UPDATE INTO bkfair01.workshop_rewards ( \
                             fair_id, reward_id, complete_flag )" >> $log
     fn_import_process_log $? $MSGFILE
     
          #---------------------------------------------------------
          #          Bookfair Release 19.8
          #---------------------------------------------------------
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_BFC_EVENTS_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_BFC_EVENTS_CPW' OF DEL \
          MODIFIED BY COMPOUND=5 \
          COMMITCOUNT 1000 \
          MESSAGES '$MSGFILE' \
          REPLACE INTO bkfair01.chairperson_events " >> $log
     fn_import_process_log $? $MSGFILE
     
          #---------------------------------------------------------
          #       Bookfair Release 21
          #       Note: in future release, review the commitcount
          #             in increasing it based on the # of records.
          #---------------------------------------------------------
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_HP_PRINCIPAL_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_HP_PRINCIPAL_CPW' OF DEL \
          MODIFIED BY COMPOUND=5 \
          COMMITCOUNT 1000 \
          MESSAGES '$MSGFILE' \
          REPLACE INTO bkfair01.lu_principal_activities " >> $log
     fn_import_process_log $? $MSGFILE
     
          #---------------------------------------------------------
          #       Bookfair Release 22
          #---------------------------------------------------------
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_COA_TAX_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_COA_TAX_CPW' OF DEL \
          MODIFIED BY DATEFORMAT=\"yyyymmdd\" \
          COMMITCOUNT 1000 \
          MESSAGES '$MSGFILE' \
          INSERT_UPDATE INTO bkfair01.lu_coa_taxes" >> $log
     fn_import_process_log $? $MSGFILE
     
          #---------------------------------------------------------
          #       Bookfair Release 24.5
          #---------------------------------------------------------
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_PLANNER_WORKSHOPS_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 -v "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_PLANNER_WORKSHOPS_CPW' OF DEL \
             MODIFIED BY USEDEFAULTS \
             METHOD P(1,2,3,4,5,6) \
             COMMITCOUNT 5000 \
             MESSAGES '$MSGFILE' \
             INSERT_UPDATE INTO bkfair01.fair_planner_workshops \
                ( fair_id, event_id, \
                  workshop_person_id, registration_status, \
                  first_name, last_name )"  | sed 's/  */ /g' >> $log
     fn_import_process_log $? $MSGFILE
     
     db2 -v "REORG TABLE bkfair01.fair_planner_workshops" >> $log
     
     db2 -v "RUNSTATS ON TABLE bkfair01.fair_planner_workshops \
                WITH DISTRIBUTION AND INDEXES ALL"  | sed 's/  */ /g' >> $log
     
          #---------------------------------------------------------
          #       Bookfair Release 25.3 Calendar Trade
          #---------------------------------------------------------
     echo "Calendar Data Processing Starting at: $(date)... " >> $log
          # ...CALENDAR_TYPE file is only processed if it's present 
          #    per Business' agreement; This file is being reloaded only 
          #    a couple times per year.
          
     if [[ -e ${DATA_FILE_PREFIX}_CALENDAR_TYPE_CPW ]]; then
        echo "----------------------------------------------------------"    >> $log
        echo "  Starting ${DATA_FILE_PREFIX}_CALENDAR_TYPE_CPW Data Load..." >> $log
        echo "----------------------------------------------------------"    >> $log
        MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_CALENDAR_TYPE_CPW.txt
        echo "" > $MSGFILE
        echo "Import message file: $MSGFILE" >> $log
        db2 -v "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_CALENDAR_TYPE_CPW' OF DEL \
                MODIFIED BY USEDEFAULTS \
                METHOD P(1) \
                COMMITCOUNT 1000 \
                MESSAGES '$MSGFILE' \
                REPLACE INTO bkfair01.lu_planner_calendar_types "  | sed 's/  */ /g' >> $log
        
        fn_import_process_log $? $MSGFILE
        
        db2 -v "REORG TABLE bkfair01.lu_planner_calendar_types" >> $log
        
        db2 -v "RUNSTATS ON TABLE bkfair01.lu_planner_calendar_types \
                   WITH DISTRIBUTION AND INDEXES ALL"  | sed 's/  */ /g' >> $log
     fi

        # This file is almost always reloaded daily...

     if [[ -e ${DATA_FILE_PREFIX}_CALENDAR_CPW ]]; then
        echo "----------------------------------------------------------"    >> $log
        echo "  Starting ${DATA_FILE_PREFIX}_CALENDAR_CPW Data Load..."      >> $log
        echo "----------------------------------------------------------"    >> $log
        MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_CALENDAR_CPW.txt
        echo "" > $MSGFILE
        echo "Import message file: $MSGFILE" >> $log
        db2 -v "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_CALENDAR_CPW' OF DEL \
                MODIFIED BY USEDEFAULTS \
                METHOD P(1, 3, 4, 2) \
                COMMITCOUNT 1000 \
                MESSAGES '$MSGFILE' \
                REPLACE INTO bkfair01.lu_planner_calendar_emails \
                   ( email, calendar_type, calendar_id, email_type )"  | sed 's/  */ /g' >> $log
        fn_import_process_log $? $MSGFILE
        
           # Force all email data to be in lower case...
           
        db2 -av "UPDATE bkfair01.lu_planner_calendar_emails \
                    SET email = LOWER(email)"  | sed 's/  */ /g' >> $log
        
           # Per Business rule, delete all records not having a calendar_type record
           #    present in the Reference table...
        
        printf "\n\n Feedback on LU_PLANNER_CALENDAR_EMAILS Data Deletion " >> $log
        printf "\n for non-referenced data to LU_PLANNER_CALENDAR_TYPES"    >> $log
        printf "\n as well as inexistant email from either SALESPERSON OR " >> $log
        printf "\n FSRPERSON tables.\n\n" >> $log
        
        db2 -av "EXPORT TO '${DB_HOME}/${DATA_FILE_PREFIX}_CALENDAR_ERRORS_${exec_date}_${exec_time}.csv' OF DEL \
                   SELECT lue.email, lue.calendar_type \
                      FROM bkfair01.lu_planner_calendar_emails lue \
                         LEFT OUTER JOIN bkfair01.lu_planner_calendar_types lut \
                           ON lue.calendar_type = lut.calendar_type \
                      WHERE lut.calendar_type IS NULL \
                      UNION \
                      SELECT lue.email, lue.calendar_type \
                         FROM bkfair01.lu_planner_calendar_emails lue \
                            LEFT OUTER JOIN bkfair01.salesperson  s \
                              ON lue.email = LOWER(s.email) \
                          WHERE lue.email_type = 'C' \
                            AND s.email IS NULL \
                      UNION \
                      SELECT lue.email, lue.calendar_type \
                         FROM bkfair01.lu_planner_calendar_emails lue \
                            LEFT OUTER JOIN bkfair01.fsrperson  fsr \
                              ON lue.email = LOWER(fsr.fsr_email) \
                         WHERE lue.email_type = 'R'\
                           AND fsr.fsr_email IS NULL \
                    FOR READ ONLY WITH UR"
        
             # Time to delete the data as that file will be emailed...
        
        db2 -av "DELETE FROM bkfair01.lu_planner_calendar_emails \
                   WHERE (email, calendar_type) IN \
                      ( SELECT lue.email, lue.calendar_type \
                           FROM bkfair01.lu_planner_calendar_emails lue \
                              LEFT OUTER JOIN bkfair01.lu_planner_calendar_types lut \
                                ON lue.calendar_type = lut.calendar_type \
                           WHERE lut.calendar_type IS NULL \
                        UNION \
                        SELECT lue.email, lue.calendar_type \
                           FROM bkfair01.lu_planner_calendar_emails lue \
                              LEFT OUTER JOIN bkfair01.salesperson  s \
                                ON lue.email = LOWER(s.email) \
                           WHERE lue.email_type = 'C' \
                             AND s.email IS NULL \
                        UNION \
                        SELECT lue.email, lue.calendar_type \
                           FROM bkfair01.lu_planner_calendar_emails lue \
                              LEFT OUTER JOIN bkfair01.fsrperson  fsr \
                                ON lue.email = LOWER(fsr.fsr_email) \
                           WHERE lue.email_type = 'R'\
                             AND fsr.fsr_email IS NULL )" \
             | sed 's/  */ /g' >> $log
        
        printf "\n\n Feedback on UNPUBLISHED.PLANNER_CALENDAR_RANGES Data Deletion "       >> $log
        printf "\n for non-referenced data to LU_PLANNER_CALENDAR_TYPES...\n\n "  >> $log
        
        db2 -av "DELETE FROM unpublished.planner_calendar_ranges \
                   WHERE calendar_type NOT IN \
                       ( SELECT calendar_type FROM bkfair01.lu_planner_calendar_types)" \
             | sed 's/  */ /g' >> $log
        
        printf "\n\n Feedback on BKFAIR01.PLANNER_CALENDAR_RANGES Data Deletion "       >> $log
        printf "\n for non-referenced data to LU_PLANNER_CALENDAR_TYPES...\n\n "  >> $log
        
        db2 -av "DELETE FROM bkfair01.planner_calendar_ranges \
                   WHERE calendar_type NOT IN \
                       ( SELECT calendar_type FROM bkfair01.lu_planner_calendar_types)" \
             | sed 's/  */ /g' >> $log
        
        db2 -v "REORG TABLE bkfair01.lu_planner_calendar_emails" >> $log
        
        db2 -v "RUNSTATS ON TABLE bkfair01.lu_planner_calendar_emails \
                   WITH DISTRIBUTION AND INDEXES ALL"  | sed 's/  */ /g' >> $log
     fi
     
    printf "\n\n ***** END OF CIS DATA FILE HANDLING FROM FROM FTP DIRECTORY! *****\n"  >> $log
     
     echo "========== Proc p_bkfair_err_load Starting at: $(date)... " >> $log
     
        # Delete the data from table BOOK_FAIR_ERROR_DATA this way to prevent  
        #  unnecessary logging while deleting that data from the procedure.
        
     db2 -v " IMPORT FROM /dev/null OF DEL \
                 REPLACE INTO bkfair01.book_fair_error_data " >> $log
     echo "End of emptying BOOK_FAIR_ERROR_DATA: $(date)" >> $log
     
     db2 "CALL bkfair01.p_bkfair_err_load()" >> $log
     if [[ $? != 0 ]]; then
          echo " Error:  bkfair01.p_bkfair_err_load encountered errors."  >> $log
     fi
     
     
     echo "========== Proc p_bkfair_data_load Starting at: $(date)... " >> $log
     
     db2 "CALL bkfair01.p_bkfair_data_load()" >> $log
     if [[ $? != 0 ]]; then
          echo " Error: bkfair01.p_bkfair_data_load  encountered errors."  >> $log
     fi
     
     
     echo "========== Proc sp_bkfair_finanform Starting at: $(date)... " >> $log
     
     db2 "CALL bkfair01.sp_bkfair_finanform()" >> $log
     if [[ $? != 0 ]]; then
          echo " Error: bkfair01.sp_bkfair_finanform encountered errors."  >> $log
     fi
     
     
     echo "========== Proc sp_previous_fairs Starting at: $(date)... " >> $log
     
     db2 -v "call bkfair01.sp_previous_fairs ('DAILY_LOAD', ?, ?, ?, ?, ?)" >> $log
     
     if [[ $? != 0 ]]; then
          echo " Error: bkfair01.p_previous_fairs  encountered errors."  >> $log
     fi
     
     db2 -v "REORG TABLE bkfair01.previous_fairs" >> $log
     
     db2 -v "RUNSTATS ON TABLE bkfair01.previous_fairs \
                WITH DISTRIBUTION AND INDEXES ALL"  | sed 's/  */ /g' >> $log
     
          #-----------------------------------
          #          Bookfair Release 16.5.2
          #-----------------------------------
     echo "========== Proc sp_profit_balance Starting at: $(date)... " >> $log
     
          # Delete the data from table PROFIT_BALANCE this way to prevent
          #  unnecessary logging while deleting that data from the procedure.
     
     db2 -v " IMPORT FROM /dev/null OF DEL \
                 REPLACE INTO bkfair01.profit_balance "  | sed 's/  */ /g' >> $log
     echo "End of emptying PROFIT_BALANCE: $(date)" >> $log
          
     db2 "CALL bkfair01.sp_profit_balance()" >> $log
     
     if [[ $? != 0 ]]; then
          echo " Error: bkfair01.sp_profit_balance encountered errors."  >> $log
     fi
     
     
     echo "========== Proc sp_disassociate_url Starting at: $(date)... " >> $log
     
     db2 "CALL bkfair01.sp_disassociate_url()"  >> $log
     
     if [[ $? != 0 ]]; then
          echo " Error:  bkfair01.sp_disassociate_url  encountered errors."  >> $log
     fi
     
     
#          !!! TO BE REMOVED POST RELEASE 26.5 ONLY (PART OF ROLLBACK PLAN)
#          #-----------------------------------
#          #          Bookfair Release 10.5
#          #-----------------------------------
#     echo "========== Proc p_lu_hompage_event Starting at: $(date)... " >> $log
#     
#     db2 "CALL bkfair01.p_lu_hompage_event()"  >> $log
#     
#     if [[ $? != 0 ]]; then
#          echo " Error:  bkfair01.p_lu_hompage_event  encountered errors."  >> $log
#     fi
     
     
          #-----------------------------------
          #          Bookfair Release 11
          #-----------------------------------
     echo "========== Proc bkfair01.p_share_idea Starting at: $(date)... " >> $log
     
     db2 "CALL bkfair01.p_share_idea()"  >> $log
     
     if [[ $? != 0 ]]; then
          echo " Error:  bkfair01.p_share_idea  encountered errors."  >> $log
     fi
     
     
          #-----------------------------------
          #          Bookfair Release 12.5 
          #-----------------------------------
     echo "========== Proc bkfair01.p_profit_rules Starting at: $(date)... " >> $log
     
     db2 "CALL bkfair01.p_profit_rules()"  >> $log
     
     if [[ $? != 0 ]]; then
          echo " Error:  bkfair01.p_profit_rules  encountered errors."  >> $log
     fi
     
     
     echo "========== Proc bkfair01.p_fuel_surcharge Starting at: $(date)... " >> $log
     
     db2 "CALL bkfair01.p_fuel_surcharge()"  >> $log
     
     if [[ $? != 0 ]]; then
          echo " Error:  bkfair01.p_fuel_surcharge  encountered errors."  >> $log
     fi
     
     
          #-----------------------------------
          #          Bookfair Release 14.1
          #-----------------------------------
     echo "========== Proc bkfair01.p_mx_fair_hist_chl Starting at: $(date)... " >> $log
     
          # Delete the data from table MAX_FAIR_HISTORY_CHLD this way to prevent
          #  unnecessary logging while deleting that data from the procedure.
     
     db2 -v " IMPORT FROM /dev/null OF DEL \
                 REPLACE INTO bkfair01.max_fair_history_chld "  | sed 's/  */ /g' >> $log
     echo "End of emptying MAX_FAIR_HISTORY_CHLD: $(date)" >> $log
     
     db2 "CALL bkfair01.p_mx_fair_hist_chl()"  >> $log
     
     if [[ $? != 0 ]]; then
          echo " Error:  bkfair01.p_mx_fair_hist_chl  encountered errors."  >> $log
     fi
     
        # Reorg this table regularly due to a performance hit encountered during
        #   the performance testing.
     
     db2 -v "REORG TABLE bkfair01.max_fair_history_chld \
                INDEX bkfair01.xpkc_mx_fr_hi_chld"  | sed 's/  */ /g' >> $log
     
     db2 -v "RUNSTATS ON TABLE bkfair01.max_fair_history_chld \
                WITH DISTRIBUTION AND  INDEXES ALL"  | sed 's/  */ /g' >> $log
     
     echo "========== Proc bkfair01.p_mx_fair_hist_prt Starting at: $(date)... " >> $log
     
          # Delete the data from table MAX_FAIR_HISTORY_PRT this way to prevent
          #  unnecessary logging while deleting that data from the procedure.
     
     db2 -v " IMPORT FROM /dev/null OF DEL \
                 REPLACE INTO bkfair01.max_fair_history_prt "  | sed 's/  */ /g' >> $log
     echo "End of emptying MAX_FAIR_HISTORY_PRT: $(date)" >> $log
     
     db2 "CALL bkfair01.p_mx_fair_hist_prt()"  >> $log
     
     if [[ $? != 0 ]]; then
          echo " Error:  bkfair01.p_mx_fair_hist_prt  encountered errors."  >> $log
     fi
     
     db2 -v "REORG TABLE bkfair01.max_fair_history_prt \
                INDEX bkfair01.xpkc_max_fr_hi_prt"  | sed 's/  */ /g' >> $log
     
     db2 -v "RUNSTATS ON TABLE bkfair01.max_fair_history_prt \
                WITH DISTRIBUTION AND  INDEXES ALL"  | sed 's/  */ /g' >> $log
     
          #-----------------------------------
          #          Bookfair Release 14.5
          #-----------------------------------
     echo "========== Proc bkfair01.p_coa Starting at: $(date)... " >> $log
     
     db2 "CALL bkfair01.p_coa()"  >> $log
     
     if [[ $? != 0 ]]; then
          echo " Error:  bkfair01.p_coa  encountered errors."  >> $log
     fi
     #### END PROCS
     
     
     echo "----------------------------------------------------------" >> $log
     echo "               Start to BKFAIR DATA EXPORT... "             >> $log
     echo "----------------------------------------------------------" >> $log
     echo "    Start book_fair_error_data Export at $(date)..."        >> $log 
     echo "----------------------------------------------------------" >> $log
     
     db2 -v "EXPORT TO '${DB_HOME}/${DATA_FILE_PREFIX}_CIS_CPW_ERROR' OF DEL \
                SELECT * FROM bkfair01.book_fair_error_data"  | sed 's/  */ /g' >> $log
     
     echo "----------------------------------------------------------" >> $log
     echo "         Start Financial Forms Data Export at $(date)..."   >> $log 
     echo "----------------------------------------------------------" >> $log
     
          #------------------------------------
          # Bookfair Release 19
          #------------------------------------
          
     db2 -v "EXPORT to '${DB_HOME}/${DATA_FILE_PREFIX}_CPW_HOMEPAGE_CUSTOMER_EMAILS_EXPORT' OF DEL \
                SELECT fair_id, school_id, email \
                   FROM bkfair01.homepage_customer_emails \
                   WHERE create_date > ( CURRENT TIMESTAMP - 7 DAYS ) \
                   FOR READ ONLY WITH UR " | sed 's/  */ /g' >> $log
     
          #--------------------------------------------
          # Bookfair Release 19.5
          # Bookfair Release 24:
          #   Add 3 products to where-clause filter:
          #      - ME, MM, MB
          #--------------------------------------------
     
     db2 -v "EXPORT to '${DB_HOME}/${DATA_FILE_PREFIX}_WORKSHOP_REWARDS_REPUBLISHED_HOMEPAGES_EXPORT' OF DEL \
                SELECT hu.fair_id \
                   FROM bkfair01.homepage_url hu, \
                        bkfair01.fair         f \
                   WHERE hu.republished_date > ( CURRENT TIMESTAMP - 7 DAYS ) \
                     AND hu.fair_id = f.id \
                     AND f.product_id NOT IN \
                        ( 'BE', 'BM', 'BP', 'TM', 'TT', \
                          'TB', 'BB', 'ME', 'MM', 'MB' ) \
                   FOR READ ONLY WITH UR " | sed 's/  */ /g' | sed 's/  */ /g' >> $log
     
          #-----------------------------------------------------------
          # Release 19.5
          # Process the ${DATA_FILE_PREFIX}_OFE_CANCEL_CPW file...
     
          # First ensure that we are not dealing with a file 
          #   with blank lines etc.
          # and filter out the OFE_CANCEL file over to a temp file...
          #-----------------------------------------------------------
          
     if [[ -e ${DB_HOME}/${DATA_FILE_PREFIX}_OFE_CANCEL_CPW ]]; then
          sed 's/  *//g' ${DATA_FILE_PREFIX}_OFE_CANCEL_CPW |  
              awk 'BEGIN {RS = "\n"} $1 ~/^[0123456789]/ {print $1}' > ${DB_HOME}/daily_temp_OFE_CANCEL_CPW
          file_ofe_cancel_ct=$(cat ${DB_HOME}/daily_temp_OFE_CANCEL_CPW | wc -l | sed 's/  *//g' )
     fi
     
          # Then,if it exists, use the temp file {DB_HOME}/daily_temp_OFE_CANCEL_CPW 
          # towards processing when necessary...
          
     if [[ -e ${DB_HOME}/daily_temp_OFE_CANCEL_CPW  &&
           -s ${DB_HOME}/daily_temp_OFE_CANCEL_CPW ]]; then
     
        while read LINE
        do
           db2 -v "UPDATE bkfair01.online_shopping \
                      SET ofe_status = 'Cancelled', \
                          updated_date = CURRENT TIMESTAMP \
                      WHERE fair_id = $LINE" | sed 's/  */ /g' >> $log
           
               # Check that each fair got updated successfully towards email feedback...
     
            cmd_success_ct=$(tail -n 2 $log | egrep 'completed successfully' | wc -l | sed 's/  *//g')
            if (( $cmd_success_ct == 1 )); then
               (( ofe_cancel_loop_ct=ofe_cancel_loop_ct + 1 ))
            fi
        done < ${DB_HOME}/daily_temp_OFE_CANCEL_CPW 
        
             # Check that the number of OFE_CANCEL fair_id correspond to the 
             # actual number of records that have just been updated and send
             # an email to specific folks per Business request.
        
        if (( $ofe_cancel_loop_ct != $file_ofe_cancel_ct )); then
           (echo "\nAll,"; \
            echo "There were ${ofe_cancel_loop_ct} fair_id out of"; \
            echo "${file_ofe_cancel_ct} fair_id delivered via the import file"; \
            echo "${DATA_FILE_PREFIX}_OFE_CANCEL_CPW.";\
            echo "\nRegards,"; \
            echo "The Data team";) | \
            mailx -s "${host_name}-${app_env} - ${DATA_FILE_PREFIX}_OFE_CANCEL_CPW Not Fully Executed" \
                     "${email_cancel_ofes}"
        fi
        
     else   ### if [[ -e ${DB_HOME}/daily_temp_OFE_CANCEL_CPW  &&
        printf "\n\n Either the file ${DATA_FILE_PREFIX}_OFE_CANCEL_CPW was inexistant" >> $log
        printf "\n or there were no records in that file .\n\n" >> $log
     fi     ### if [[ -e ${DB_HOME}/daily_temp_OFE_CANCEL_CPW  &&...
     
          #--------------------------------------------
          # Bookfair Release 19.7
          # Bookfair Release 24:
          #   Add 3 products to where-clause filter:
          #      - ME, MM, MB
          #--------------------------------------------
     
     db2 -v "EXPORT to '${DB_HOME}/${DATA_FILE_PREFIX}_VOLUNTEER_OPTIN' OF DEL \
                SELECT v.fair_id, v.school_id, v.first_name, \
                       v.last_name, v.email, v.phone, \
                       v.optin_source, v.submit_date \
                   FROM bkfair01.volunteer_forms v, \
                        bkfair01.fair            f \
                   WHERE v.submit_date > ( CURRENT TIMESTAMP - 7 DAYS ) \
                     AND v.contact_me_ckbox = 'Y' \
                     AND v.fair_id = f.id \
                     AND f.product_id NOT IN \
                        ( 'BE', 'BM', 'BP', 'TM', 'TT', \
                          'TB', 'BB', 'ME', 'MM', 'MB' ) \
                   FOR READ ONLY WITH UR " | sed 's/  */ /g' | sed 's/  */ /g' >> $log
     
     
          # Register this new task action in maintenance table...
     
     new_export_exec_timestamp=$(db2 -x "SELECT CURRENT TIMESTAMP FROM sysibm.sysdummy1" )
     
     db2 -v "INSERT INTO ${schema_name}.maint_bkfair_tasks \
             (task_id, create_timestamp, task_name, script_name) \
             VALUES ( (SELECT MAX(task_id) + 1 \
                          FROM ${schema_name}.maint_bkfair_tasks), \
                     '${new_export_exec_timestamp}', \
                     '${db_name}' || ' CIS Data Load Primary', \
                     '${app_env}-Bkfair_cis_load.Main Data Load' ) " | sed 's/  */ /g' >> $log
     
          # Clean up maintenance records older than 1 year
     db2 -av "DELETE FROM ${schema_name}.maint_bkfair_tasks \
                 WHERE create_timestamp < \
                      ( CURRENT TIMESTAMP - 1 YEAR )" | sed 's/  */ /g' >> $log
     
     db2 -av "DELETE FROM ${schema_name}.fair_load_feedback \
                 WHERE create_date < \
                      ( CURRENT TIMESTAMP - 1 YEAR )" | sed 's/  */ /g' >> $log
     
     db2 -v "disconnect ${db_name}"  >> $log
     db2 -v terminate  >> $log
     
          echo "----------------------------------------------------------" >> $log
          echo "     Start FTP Up-Load connect at $(date)... "              >> $log
          echo "     Second FTP CMD specific to script_action = data_load " >> $log
          echo "----------------------------------------------------------" >> $log

ftp -iv ${node_ip} << EOF >> $log
binary
lcd ${DB_HOME}
cd ${FTP_HOME}
bin
put ${DATA_FILE_PREFIX}_CIS_CPW_ERROR   ${DATA_FILE_PREFIX}_CIS_CPW_ERROR
put ${DATA_FILE_PREFIX}_CPW_HOMEPAGE_CUSTOMER_EMAILS_EXPORT   ${DATA_FILE_PREFIX}_CPW_HOMEPAGE_CUSTOMER_EMAILS_EXPORT
put ${DATA_FILE_PREFIX}_WORKSHOP_REWARDS_REPUBLISHED_HOMEPAGES_EXPORT   ${DATA_FILE_PREFIX}_WORKSHOP_REWARDS_REPUBLISHED_HOMEPAGES_EXPORT
put ${DATA_FILE_PREFIX}_VOLUNTEER_OPTIN   ${DATA_FILE_PREFIX}_VOLUNTEER_OPTIN
bye
EOF

          # Build the Main Data Load Email Feedback Body...
     printf "\n\t All,"                                                >  ${email_feedback_body}
     printf "\n\n\t Please find the attachments as a feedback to "     >> ${email_feedback_body}
     printf "\n\t the Main Data Load script from the Bookfair"         >> ${email_feedback_body}
     printf "\n\t database ${app_env} environment."                    >> ${email_feedback_body}
     printf "\n\n\t Thank you,"                                        >> ${email_feedback_body}
     printf "\n\t The Data Team\n"                                     >> ${email_feedback_body}
     
          # Release 25.3
          # Build the Calendar Data Error Email Feedback Body 
          #   only when error file exists...
     if [[ -e ${DB_HOME}/${DATA_FILE_PREFIX}_CALENDAR_ERRORS_${exec_date}_${exec_time}.csv ]]; then
        printf "\n\t All,"                                                >  ${calendar_error_email}
        printf "\n\n\t Please find the attached file representing"     >> ${calendar_error_email}
        printf "\n\t the list of Calendar emails that errored out"     >> ${calendar_error_email}
        printf "\n\t during data import into the CPT database due to"  >> ${calendar_error_email}
        printf "\n\t either inexistant email or inexistant calendar"   >> ${calendar_error_email}
        printf "\n\t type."                                            >> ${calendar_error_email}
        printf "\n\n\t Thank you,"                                     >> ${calendar_error_email}
        printf "\n\t The Data Team\n"                                  >> ${calendar_error_email}
     fi
     
     
          # Check if we have not lost a connection to the fpt server
     fn_ftp_connection_check  $?  cis_main_load_ftp  PUT_FILES
     
     #fi 
          #-------------------------------
          #     Check logs for errors
          #-------------------------------
     
     ERRORCHK=$(grep -c 'Error' $log)
     printf "\n\n     ERRORCHK        : ${ERRORCHK}\n\n"         >> $log
     printf "\n\n     IMP_FAIL        : ${IMP_FAIL}\n\n"         >> $log
     printf "\n\n     CPW_FAILED_FILES: ${CPW_FAILED_FILES}\n\n" >> $log
     
          # Sending email to specific users based 
          # on DEV, QA, and Productions environment...
     
          # -----------------------------------
          # this will create the attachements
          # -----------------------------------
               # Systematically add the log file to the list of 
               # files with import problems to be used by default 
               # in the email...
               # As a reminder, the variable ${CPW_FAILED_FILES}
               # represents the "-a ..." section of file attachments.
               # Leave the "uuencode" as is in this script!
     
     if (( $IMP_FAIL != 0 )); then
          fn_process_message_files  cis_load
          
          ###log=$BKF_ENCODE
          if [[ "${app_env}" == "dev"   ||
                "${app_env}" == "dev02"   ]] &&
             (( $ERRORCHK != 0 )); then
               #uuencode $log $log >> $BKF_ENCODE
               CPW_FAILED_FILES="$CPW_FAILED_FILES -a ${log}"
               mail -s "db2_task_failure-${host_name}-${app_env}-${subject_line_4}" \
                    ${CPW_FAILED_FILES} "${email_dev}"  <  ${email_feedback_body}
          fi
     
          if [[ "${app_env}" == "qa"    ||
                "${app_env}" == "qa02"  ||
                "${app_env}" == "uat01"    ]] &&
             (( $ERRORCHK != 0 )); then
               #uuencode $log $log >> $BKF_ENCODE
               CPW_FAILED_FILES="$CPW_FAILED_FILES -a ${log}"
               mail -s "db2_task_failure-${host_name}-${app_env}-${subject_line_4}"  \
                    ${CPW_FAILED_FILES} "${email_qa}"  <  ${email_feedback_body}
          fi
     fi
     
          # -----------------------------------
          # Send script log feedback email...
          # -----------------------------------
     
     if [[ "${app_env}" == "dev"   ||
           "${app_env}" == "dev02"   ]] &&
        (( $ERRORCHK == 0 )); then
          #uuencode $log $log >> $BKF_ENCODE
          CPW_FAILED_FILES="$CPW_FAILED_FILES -a ${log}"
          mail -s "db2_task_success-${host_name}-${app_env}-${subject_line_success}" \
               ${CPW_FAILED_FILES} "${email_dev_run}"  <  ${email_feedback_body}
          
          # Release 25.3
          # Send specific email if Calendar Error file exits...
          
        if [[ -e ${DB_HOME}/${DATA_FILE_PREFIX}_CALENDAR_ERRORS_${exec_date}_${exec_time}.csv ]]; then
             mail -s "db2-${host_name}-${app_env}-CPT Calendar Data Error Report" \
                  -a ${DB_HOME}/${DATA_FILE_PREFIX}_CALENDAR_ERRORS_${exec_date}_${exec_time}.csv \
                  "${email_dev_run}"  < ${calendar_error_email}
        fi
     
     elif [[ "${app_env}" == "qa"    ||
             "${app_env}" == "qa02"  ||
             "${app_env}" == "uat01"   ]] &&
          (( $ERRORCHK == 0 )); then
          #uuencode $log $log >> $BKF_ENCODE
          CPW_FAILED_FILES="$CPW_FAILED_FILES -a ${log}"
          mail -s "db2_task_success-${host_name}-${app_env}-${subject_line_success}" \
               ${CPW_FAILED_FILES} "${email_qa_run}"  <  ${email_feedback_body}
     
               # Release 25.3
               # Send specific email if Calendar Error file exits...
     
          if [[ -e ${DB_HOME}/${DATA_FILE_PREFIX}_CALENDAR_ERRORS_${exec_date}_${exec_time}.csv ]]; then
             mail -s "db2-${host_name}-${app_env}-CPT Calendar Data Error Report" \
                  -a ${DB_HOME}/${DATA_FILE_PREFIX}_CALENDAR_ERRORS_${exec_date}_${exec_time}.csv \
                  "${email_qa_run}"  < ${calendar_error_email}
          fi
     
     elif [[ "${app_env}" == "prod" ]] && 
          (( $ERRORCHK != 0 )); then
          #uuencode $log $log >> $BKF_ENCODE
          CPW_FAILED_FILES="$CPW_FAILED_FILES -a ${log}"
          mail -s "db2_task_failure-${host_name}-${app_env}-${prod_subject_line_failure}" \
               ${CPW_FAILED_FILES} "${email_prod_support}"  <  ${email_feedback_body}

               # Release 25.3
               # Send specific email if Calendar Error file exits...
     
         if [[ -e ${DB_HOME}/${DATA_FILE_PREFIX}_CALENDAR_ERRORS_${exec_date}_${exec_time}.csv ]]; then
            mail -s "db2-${host_name}-${app_env}-CPT Calendar Data Error Report" \
                 -a ${DB_HOME}/${DATA_FILE_PREFIX}_CALENDAR_ERRORS_${exec_date}_${exec_time}.csv \
                 "ContactCenterAlerts@scholastic.com"  < ${calendar_error_email}
         fi
     else
         uuencode $log $log >> $BKF_ENCODE
         CPW_FAILED_FILES="$CPW_FAILED_FILES -a ${log}"
         mail -s "db2_task_success-${host_name}-${app_env}-${subject_line_success}" \
              ${CPW_FAILED_FILES} "${email_prod_support}"  <  ${email_feedback_body}

               # Release 25.3
               # Send specific email if Calendar Error file exits...
     
         if [[ -e ${DB_HOME}/${DATA_FILE_PREFIX}_CALENDAR_ERRORS_${exec_date}_${exec_time}.csv ]]; then
            mail -s "db2-${host_name}-${app_env}-CPT Calendar Data Error Report" \
                 -a ${DB_HOME}/${DATA_FILE_PREFIX}_CALENDAR_ERRORS_${exec_date}_${exec_time}.csv \
                 "ContactCenterAlerts@scholastic.com"  < ${calendar_error_email}
         fi
     fi
     
        # Time to clean up old logs...
     fn_directory_cleanup
     
     
     #################################################################
     #################################################################
     #        SECOND SECTION: BKFAIR PRE-LOAD (early morning)
     #                    
     # Purpose : This script exports 3 files at Business request 
     #           These files have been extracted from the main loading
     #           script (first section above) and consist of:
     #              - "env"_FF_EXPORT
     #              - "env"_REWARDS_EXPORT
     #              - "env"_OFE_EXPORT
     #           Business agreed that the data exported in these files
     #           are about 20 hours old as the data in Bookfair is 
     #           being refreshed 2 to 3 hours after this script will
     #           have run.
     #           Also, Business wants to have that script run earlier
     #           in the morning prior to the script "bkfair_load.sh".
     #
     # Remark  : Original Script was known as:
     #           bkfair_load_secondary_export.ksh
     #
     # History : 10/15/2013 (fc) Release 24 (V-20)
     #           1- Add Filtering out 3 more products 
     #              [ 'ME', 'MM', 'MB' ]
     #              as well as 2 more that should have been added on 
     #              Release 23. [ 'TB', 'BB' ]
     #           2- Add multiple environments handling
     #           
     #################################################################
     #################################################################

elif [[ "${script_action}" == "secondary_load" ]]; then

          #-----------------------------------------------------------
          # Subsection repeated for each Action Parameter Value:
          #    1- Set the Email Subject Line
          #    2- Set the eamil Subject Line Success
          #    3- Set the email Prod Subject Line Failure
          #-----------------------------------------------------------
          
     subject_line="${app_env}-Bkfair Secondary Load Process "
     subject_line_success="${app_env}-Bkfair Secondary load process completed"
     prod_subject_line_failure="${app_env}-Bkfair Secondary Load Process Failed"
     
          # Set email subject lines and recipient to test recipients
          #   when the option of "dev_test_run" has been set as an 
          #   option parameter.
     if [[ "${script_option}" == "dev_test_run" ]]; then
          fn_mail_test_setup
          subject_line="Test-${user_name}-${app_env}-Bkfair Secondary Load Process "
          subject_line_success="Test-${user_name}-${app_env}-Bkfair Secondary load process completed"
          prod_subject_line_failure="Test-${user_name}-${app_env}-Bkfair Secondary Load Process Failed"
     fi
     
     echo "----------------------------------------------------------"  >> $log
     echo "  Starting Bkfair Secondary Load at $(date)..."              >> $log
     echo "----------------------------------------------------------"  >> $log

     printf "\n====================  Connecting to DB ${db_name}... \n" >> $log
     db2 -v connect to ${db_name}     >> $log
     fn_db2Check $? ${db_name}_CONNECT  DB2_CONNECT_CMD
     
     db2 -v SET SCHEMA ${schema_name} >> $log
     fn_db2Check $? ${schema_name}  SET_SCHEMA_CMD

     printf "\n----------------------------------------------------------"    >>  $log
     printf "\n  Update the status of OFE_ELIGIBLE table... at $(date)..."    >>  $log 
     printf "\n----------------------------------------------------------\n"  >>  $log

     db2 "CALL bkfair01.sp_bkfair_ofe_stsupdt() " >> $log

     printf "\n----------------------------------------------------------"   >>  $log
     printf "\n      Start FF Financial Data Export at $(date)..."           >>  $log 
     printf "\n----------------------------------------------------------\n" >>  $log

     db2 "EXPORT TO '${DB_HOME}/${DATA_FILE_PREFIX}_FF_EXPORT' OF DEL \
             SELECT * FROM bkfair01.v_ff_export_rpt\
             FOR READ ONLY WITH UR " | sed 's/  */ /g' >> $log
     fn_db2Check $?  V_FF_EXPORT_RPT  EXPORT_CMD

          # Reward Export data based on the Financial 
          # submitted date not by Rewards create date

     printf "\n----------------------------------------------------------"   >>  $log
     printf "\n      Start Rewards Data Export at $(date)..."                >>  $log 
     printf "\n----------------------------------------------------------\n" >>  $log

     db2 "EXPORT TO '${DB_HOME}/${DATA_FILE_PREFIX}_REWARDS_EXPORT' OF DEL \
             SELECT * FROM bkfair01.v_rewards_export_rpt \
             FOR READ ONLY WITH UR " | sed 's/  */ /g' >> $log
     fn_db2Check $?  V_REWARDS_EXPORT_RPT  EXPORT_CMD

          #     Bookfair Release 18, May 2011, 
          # OFE_EXPORT data is to get the data from the last exec_date
          # of the crontab or Control-M script.
          # This code has been reviewed to make use of the new table 
          # MAINT_BKFAIR_TASKS that tracks the time of the past export actions.
     
          # Get the most recent date the "data export" as run as Business
          #  wants just the delta of data created since last data load for OFE data...
          # Per Beth T. request, one of the OFE export needs to extract all the data and
          #  not just the delta.  The early morning task is better suited to do so.

     new_export_exec_timestamp=$(db2 -x "SELECT CURRENT TIMESTAMP FROM sysibm.sysdummy1" )

     printf "\n----------------------------------------------------------"   >>  $log
     printf "\n      Start ONLINE_SHOPPING Data Export at $(date)..."        >>  $log 
     printf "\n----------------------------------------------------------\n" >>  $log
     
     db2 "EXPORT TO '${DB_HOME}/${DATA_FILE_PREFIX}_OFE_EXPORT' OF DEL \
               SELECT t1.fair_id, \
                      t1.ofe_start_date, \
                      t1.ofe_end_date, \
                      t1.ofe_status \
               FROM bkfair01.v_ofe_export_rpt  t1, \
                    bkfair01.online_shopping   t2, \
                    bkfair01.fair              f \
               WHERE BIGINT(t1.fair_id) = t2.fair_id \
                 AND DATE(t2.updated_date) >= CURRENT DATE - 7 DAYS \
                 AND t2.fair_id = f.id \
                 AND f.product_id NOT IN \
                    ( 'BE', 'BM', 'BP', 'TM', 'TT', \
                      'TB', 'BB', 'ME', 'MM', 'MB' ) \
               FOR READ ONLY WITH UR "  | sed 's/  */ /g' >> $log
     fn_db2Check $?  V_OFE_EXPORT_RPT  EXPORT_CMD

          # Register this new task action in maintenance table...
     db2 -av "INSERT INTO ${schema_name}.maint_bkfair_tasks \
                 (task_id, create_timestamp, task_name, script_name) \
                VALUES ( (SELECT MAX(task_id) + 1 FROM ${schema_name}.maint_bkfair_tasks), \
                         '${new_export_exec_timestamp}', \
                         'Bkfair Data Export Secondary', \
                         '${app_env}-Bkfair_cis_load.secondary_load' ) " | sed 's/  */ /g' >> $log

     db2 -v terminate  >> $log

     printf "\n----------------------------------------------------------"    >> $log
     printf "\n        Start FTP Secondary Up-Load connect at $(date)... "    >> $log
     printf "\n----------------------------------------------------------\n"  >> $log

ftp -iv ${node_ip} << EOF >> $log
binary
lcd ${DB_HOME}
cd ${FTP_HOME}
bin
put ${DATA_FILE_PREFIX}_FF_EXPORT ${DATA_FILE_PREFIX}_FF_EXPORT
put ${DATA_FILE_PREFIX}_REWARDS_EXPORT ${DATA_FILE_PREFIX}_REWARDS_EXPORT
put ${DB_HOME}/${DATA_FILE_PREFIX}_OFE_EXPORT ${DATA_FILE_PREFIX}_OFE_EXPORT
bye
EOF
          # End of FTPing.
     
          # Check if we have not lost a connection to the fpt server
     fn_ftp_connection_check  $?  load_secondary_ftp  PUT_FILES

          # some feedback data...
     printf "\n\n     ERRORCHK        : ${ERRORCHK}\n\n"         >> $log
     printf "\n\n     IMP_FAIL        : ${IMP_FAIL}\n\n"         >> $log
     printf "\n\n     CPW_FAILED_FILES: ${CPW_FAILED_FILES}\n\n" >> $log

          # Sending email to specific users based 
          # on DEV, QA, and Productions environment...

          # Build the Secondary Load Email Feedback Body...
     printf "\n\t All,"                                                >  ${email_feedback_body}
     printf "\n\n\t Please find the attachments as a feedback to "     >> ${email_feedback_body}
     printf "\n\t the Secondary Load script from the Bookfair "        >> ${email_feedback_body}
     printf "\n\t database in ${app_env} environment."                 >> ${email_feedback_body}
     printf "\n\n\t Thank you"                                         >> ${email_feedback_body}
     printf "\n\t The Data Team\n"                                     >> ${email_feedback_body}
     
     if [[ "${app_env}" == "dev"   ||
           "${app_env}" == "dev02"   ]]; then
          mail -s "db2_task_success-${host_name}-${app_env}_${subject_line_success}" \
               -a ${log}  "${email_dev_run}" < ${email_feedback_body} 
     
     elif [[ "${app_env}" == "qa"    ||
             "${app_env}" == "qa02"  ||
             "${app_env}" == "uat01"   ]]; then
          mail -s "db2_task_success-${host_name}-${app_env}_${subject_line_success}" \
               -a ${log}  "${email_qa_run}" < ${email_feedback_body}
     
     elif [[ "${app_env}" == "prod" ]] && 
          [[ -e $error_file ]]; then
          
          cat $log >> $error_file
          mail -s "db2_task_failure-${host_name}-${app_env}_${prod_subject_line_failure}" \
               -a ${error_file}  "${email_prod_support}"  < ${email_feedback_body}
     
     else
         mail -s "db2_task_success-${host_name}-${app_env}_${subject_line_success}" \
              -a ${log}  "${email_prod_support}" < ${email_feedback_body}
     fi
     
        # Time to clean up old logs...
     fn_directory_cleanup
     
     
     #################################################################
     #################################################################
     #               THIRD SECTION: BKFAIR HOURLY_OFE
     #                    
     # Purpose : Business requested to export on a more frequent basis
     #           the content of table "bkfair01.online_shopping" to be
     #           executed hourly from 8:00 AM to 20:00.
     #
     # Remark  : Original Script was known as:
     #           bkfair_daily_ftp_data_export.ksh
     #
     # History : 10/15/2013 (fc) Release 24 (V-20)
     #           Add Filtering out 3 more products 
     #           [ 'ME', 'MM', 'MB' ]
     #           as well as 2 more that should have been added on 
     #           Release 23. [ 'TB', 'BB' ]
     #           
     #################################################################
     #################################################################
     
elif [[ "${script_action}" == "hourly_ofe" ]]; then

          #-----------------------------------------------------------
          # Subsection repeated for each Action Parameter Value:
          #    1- Set the Email Subject Line
          #-----------------------------------------------------------
          
     subject_line="${app_env}-Bookfair multi daily OFE Data Export"
     mailout_list="fcabaret@scholastic.com,MNaresh@scholastic.com,RAvala@scholastic.com,BTracey@scholastic.com"
     mailout_list_error="fcabaret@scholastic.com,MNaresh@scholastic.com,RAvala@scholastic.com"

          # Set email subject lines and recipient to test recipients
          #   when the option of "dev_test_run" has been set as an 
          #   option parameter.
     if [[ "${script_option}" == "dev_test_run" ]]; then
          subject_line="Test-${user_name}-${app_env}_Bookfair multi daily OFE Data Export"
          fn_mail_test_setup
     fi

     echo "----------------------------------------------------------"  >> $log
     echo "  Starting Bkfair Hourly OFE at $(date)..."                  >> $log
     echo "----------------------------------------------------------"  >> $log
     
     printf "\n All,"                                                       >> $log
     printf "\n\n The following output represents the feedback"             >> $log
     printf "\n from the script that exports and ftp the ONLINE_SHOPPING "  >> $log
     printf "\n codes data multiple times daily.\n\n"                       >> $log
     
     db2 -v "connect to ${db_name}"  >>  $log
     fn_db2Check $? ${db_name}_CONNECT  DB2_CONNECT_CMD
     db2 -v "SET SCHEMA ${schema_name}"  >> $log
     fn_db2Check $? ${schema_name}  SET_SCHEMA_CMD

          # Export the ONLINE_SHOPPING data...
     printf "\n\n ONLINE_SHOPPING Export data started on $(date)...\n\n" >> $log

          # "Bkfair Data Export Secondary" value is populated by 
          #    the second section of this very script.

     last_ofe_export_date=$(db2 -x \
          "SELECT MAX(create_timestamp) \
           FROM ${schema_name}.maint_bkfair_tasks \
           WHERE task_name IN ('Bkfair Data Export Secondary', 'OFE Data Export') ")

     new_export_exec_timestamp=$(db2 -x \
          "SELECT CURRENT TIMESTAMP FROM sysibm.sysdummy1" )

     db2 -v "EXPORT TO '${DB_HOME}/${DATA_FILE_PREFIX}_OFE_EXPORT' OF DEL \
                SELECT t1.fair_id, \
                       t1.ofe_start_date, \
                       t1.ofe_end_date, \
                       t1.ofe_status \
                FROM ${schema_name}.v_ofe_export_rpt  t1, \
                     ${schema_name}.online_shopping   t2, \
                     ${schema_name}.fair              f \
                WHERE BIGINT(t1.fair_id) = t2.fair_id \
                  AND t2.updated_date >= '${last_ofe_export_date}' \
                  AND t2.fair_id = f.id \
                  AND f.product_id NOT IN \
                     ( 'BE', 'BM', 'BP', 'TM', 'TT', \
                       'TB', 'BB', 'ME', 'MM', 'MB' ) \
                FOR READ ONLY WITH UR "  | sed 's/  */ /g' >> $log
     fn_db2Check $? ONLINE_SHOPPING  EXPORT_CMD
     
     # Register this new task action in maintenance table...
     db2 -v "INSERT INTO ${schema_name}.maint_bkfair_tasks \
             (task_id, create_timestamp, task_name, script_name) \
             VALUES ( (SELECT MAX(task_id) + 1 FROM ${schema_name}.maint_bkfair_tasks), \
                      '${new_export_exec_timestamp}', \
                      'OFE Data Export', \
                      '${app_env}-Bkfair_cis_load.hourly_ofe' ) "  | sed 's/  */ /g' >> $log

     printf "\n\n Export ONLINE_SHOPPING data ended on $(date)...\n" >> $log

     db2 -v "terminate"  >> $log

     echo "----------------------------------------------------------" >> $log
     echo "     Start FTP Up-Load connect at $(date)... "              >> $log
     echo "     FTP CMD specific to script_action = hourly_ofe "       >> $log
     echo "----------------------------------------------------------" >> $log

ftp -iv ${node_ip} << EOF >> $log 
binary
lcd ${DB_HOME}
cd ${FTP_HOME}
bin
put ${DATA_FILE_PREFIX}_OFE_EXPORT   ${DATA_FILE_PREFIX}_OFE_EXPORT
bye
EOF

          # End of FTPing.
     
          # Check if we have not lost a connection to the fpt server
     fn_ftp_connection_check  $?  hourly_ofe  PUT_FILES

          # some feedback data...
     printf "\n\n     ERRORCHK        : ${ERRORCHK}\n\n"         >> $log
     printf "\n\n     IMP_FAIL        : ${IMP_FAIL}\n\n"         >> $log
     printf "\n\n     CPW_FAILED_FILES: ${CPW_FAILED_FILES}\n\n" >> $log

          #--------------------------------------------------------
          #  Send Feedback email...
          #--------------------------------------------------------

          # Build the Hourly OFE Email Feedback Body...
     printf "\n\t All,"                                                >  ${email_feedback_body}
     printf "\n\n\t Please find the attachments as a feedback to "     >> ${email_feedback_body}
     printf "\n\t the Hourly OFE script from the Bookfair "            >> ${email_feedback_body}
     printf "\n\t database in ${app_env} environment."                 >> ${email_feedback_body}
     printf "\n\n\t Thank you"                                         >> ${email_feedback_body}
     printf "\n\t The Data Team\n"                                     >> ${email_feedback_body}
     
     if [[ -e $error_file ]]; then
          mail -s "db2_task_failure -${host_name} - ${subject_line} encountered problems..." \
               -a ${error_file}  "$mailout_list_error"  <  ${email_feedback_body}
          mail -s "db2_task_failure -${host_name} - ${subject_line}" \
               -a ${log}  "$mailout_list"  <  ${email_feedback_body}

     else
          mail -s "db2_task_success -${host_name} - ${subject_line} Successful" \
               -a ${log}  "$mailout_list"  <  ${email_feedback_body}
     fi

        # Time to clean up old logs...
     fn_directory_cleanup
     
     
     #################################################################
     #################################################################
     #               FOURTH SECTION: BKFAIR EMAIL_FORMS
     #                    
     # Purpose : Export various data files concerning Sales 
     #           information from views: 
     #              - V_EXPORT_NOT_CORRECT_RPT
     #              - V_EXPORT_SALES_CONSULTANT_RPT
     #
     # Remark  : Original Script was known as:
     #           bkfair_email_forms_export.sh
     #
     # History : 11/18/2013 (fc) Release 24 (V-20)
     #           Integrate the email_forms script.
     #           
     #################################################################
     #################################################################

elif [[ "${script_action}" == "email_forms" ]]; then

          #-----------------------------------------------------------
          # Subsection repeated for each Action Parameter Value:
          #    1- Set the Email Subject Line
          #-----------------------------------------------------------
     
     subject_line="${app_env}-Bookfair Email Forms Export process completed"
     dev_mailout_list="fcabaret@scholastic.com, nmale@scholastic.com"
     qa_mailout_list="eCommerceDatabaseAdministrators@scholastic.com, fcabaret@scholastic.com, nmale@scholastic.com,ravala@scholastic.com"
     prod_mailout_list="bthorgersen@scholastic.com, eCommerceDatabaseAdministrators@scholastic.com, fcabaret@scholastic.com, nmale@scholastic.com"

          # Set email subject lines and recipient to test recipients
          #   when the option of "dev_test_run" has been set as an 
          #   option parameter.
          
     if [[ "${script_option}" == "dev_test_run" ]]; then
          subject_line="Test-${user_name}-${app_env}_Bookfair Email Forms Export"
          fn_mail_test_setup
     fi

     echo "----------------------------------------------------------"  >> $log
     echo "  Starting Bkfair Email Forms at $(date)..."                 >> $log
     echo "----------------------------------------------------------"  >> $log

     echo "=====  Start to connect to DB on $(date)" >> $log

     db2 connect to ${db_name} >> $log
     fn_db2Check $? ${db_name}_CONNECT  DB2_CONNECT_CMD
     db2 SET SCHEMA ${schema_name} >> $log
     fn_db2Check $? ${schema_name}  SET_SCHEMA_CMD

     new_export_exec_timestamp=$(db2 -x \
          "SELECT CURRENT TIMESTAMP FROM sysibm.sysdummy1" )

     printf "\n=====  Start Email forms Export of ${DATA_FILE_PREFIX}_EXPORT_NOT_CORRECT on $(date) " >> $log

     db2 "EXPORT TO '${DB_HOME}/${DATA_FILE_PREFIX}_EXPORT_NOT_CORRECT' OF DEL \
             SELECT * FROM bkfair01.v_export_not_correct_rpt \
             FOR READ ONLY WITH UR"  | sed 's/  */ /g' >> $log
     fn_db2Check $? EXPORT_NOT_CORRECT  EXPORT_CMD
     
     printf "\n=====  Start Email forms Export of ${DATA_FILE_PREFIX}_EXPORT_SALES_CONSULTANT on $(date) " >> $log

     db2 "EXPORT TO '${DB_HOME}/${DATA_FILE_PREFIX}_EXPORT_SALES_CONSULTANT' OF DEL \
             SELECT * FROM bkfair01.v_export_sales_consultant_rpt \
             FOR READ ONLY WITH UR"  | sed 's/  */ /g' >> $log
     fn_db2Check $? SALES_CONSULTANT  EXPORT_CMD
     printf "\n=====  Start Email forms Export of ${DATA_FILE_PREFIX}_COA_EXPORT on $(date) " >> $log

          # Register this new task action in maintenance table...
     db2 -v "INSERT INTO ${schema_name}.maint_bkfair_tasks \
             (task_id, create_timestamp, task_name, script_name) \
             VALUES ( (SELECT MAX(task_id) + 1 FROM ${schema_name}.maint_bkfair_tasks), \
                      '${new_export_exec_timestamp}', \
                      'Email Forms Export', \
                      '${app_env}-Bkfair_cis_load.email_forms' ) "  | sed 's/  */ /g' >> $log
     
     db2 terminate >> $log
     
     echo "----------------------------------------------------------" >> $log
     echo "     Start FTP Up-Load connect at $(date)... "              >> $log
     echo "     FTP CMD specific to script_action = email_forms "      >> $log
     echo "----------------------------------------------------------" >> $log
     
ftp -iv ${node_ip} << EOF >> $log
binary
lcd ${DB_HOME}
cd ${FTP_HOME}
bin
put ${DATA_FILE_PREFIX}_EXPORT_NOT_CORRECT ${DATA_FILE_PREFIX}_EXPORT_NOT_CORRECT
put ${DATA_FILE_PREFIX}_EXPORT_SALES_CONSULTANT ${DATA_FILE_PREFIX}_EXPORT_SALES_CONSULTANT
bye
EOF

          # End of FTPing.
          # Check if we have not lost a connection to the fpt server
     fn_ftp_connection_check  $?  email_forms  PUT_FILES

          # some feedback data...
     printf "\n\n     ERRORCHK        : ${ERRORCHK}\n\n"         >> $log
     printf "\n\n     IMP_FAIL        : ${IMP_FAIL}\n\n"         >> $log
     printf "\n\n     CPW_FAILED_FILES: ${CPW_FAILED_FILES}\n\n" >> $log

          #--------------------------------------------------------
          #  Send Feedback email...
          #--------------------------------------------------------

          # Build the "Email Forms" Email Feedback Body...
     printf "\n\t All,"                                                >  ${email_feedback_body}
     printf "\n\n\t Please find the attachments as a feedback to "     >> ${email_feedback_body}
     printf "\n\t the Email Forms script from the Bookfair "           >> ${email_feedback_body}
     printf "\n\t database in ${app_env} environment."                 >> ${email_feedback_body}
     printf "\n\n\t Thank you"                                         >> ${email_feedback_body}
     printf "\n\t The Data Team\n"                                     >> ${email_feedback_body}

     if [[ "${app_env}" == "dev"   ||
           "${app_env}" == "dev02"   ]]; then
          mailx -s "db2_task_success-${host_name}-${subject_line}" \
                -a ${log}  "${dev_mailout_list}"  < ${email_feedback_body}
     
     elif [[ "${app_env}" == "qa"    ||
             "${app_env}" == "qa02"  ||
             "${app_env}" == "uat01"   ]]; then
          mailx -s "db2_task_success-${host_name}-${subject_line}" \
                -a ${log}  "${qa_mailout_list}"  < ${email_feedback_body} 
     
     elif [[ "${app_env}" == "prod" ]]; then
          mailx -s "db2_task_success-${host_name}-${subject_line}" \
                -a ${log}  "${prod_mailout_list}" < ${email_feedback_body} 
     fi

        # Time to clean up old logs...
     fn_directory_cleanup


     #################################################################
     #################################################################
     #               FIFTH SECTION: BKFAIR REWARDS_LOAD
     #                    
     # Purpose : Load Reward Data using a combination of CIS flat
     #           files Imports and Stored Procedure execution. 
     #
     # Remark  : Original Script was known as:
     #           bkfair_rewards_load.sh
     #
     # History : 11/18/2013 (fc) Release 24 (V-20)
     #           Integrate the Rewards load script.
     #           
     #################################################################
     #################################################################

elif [[ "${script_action}" == "rewards_load" ]]; then

          #----------------------------------
          #  Setting emails values...
          #----------------------------------
     
     subject_line="${app_env}-Bkfair Rewards Load Process"
     subject_line_4="${app_env}-Bkfair Rewards - 1 or more CIS procedures encountered errors"
     mailout_list="eCommerceDatabaseAdministrators@scholastic.com, fcabaret@scholastic.com, nmale@scholastic.com"

          # Set email subject lines and recipient to test recipients
          #   when the option of "dev_test_run" has been set as an 
          #   option parameter.
          
     if [[ "${script_option}" == "dev_test_run" ]]; then
          subject_line="Test-${user_name}-${app_env}-Bkfair Rewards Load Process"
          subject_line_4="Test-${user_name}-${app_env}-Bkfair Rewards - 1 or more CIS procedures encountered errors"
          fn_mail_test_setup
     fi

     echo "----------------------------------------------------------"  >> $log
     echo "  Starting Bkfair Rewards Load at $(date)..."                >> $log
     echo "----------------------------------------------------------"  >> $log

     echo "  Archiving of yesterday Rewards CPW files at $(date) ..."   >> $log

     mv ${DATA_FILE_PREFIX}_CUSTOMER_REWARDS_PARENT_CPW \
        ${DATA_FILE_PREFIX}_CUSTOMER_REWARDS_PARENT_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_CUSTOMER_REWARDS_PARENT_CPW_prev.${exec_date}_${exec_time}

     mv ${DATA_FILE_PREFIX}_CUSTOMER_REWARDS_CHILD_CPW \
        ${DATA_FILE_PREFIX}_CUSTOMER_REWARDS_CHILD_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_CUSTOMER_REWARDS_CHILD_CPW_prev.${exec_date}_${exec_time}

     echo "----------------------------------------------------------"  >> $log
     echo "   Starting Rewards FTP Download at $(date) ..."             >> $log
     echo "----------------------------------------------------------"  >> $log

ftp -iv ${node_ip} << EOF >> $log 
binary
lcd ${DB_HOME}
cd ${FTP_HOME}
get ${DATA_FILE_PREFIX}_CUSTOMER_REWARDS_PARENT_CPW
get ${DATA_FILE_PREFIX}_CUSTOMER_REWARDS_CHILD_CPW
bye
EOF
          # End of FTPing.
     
          # Check if we have not lost a connection to the fpt server
     fn_ftp_connection_check  $?  cis_get_rewards_ftp  GET_FILES
     

     if [ -f ${DATA_FILE_PREFIX}_CUSTOMER_REWARDS_PARENT_CPW ]; then
          echo "----------------------------------------------------------" >> $log
          echo "   Starting Loading CIS Rewards CPW File at $(date)... "    >> $log
          echo "----------------------------------------------------------" >> $log
          
     else
          printf "\nAll, "                                            >> $log
          printf "\n\n Error: CIS REWARDS CPW file was not found  "   >> $log 
          printf "\n on the FTP site."                                >> $log
          printf "\n The File was therefore not loaded!"              >> $log
          printf "\n\n The Data Team\n"                               >> $log
          
          mail -s "db2_task_failure-${host_name}-${app_env}-${subject_line} Failed.  FTP File Missing" \
                  "${mailout_list}" < $log 
          exit 1
     fi

     echo "===================  Start to connect to DB on $(date)" >> $log

     db2 connect to ${db_name} >> $log
     fn_db2Check $? ${db_name}_CONNECT  DB2_CONNECT_CMD
     
     db2 SET SCHEMA ${schema_name} >> $log
     fn_db2Check $? ${schema_name}  SET_SCHEMA_CMD
     
     new_export_exec_timestamp=$(db2 -x \
          "SELECT CURRENT TIMESTAMP FROM sysibm.sysdummy1" )

     echo "=================== Start Importing into CUSTOMER_REWARDS_PARENT_LOAD on $(date)..." >> $log
     
     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_CUSTOMER_REWARDS_PARENT_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_CUSTOMER_REWARDS_PARENT_CPW' OF DEL \
             MODIFIED BY COMPOUND=5 \
             COMMITCOUNT 5000 \
             MESSAGES '$MSGFILE' \
             REPLACE INTO bkfair01.customer_rewards_parent_load" >> $log
     fn_import_process_log $? $MSGFILE
     
     echo "=================== Start Importing into CUSTOMER_REWARDS_CHILD_LOAD on $(date)..." >> $log

     MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_CUSTOMER_REWARDS_CHILD_CPW.txt
     echo "" > $MSGFILE
     echo "Import message file: $MSGFILE" >> $log
     db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_CUSTOMER_REWARDS_CHILD_CPW' OF DEL \
             MODIFIED BY COMPOUND=5 \
             COMMITCOUNT 5000 \
             MESSAGES '$MSGFILE' \
             REPLACE INTO bkfair01.customer_rewards_child_load" >> $log
     fn_import_process_log $? $MSGFILE
     
     echo "===================  Start Running p_rewards_parent on $(date)... " >> $log
 
     db2 "call bkfair01.p_rewards_parent()" >> $log

     echo "===================  Start Running p_rewards_child on $(date) " >> $log
     
     db2 "call bkfair01.p_rewards_child()" >> $log

     echo "===================  Terminate Connection to Bkfair01 on $(date) " >> $log
     
          # Register this new task action in maintenance table...
     db2 -v "INSERT INTO ${schema_name}.maint_bkfair_tasks \
             (task_id, create_timestamp, task_name, script_name) \
             VALUES ( (SELECT MAX(task_id) + 1 FROM ${schema_name}.maint_bkfair_tasks), \
                      '${new_export_exec_timestamp}', \
                      'Rewards CPW Load', \
                      '${app_env}-Bkfair_cis_load.rewards_load' ) "  | sed 's/  */ /g' >> $log

     db2 terminate  >> $log

          # Build the "Rewards Load" Email Feedback Body...
     printf "\n\t All,"                                                >  ${email_feedback_body}
     printf "\n\n\t Please find the attachments as a feedback to "     >> ${email_feedback_body}
     printf "\n\t the Rewards Load script from the Bookfair "          >> ${email_feedback_body}
     printf "\n\t database in ${app_env} environment."                 >> ${email_feedback_body}
     printf "\n\n\t Thank you"                                         >> ${email_feedback_body}
     printf "\n\t The Data Team\n"                                     >> ${email_feedback_body}

          #-------------------------------
          #     Check logs for errors
          #-------------------------------

     ERRORCHK=$(grep -c 'Error' $log)
     printf "\n\n     ERRORCHK : ${ERRORCHK}\n\n" >> $log

          # some feedback data...
     printf "\n\n     ERRORCHK        : ${ERRORCHK}\n\n"         >> $log
     printf "\n\n     IMP_FAIL        : ${IMP_FAIL}\n\n"         >> $log
     printf "\n\n     CPW_FAILED_FILES: ${CPW_FAILED_FILES}\n\n" >> $log

          # -----------------------------------
          # this will create the attachements
          # -----------------------------------
               # Systematically add the log file to the list of 
               # files with import problems to be used by default 
               # in the email...
               # As a reminder, the variable ${CPW_FAILED_FILES}
               # represents the "-a ..." section of file attachments.
               # Leave the "uuencode" as is in this script!
     
     if (( $IMP_FAIL != 0 )); then
          fn_process_message_files  rewards_load
          
          ###log=$BKF_ENCODE
          if [[ "${app_env}" == "dev"   ||
                "${app_env}" == "dev02"   ]] &&
             (( $ERRORCHK != 0 )); then
               #uuencode $log $log >> $BKF_ENCODE
               CPW_FAILED_FILES="$CPW_FAILED_FILES -a ${log}"
               mail -s "db2_task_failure-${host_name}-${app_env}-${subject_line_4}" \
                    ${CPW_FAILED_FILES} "${mailout_list}"  <  ${email_feedback_body}
          fi
     
          if [[ "${app_env}" == "qa"    ||
                "${app_env}" == "qa02"  ||
                "${app_env}" == "uat01"   ]] &&
             (( $ERRORCHK != 0 )); then
               #uuencode $log $log >> $BKF_ENCODE
               CPW_FAILED_FILES="$CPW_FAILED_FILES -a ${log}"
               mail -s "db2_task_failure-${host_name}-${app_env}-${subject_line_4}"  \
                    ${CPW_FAILED_FILES} "${mailout_list}"  <  ${email_feedback_body}
          fi
     fi   # end if (( $IMP_FAIL != 0 )); then

     mailx -s "db2_task_success-${host_name}-${app_env}-${subject_line} Completed"  \
              -a ${log} "${mailout_list}"  <  ${email_feedback_body}
     
        # Time to clean up old logs...
     fn_directory_cleanup
     
     
     #################################################################
     #################################################################
     #               SIXTH SECTION: BKFAIR TAX_LOAD
     #                    
     # Purpose : Load Tax Data using a combination of CIS flat
     #           files Imports and Exports.
     #           This section imports and exports Tax related data 
     #           on a daily basis.
     #           This implementation is part of Release_22 of Bkfair.
     #            All the data coming in represents delta data from the
     #            backend going back 7 days only.
     #
     # Remark  : Original Script was known as:
     #           bkfair_tax_load.ksh
     #
     # History : 11/25/2013 (fc) Release 24 (V-21)
     #           Integrate the Tax load script.
     #           
     #################################################################
     #################################################################

elif [[ "${script_action}" == "tax_load" ]]; then
          # Setting emails values...
     subject_line_failure="Bkfair Tax Import Export process failed!!! file not found"
     subject_line_2="Dev Malformed Tax Import Files"
     subject_line_3="QA Malformed Tax Import Files"
     subject_line_4=" An Error Occurred During Tax File Loading"
     subject_line_success="Bkfair Tax Import Export Process Completed"
     prod_subject_line_failure="Prod Bkfair Tax Process Failed"
     
          
     if [[ "${script_option}" == "dev_test_run" ]]; then
          fn_mail_test_setup
          subject_line_failure="Test-${user_name}-${app_env}-Bkfair Tax Import Export process failed!!! file not found"
          subject_line_2="Test-${user_name}-${app_env}-Malformed Tax Import Files"
          subject_line_3="Test-${user_name}-${app_env}-Malformed Tax Import Files"
          subject_line_4="Test-${user_name}-${app_env}-An Error Occurred During Tax File Loading"
          subject_line_success="Test-${user_name}-${app_env}-Bkfair Tax Import Export Process Completed"
          prod_subject_line_failure="Test-${user_name}-${app_env}-Bkfair Tax Process Failed"
     fi
     
     printf "\n----------------------------------------------------------"     >> $log
     printf "\n  Starting Archiving of yesterday Tax files at $(date) ..."     >> $log
     printf "\n----------------------------------------------------------\n\n" >> $log
     
     mv ${DATA_FILE_PREFIX}_SCHOOL_TAX_SELECTION_CPW \
        ${DATA_FILE_PREFIX}_SCHOOL_TAX_SELECTION_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_SCHOOL_TAX_SELECTION_CPW_prev.${exec_date}_${exec_time}
     
     mv ${DATA_FILE_PREFIX}_SCHOOL_TAX_STATUS_CPW \
        ${DATA_FILE_PREFIX}_SCHOOL_TAX_STATUS_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_SCHOOL_TAX_STATUS_CPW_prev.${exec_date}_${exec_time}
     
     mv ${DATA_FILE_PREFIX}_TAX_SCHOOL_TYPES_CPW \
        ${DATA_FILE_PREFIX}_TAX_SCHOOL_TYPES_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_TAX_SCHOOL_TYPES_CPW_prev.${exec_date}_${exec_time}
     
     mv ${DATA_FILE_PREFIX}_STATE_TAX_CHOICE_CPW \
        ${DATA_FILE_PREFIX}_STATE_TAX_CHOICE_CPW_prev.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_STATE_TAX_CHOICE_CPW_prev.${exec_date}_${exec_time}
     
        # Archive the export file as well...
     mv ${DATA_FILE_PREFIX}_SCHOOL_TAX_SELECTION_EXPORT \
        ${DATA_FILE_PREFIX}_SCHOOL_TAX_SELECTION_EXPORT.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_SCHOOL_TAX_SELECTION_EXPORT.${exec_date}_${exec_time}
     
     mv ${DATA_FILE_PREFIX}_SCHL_SCHOOL_TAX_STATUS_CPW \
        ${DATA_FILE_PREFIX}_SCHL_SCHOOL_TAX_STATUS_CPW.${exec_date}_${exec_time}
     gzip ${DATA_FILE_PREFIX}_SCHL_SCHOOL_TAX_STATUS_CPW.${exec_date}_${exec_time}
     
     printf "\n----------------------------------------------------------"     >> $log
     printf "\n              Starting FTP Download at $(date)..."              >> $log
     printf "\n----------------------------------------------------------\n\n" >> $log
          
          # -----------------------------------
          # Get the CIS Tax Files from the 
          #   regular file server to which 
          #   we access via ftp as agreed
          #   on 04/04/2013.
          # -----------------------------------

ftp -iv ${node_ip} << EOF >> $log 
binary
lcd ${DB_HOME}
cd ${FTP_HOME}
get ${DATA_FILE_PREFIX}_SCHOOL_TAX_SELECTION_CPW
get ${DATA_FILE_PREFIX}_SCHOOL_TAX_STATUS_CPW
get ${DATA_FILE_PREFIX}_TAX_SCHOOL_TYPES_CPW
get ${DATA_FILE_PREFIX}_STATE_TAX_CHOICE_CPW
get ${DATA_FILE_PREFIX}_SCHL_SCHOOL_TAX_STATUS_CPW
bye
EOF

             # Check if we have not lost a connection to the fpt server
        fn_ftp_connection_check  $?  cis_ftp  GET_FILES

     echo "*****Connecting to DB ${db_name} at $(date)... " >> $log
     
     db2 -v connect to ${db_name} >> $log
     db2 -v SET SCHEMA ${schema_name} >> $log
     
          #--------------------------
          # DATA FILE IMPORT SECTION
          #--------------------------
     
     printf "\n Starting Loading ${DATA_FILE_PREFIX}_SCHOOL_TAX_SELECTION_CPW File at $(date)...\n"  >> $log
     
     if [[ -e ${DB_HOME}/${DATA_FILE_PREFIX}_SCHOOL_TAX_SELECTION_CPW ]]  &&
        [[ -s ${DB_HOME}/${DATA_FILE_PREFIX}_SCHOOL_TAX_SELECTION_CPW ]]; then
        
        MSGFILE=$LOG_HOME/msg_${DATA_FILE_PREFIX}_SCHOOL_TAX_SELECTION_CPW.txt
        echo "" > ${MSGFILE}
        printf "\n Import message file: ${MSGFILE} \n" >> $log
        db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_SCHOOL_TAX_SELECTION_CPW' OF DEL \
             MODIFIED BY DATEFORMAT=\"yyyymmdd\" \
             METHOD P(1,2,3) \
             COMMITCOUNT 1000 \
             MESSAGES '${MSGFILE}' \
             INSERT_UPDATE INTO ${schema_name}.school_tax_selection ( \
                school_id, tax_selection, update_date )" | sed 's/  */ /g' >> $log
        fn_import_process_log $? ${MSGFILE}
     
     else
        printf "\n The file ${DATA_FILE_PREFIX}_SCHOOL_TAX_SELECTION_CPW was either absent or empty. \n"  >> $log
     fi   ### ..._SCHOOL_TAX_SELECTION_CPW ]]...
     
     
     printf "\n Starting Loading ${DATA_FILE_PREFIX}_SCHOOL_TAX_STATUS_CPW File at $(date)...\n"  >> $log
     
     if [[ -e ${DB_HOME}/${DATA_FILE_PREFIX}_SCHOOL_TAX_STATUS_CPW ]]  &&
        [[ -s ${DB_HOME}/${DATA_FILE_PREFIX}_SCHOOL_TAX_STATUS_CPW ]]; then
        
        MSGFILE=${LOG_HOME}/msg_${DATA_FILE_PREFIX}_SCHOOL_TAX_STATUS_CPW.txt
        echo "" > ${MSGFILE}
        echo "Import message file: ${MSGFILE}" >> $log
        db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_SCHOOL_TAX_STATUS_CPW' OF DEL \
             METHOD P(1,2,3,4,5) \
             COMMITCOUNT 1000 \
             MESSAGES '${MSGFILE}' \
             INSERT_UPDATE INTO ${schema_name}.school_tax_status ( \
                school_id, tax_status, expiration_date, \
                invalid_reason, update_date )" | sed 's/  */ /g' >> $log
        fn_import_process_log $? ${MSGFILE}
     
     else
        printf "\n The file ${DATA_FILE_PREFIX}_SCHOOL_TAX_STATUS_CPW was either absent or empty. \n"  >> $log
     fi   ### ..._SCHOOL_TAX_STATUS_CPW ]]...
     
     
     printf "\n Starting Loading ${DATA_FILE_PREFIX}_TAX_SCHOOL_TYPES_CPW File at $(date)...\n"  >> $log
     
     if [[ -e ${DB_HOME}/${DATA_FILE_PREFIX}_TAX_SCHOOL_TYPES_CPW ]]  &&
        [[ -s ${DB_HOME}/${DATA_FILE_PREFIX}_TAX_SCHOOL_TYPES_CPW ]]; then
        
        MSGFILE=${LOG_HOME}/msg_${DATA_FILE_PREFIX}_TAX_SCHOOL_TYPES_CPW.txt
        echo "" > ${MSGFILE}
        echo "Import message file: ${MSGFILE}" >> $log
        db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_TAX_SCHOOL_TYPES_CPW' OF DEL \
             METHOD P(1,2,3,4) \
             COMMITCOUNT 1000 \
             MESSAGES '${MSGFILE}' \
             INSERT_UPDATE INTO ${schema_name}.tax_school_types ( \
                state, school_type, state_url, update_date )" | sed 's/  */ /g' >> $log
        fn_import_process_log $? ${MSGFILE}
     
     else
        printf "\n The file ${DATA_FILE_PREFIX}_TAX_SCHOOL_TYPES_CPW was either absent or empty. \n"  >> $log
     fi   ### ..._TAX_SCHOOL_TYPES_CPW ]]...
          
          
     printf "\n Starting Loading ${DATA_FILE_PREFIX}_STATE_TAX_CHOICE_CPW File at $(date)...\n"  >> $log
     
     if [[ -e ${DB_HOME}/${DATA_FILE_PREFIX}_STATE_TAX_CHOICE_CPW ]]  &&
        [[ -s ${DB_HOME}/${DATA_FILE_PREFIX}_STATE_TAX_CHOICE_CPW ]]; then
        
        MSGFILE=${LOG_HOME}/msg_${DATA_FILE_PREFIX}_STATE_TAX_CHOICE_CPW.txt
        echo "" > ${MSGFILE}
        echo "Import message file: ${MSGFILE}" >> $log
        db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_STATE_TAX_CHOICE_CPW' OF DEL \
             METHOD P(1,2,3,4) \
             COMMITCOUNT 1000 \
             MESSAGES '${MSGFILE}' \
             INSERT_UPDATE INTO ${schema_name}.state_tax_choices ( \
                state, activated, agency_url, update_date )" | sed 's/  */ /g' >> $log
        fn_import_process_log $? ${MSGFILE}
     
     else
        printf "\n The file ${DATA_FILE_PREFIX}_STATE_TAX_CHOICE_CPW was either absent or empty. \n"  >> $log
     fi   ### ..._STATE_TAX_CHOICE_CPW ]]...
     
     
        # Per QC #1596 request July 2013: New file handling for test data...
        
     printf "\n Starting Loading ${DATA_FILE_PREFIX}_SCHL_SCHOOL_TAX_STATUS_CPW File at $(date)...\n"  >> $log
     
     if [[ -e ${DB_HOME}/${DATA_FILE_PREFIX}_SCHL_SCHOOL_TAX_STATUS_CPW ]]  &&
        [[ -s ${DB_HOME}/${DATA_FILE_PREFIX}_SCHL_SCHOOL_TAX_STATUS_CPW ]]; then
        
        MSGFILE=${LOG_HOME}/msg_${DATA_FILE_PREFIX}_SCHL_SCHOOL_TAX_STATUS_CPW.txt
        echo "" > ${MSGFILE}
        echo "Import message file: ${MSGFILE}" >> $log
        db2 "IMPORT FROM '${DB_HOME}/${DATA_FILE_PREFIX}_SCHL_SCHOOL_TAX_STATUS_CPW' OF DEL \
             METHOD P(1,2,3,4,5) \
             COMMITCOUNT 1000 \
             MESSAGES '${MSGFILE}' \
             INSERT_UPDATE INTO ${schema_name}.school_tax_status ( \
                school_id, tax_status, expiration_date, \
                invalid_reason, update_date )" | sed 's/  */ /g' >> $log
        fn_import_process_log $? ${MSGFILE}
     
     else
        printf "\n The file ${DATA_FILE_PREFIX}_SCHL_SCHOOL_TAX_STATUS_CPW was either absent or empty. \n"  >> $log
     fi   ### ..._SCHL_SCHOOL_TAX_STATUS_CPW ]]...
     
     
        #--------------------------
        # DATA FILE EXPORT SECTION
        #--------------------------
     printf "\n Starting uploading ${DATA_FILE_PREFIX}_SCHOOL_TAX_SELECTION_EXPORT File at $(date)... \n"  >> $log
     
     db2 "EXPORT TO '${DB_HOME}/${DATA_FILE_PREFIX}_SCHOOL_TAX_SELECTION_EXPORT' OF DEL \
          MODIFIED BY DATESISO \
          SELECT * FROM ${schema_name}.v_school_tax_selection_export \
             FOR READ ONLY WITH UR " >> $log
     
        #-------------------------------
        # TASK ACTION RECORDING SECTION
        #-------------------------------
        
     new_export_exec_timestamp=$(db2 -x "SELECT CURRENT TIMESTAMP FROM sysibm.sysdummy1" )
     
     db2 -v "INSERT INTO ${schema_name}.maint_bkfair_tasks \
             (task_id, create_timestamp, task_name, script_name) \
             VALUES ( (SELECT MAX(task_id) + 1 FROM ${schema_name}.maint_bkfair_tasks), \
                     '${new_export_exec_timestamp}', \
                     'Bkfair Tax Load', \
                     '${app_env}-Bkfair_cis_load.tax_load' ) " | sed 's/  */ /g' >> $log
     
     db2 -v terminate  >> $log
     printf "\n Terminate DB Connection from ${db_name} at $(date)...\n" >> $log
     
     
        #--------------------------
        # CRM UPLOAD SECTION
        #--------------------------
          
     printf "\n Start CRM FTP Up-Load connect at $(date)... \n"   >> $log
     
     printf "\n----------------------------------------------------"     >> $log
     printf "\n                Start CIS FTP Up-Load at $(date)... "     >> $log
     printf "\n----------------------------------------------------\n\n" >> $log
     
          # ------------------------------------------------
          # Ftp/upload file ...SCHOOL_TAX_SELECTION_EXPORT
          #   back to CIS SIG_BookFairs directory.
          # ------------------------------------------------

ftp -iv ${node_ip} << EOF >> $log 
binary
lcd ${DB_HOME}
cd ${FTP_HOME}
bin
put ${DATA_FILE_PREFIX}_SCHOOL_TAX_SELECTION_EXPORT  ${DATA_FILE_PREFIX}_SCHOOL_TAX_SELECTION_EXPORT
bye
EOF

             # Check if we have not lost a connection to the fpt server
        fn_ftp_connection_check  cis_ftp  PUT_FILES
     
          # -----------------------------------
          # Time to clean up old files...
          # -----------------------------------
     find ${LOG_HOME} -mtime +33 -name "${app_env}_bkfair_tax_load*" -exec rm -f {} \;

          # Build the "Tax Load" Email Feedback Body...
     printf "\n\t All,"                                                >  ${email_feedback_body}
     printf "\n\n\t Please find the attachments as a feedback to "     >> ${email_feedback_body}
     printf "\n\t the Tax Load script from the Bookfair "              >> ${email_feedback_body}
     printf "\n\t database in ${app_env} environment."                 >> ${email_feedback_body}
     printf "\n\n\t Thank you"                                         >> ${email_feedback_body}
     printf "\n\t The Data Team\n"                                     >> ${email_feedback_body}

          #-------------------------------
          #     Check Logs for Errors
          # -----------------------------------
          # this will create the attachements
          # -----------------------------------
          # Systematically add the log file to the list of 
          # files with import problems to be used by default 
          # in the email...
          # As a reminder, the variable ${CPW_FAILED_FILES}
          # represents the "-a ..." section of file attachments.
          # Leave the "uuencode" as is in this script!
          #-------------------------------
     
     ERRORCHK=$(grep -c 'Error' $log)
     
          # some feedback data...
     printf "\n\n     ERRORCHK        : ${ERRORCHK}\n\n"         >> $log
     printf "\n\n     IMP_FAIL        : ${IMP_FAIL}\n\n"         >> $log
     printf "\n\n     CPW_FAILED_FILES: ${CPW_FAILED_FILES}\n\n" >> $log
     
        # Sending email to specific users based 
        # on DEV, QA, and Productions environment...
     
        # -----------------------------------
        # this will create the attachements
        # -----------------------------------
        # Systematically add the log file to the list of 
        # files with import problems to be used by default 
        # in the email...
     
     if (( $IMP_FAIL != 0 )); then
        fn_process_message_files  tax_load
        
        if [[ "${app_env}" == "dev"   ||
              "${app_env}" == "dev02"   ]] &&
           (( $ERRORCHK != 0 )); then
           #uuencode $log $log >> $BKF_ENCODE
           CPW_FAILED_FILES="$CPW_FAILED_FILES -a ${log}"
           mail -s "db2_task_failure-${host_name}-${app_env}-${subject_line_4}" \
                ${CPW_FAILED_FILES} "${email_dev}"  <  ${email_feedback_body}
        fi
     
        if [[ "${app_env}" == "qa"    ||
              "${app_env}" == "qa02"  ||
              "${app_env}" == "uat01"   ]] &&
           (( $ERRORCHK != 0 )); then
           #uuencode $log $log >> $BKF_ENCODE
           CPW_FAILED_FILES="$CPW_FAILED_FILES -a ${log}"
           mail -s "db2_task_failure-${host_name}-${app_env}-${subject_line_4}" \
                ${CPW_FAILED_FILES} "${email_qa}"  <  ${email_feedback_body}
        fi
        
     else
               # ensure that an Log Encoded file exists 
               #  to be able to send the log file...
   
          BKF_ENCODE=/tmp/${user_name}_${app_env}_bkfair_tax_load.tmp.encode
          rm ${BKF_ENCODE}
          touch ${BKF_ENCODE}
     fi   ### if (( $import_failure != 0 ))...
     
     
          # -----------------------------------
          # Send script log feedback email...
          # -----------------------------------
     
     
     if [[ "${app_env}" == "dev"   ||
           "${app_env}" == "dev02"   ]] &&
        (( $ERRORCHK == 0 )); then
        #uuencode $log $log >> ${BKF_ENCODE}
        CPW_FAILED_FILES="$CPW_FAILED_FILES -a ${log}"
        mail -s "db2_task_success-${host_name}-${app_env}-${subject_line_success}" \
             ${CPW_FAILED_FILES} "${email_dev_run}"  <  ${email_feedback_body}
     
     elif [[ "${app_env}" == "qa"    ||
             "${app_env}" == "qa02"  ||
             "${app_env}" == "uat01"   ]] &&
          (( $ERRORCHK == 0 )); then
        #uuencode $log $log >> ${BKF_ENCODE}
        CPW_FAILED_FILES="$CPW_FAILED_FILES -a ${log}"
        mail -s "db2_task_success-${host_name}-${app_env}-${subject_line_success}" \
             ${CPW_FAILED_FILES} "${email_qa_run}"  <  ${email_feedback_body}
     
     elif [[ "${app_env}" == "prod" ]] && 
          (( $ERRORCHK != 0 )); then
        #uuencode $log $log >> ${BKF_ENCODE}
        CPW_FAILED_FILES="$CPW_FAILED_FILES -a ${log}"
        mail -s "db2_task_failure-${host_name}-${app_env}-${prod_subject_line_failure}" \
             ${CPW_FAILED_FILES} "${email_prod_support}"  <  ${email_feedback_body}
     
     else
        uuencode $log $log >> $BKF_ENCODE
        CPW_FAILED_FILES="$CPW_FAILED_FILES -a ${log}"
        mail -s "db2_task_success-${host_name}-${app_env}-${subject_line_success}"  \
             ${CPW_FAILED_FILES} "${email_prod_support}"  <  ${email_feedback_body}
     fi   ### if [[ "${app_env}" == "dev"   ||...
     
        # Time to clean up old logs...
     fn_directory_cleanup
     
     
     #################################################################
     #################################################################
     #               SEVENTH SECTION: BKFAIR ZIPCODE_LOAD
     #                    
     # Purpose : Load Zipcode Data using a production FTP site
     #           to import a vendor file for zipcodes.
     #
     # Remark  : Original Script was known as:
     #           bkfair_zipcode_load.ksh
     #
     # History : 12/09/2013 (fc) Release 24 (V-22)
     #           Integrate the Zipcode load script.
     #           
     #################################################################
     #################################################################

elif [[ "${script_action}" == "zipcode_load" ]]; then
          # Setting emails values...
     subject_line="Bkfair ZipCode-Lat-Lon Load Process "
     mail_report_list="mnaresh@scholastic.com,fcabaret@scholastic.com,eCommerceDatabaseAdministrators@scholastic.com"
     mailout_list="mnaresh@scholastic.com,fcabaret@scholastic.com,eCommerceDatabaseAdministrators@scholastic.com"
     mailout_list_error="mnaresh@scholastic.com,fcabaret@scholastic.com,eCommerceDatabaseAdministrators@scholastic.com"
     
          # Set email subject lines and recipient to test recipients
          #   when the option of "dev_test_run" has been set as an 
          #   option parameter.
          
     if [[ "${script_option}" == "dev_test_run" ]]; then
          fn_mail_test_setup
          subject_line="Test-${user_name}-${app_env}-Bkfair ZipCode-Lat-Lon Load Process "
     fi

     printf "\n----------------------------------------------------------"     >> $log
     printf "\n  Starting Archiving of last month Zipcode file at $(date) ..." >> $log
     printf "\n----------------------------------------------------------\n\n" >> $log
     
     
      #------------------------------------------------
      # Get Files Section...
      #   and set node_ip to be ftp_zipcode_ip
      #   for function call parameter standardization.
      # Also, the file is prepended with ${app_env} to avoid
      #   confusion when running the script from the 
      #   multiple swimlanes.
      #------------------------------------------------
      
node_ip=${ftp_zipcode_ip}
ftp -iv ${node_ip} << EOF | tee -a ${log}
binary
lcd ${DB_HOME}
cd ${FTP_ZIPCODE_HOME}
ls
get ${ftp_zipcode_file} ${app_env}_${ftp_zipcode_file}
bye
EOF
#
        # Test for Connection Status...
     fn_ftp_connection_check  $?  zipcode_ftp  GET_FILES
     
        # Test for existence of zipcode file...
     if [[ -f ${app_env}_${ftp_zipcode_file} ]]; then
        echo "Start to load Bkfair Zipcode file at $(date). " | tee -a ${log}
     else
        echo " ===========================================" | tee -a ${log}
        echo " "                                            | tee -a ${log}
        echo "Bkfair ZIPCODE file not found on FTP site. "  | tee -a ${log}
        echo "The File did not load !!!"                    | tee -a ${log}
        echo " "                                            | tee -a ${log}
        echo " ===========================================" | tee -a ${log}
        mailx -s "db2_task_failure -${host_name}- ${hostname_env}-${subject_line}... zipcode file inexistant" "$mailout_list" < ${log}
        exit 1
     fi
     
     printf "\n\n----- Connection to DB on $(date)......\n\n" | tee -a $log
     db2 -v connect to ${db_name} >> ${log}
     
     printf "\n\n----- Backup current LU_ZIPCODES tables before importing anew on $(date)..." | tee -a ${log}
     db2 -v "EXPORT TO '${DB_HOME}/bu__lu_zipcodes__bef_import_${exec_date}_${exec_time}.ixf' OF IXF \
                SELECT * FROM ${schema_name}.lu_zipcodes \
                FOR READ ONLY WITH UR " | sed 's/  */ /g' | tee -a ${log}
     
     printf "\n\n----- Start loading zipcode into ZIPCODE_DATA_LOAD table on $(date)..." | tee -a ${log}
          # Note: We must skip the first 2 records as they represent headers and Vendor info.
     
     MSGFILE=${LOG_HOME}/msg_${DATA_FILE_PREFIX}_FTP_ZIPCODE_FILE_IMPORT_${exec_date}_${exec_time}.txt
     
     db2 -v "IMPORT FROM '${app_env}_${ftp_zipcode_file}' OF DEL \
                MODIFIED BY COMPOUND=5 \
                COMMITCOUNT 5000 \
                SKIPCOUNT 2 \
                MESSAGES '${MSGFILE}' \
                REPLACE INTO ${schema_name}.zipcode_data_load" | sed 's/  */ /g' | tee -a ${log}
     
     db2 -v "REORG TABLE ${schema_name}.zipcode_data_load \
                INDEX ${schema_name}.x_zipcode_data_load" | sed 's/  */ /g' | tee -a ${log}
     
     db2 -v "RUNSTATS ON TABLE ${schema_name}.zipcode_data_load \
                WITH DISTRIBUTION AND INDEXES ALL" | sed 's/  */ /g' | tee -a ${log}
     
     printf "\n\n----- Export the zipcodes selecting the Preferred cities (P) on $(date)..." | tee -a ${log}
     db2 -v "EXPORT TO '${app_env}_${application_zipcodes}' OF IXF \
                SELECT zipcode, \
                       latitude || '^' || longitude \
                   FROM ${schema_name}.zipcode_data_load \
                   WHERE preferred = 'P' \
                     AND state IS NOT NULL" | sed 's/  */ /g' | tee -a ${log}
     
     printf "\n\n----- Start loading zipcode into LU_ZIPCODES on $(date)..."
     
     MSGFILE=${LOG_HOME}/msg_${DATA_FILE_PREFIX}_APPLICATION_ZIPCODES_IMPORT_${exec_date}_${exec_time}.txt
     
     db2 -v "IMPORT FROM '${app_env}_${application_zipcodes}' OF IXF \
                MODIFIED BY COMPOUND=5 \
                COMMITCOUNT 5000 \
                MESSAGES '${MSGFILE}' \
                REPLACE INTO ${schema_name}.lu_zipcodes" | sed 's/  */ /g' | tee -a ${log}
     
     printf "\n\n----- Starting statistics on LU_ZIPCODES on $(date)..."  | tee -a ${log}
     db2 -v "RUNSTATS ON TABLE ${schema_name}.lu_zipcodes \
                WITH DISTRIBUTION AND DETAILED INDEXES ALL" | sed 's/  */ /g' | tee -a ${log}
     
          # Register this new task action in maintenance table...
     
     new_export_exec_timestamp=$(db2 -x "SELECT CURRENT TIMESTAMP FROM sysibm.sysdummy1" )
     
     db2 -av "INSERT INTO ${schema_name}.maint_bkfair_tasks \
                     (task_id, create_timestamp, task_name, script_name) \
                 VALUES ( (SELECT MAX(task_id) + 1 FROM ${schema_name}.maint_bkfair_tasks), \
                        '${new_export_exec_timestamp}', \
                        'Bkfair Zipcode Data Refresh', \
                        '${app_env}-Bkfair_cis_load.zipcode_load' ) " | sed 's/  */ /g' >> $log
     
     db2 -v "terminate"  | tee -a ${log}
     
        # Archive  and zip the current file...
     mv ${app_env}_${ftp_zipcode_file} ${DB_HOME}/${app_env}_${ftp_zipcode_file}_${exec_date}_${exec_time}
     gzip ${DB_HOME}/${app_env}_${ftp_zipcode_file}_${exec_date}_${exec_time}
     
        # Clean up input data files older than 6 months...
     find ${DB_HOME} -mtime +190 -name "${app_env}_${ftp_zipcode_file}*" -exec rm -f {} \;
     find ${LOG_HOME} -mtime +190 -name "${app_env}_${script_name}__${script_action}*" -exec rm -f {} \;
     find ${LOG_HOME} -mtime +190 -name "msg_${app_env}*" -exec rm -f {} \;
     
        # Note: to re-use this zipped file, one must strip the date-time
        #       section from the name to be able to use it into the code
        
          # Build the "Tax Load" Email Feedback Body...
     printf "\n\t All,"                                                >  ${email_feedback_body}
     printf "\n\n\t Please find the attachments as a feedback to "     >> ${email_feedback_body}
     printf "\n\t the Zipcode Load script from the Bookfair "          >> ${email_feedback_body}
     printf "\n\t database in ${app_env} environment."                 >> ${email_feedback_body}
     printf "\n\n\t Thank you"                                         >> ${email_feedback_body}
     printf "\n\t The Data Team\n"                                     >> ${email_feedback_body}

          # some feedback data...
     printf "\n\n     ERRORCHK        : ${ERRORCHK}\n\n"         >> $log
     printf "\n\n     IMP_FAIL        : ${IMP_FAIL}\n\n"         >> $log
     printf "\n\n     CPW_FAILED_FILES: ${CPW_FAILED_FILES}\n\n" >> $log

     mailx -s "db2_task_success -${host_name}- ${app_env}-${subject_line} completed" \
           -a ${log}  "$mailout_list" < ${email_feedback_body}
     
fi   ### elif [[ "${script_action}" == "data_load, secondary_load, hourly_ofe,
        ### email_forms, rewards_load, tax_load, zipcode_load" ]]...

exit 0
